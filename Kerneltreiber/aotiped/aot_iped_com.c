/* ***************************************************************************
 *
 *   Customer: AoT
 *   Project#: 17-03
 *       Name: aot_iped communication 
 *
 *     Module: Kernel Driver
 *      State:
 * Originator: Schwab Daniel
 *
 *   $HeadURL: $
 *  $Revision: $
 *      $Date: $
 *    $Author: $
 *
 *  Developed by Art of Technology AG, 2017
 *
 **************************************************************************** */

// VERSION 1.0.0

#include <linux/module.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/wait.h>
#include <asm/uaccess.h>
#include <linux/sched.h>
#include <linux/poll.h>
#include <asm/uaccess.h>
#include <asm/atomic.h>

#define DEVICENAME		20
#define DEVICENAMELENGTH	12
#define DEVICE_BUFFER_SIZE	8200

#define DEVICENAMECOUNT		3	// Number of devices

#define AOT_UART_COM		0
#define AOT_DEBG_COM		1
#define AOT_IPED_COM0		2
#define AOT_IPED_COM1		3
#define AOT_IPED_COM2		4
#define AOT_IPED_COM3		5
#define AOT_IPED_COM4		6
#define AOT_IPED_COM5		7
#define AOT_IPED_COM6		8
#define AOT_IPED_COM7		9
#define AOT_IPED_COM8		10
#define AOT_IPED_COM9		11
#define AOT_IPED_COM10		12
#define AOT_IPED_COM11		13
#define AOT_IPED_COM12		14
#define AOT_IPED_COM13		15
#define AOT_IPED_COM14		16
#define AOT_IPED_COM15		17
#define AOT_IPED_COM16		18
#define AOT_IPED_COM17		19
#define AOT_IPED_COM18		20
#define AOT_IPED_COM19		21
#define AOT_IPED_COM20		22
#define AOT_IPED_COM21		23

#define DATEN_IM_FIFO		0x01
#define DATEN_FREE_IM_FIFO	0x02

// Wait queue

char au8_devicename_class[]    = "aot_iped";

// Choose always the same name length
char au8_devicename_tmpl[]     = "aot_iped_COM";
char au8_devicename_com_tmpl[] = "aot_UART_COM";
char au8_devicename_dbg_tmpl[] = "aot_DEBG_COM";

typedef struct
{ 
	char au8_devicename[DEVICENAME];
	char au8_Buffer_in[DEVICE_BUFFER_SIZE];
	char au8_Buffer_out[DEVICE_BUFFER_SIZE];
	int in_FIFOin;	  	// Input Zaehler
	int in_FIFOout;		// Output Zaehler
	int out_FIFOin;	  	// Input Zaehler
	int out_FIFOout;	// Output Zaehler
	struct device *aot_dev;
	dev_t dev_number;
	wait_queue_head_t my_queue;
	int i32my_queue_flag;
} aot_struct_t;


static aot_struct_t aot_devicestruct[DEVICENAMECOUNT];

static struct cdev *driver_object;
static struct class *aot_class;

//****************************************************************************
// Gibt zurueck, wieviel Speicher im FIFO noch frei ist
//****************************************************************************
int fifofree (int in, int out, int max)
{
if (in < out)
	{
	return (out - in);
	}
	else
	{
	if (in == out)
		{
		// ACHTUNG max + 1 muss gemacht werden, da die 0 ebenfalls ein Speicherplatz ist
		return (max + 1);
		}
		else
		{
		// ACHTUNG max + 1 muss gemacht werden, da die 0 ebenfalls ein Speicherplatz ist
		return ((max + 1) - (in - out));
		}
	}
return 0;
}

//****************************************************************************
// FIFO Increment ausf�hren
//****************************************************************************
int incFifo (int fifo, int max)
{
if (fifo == max)
	{
	fifo = 0x00;
	}
	else
	{
	fifo++;
	}
return (fifo);
}

//****************************************************************************
// Status Abfragen vom FIFO
//****************************************************************************
int fifo_Status (int s, int FIFOin, int FIFOout, int MAXFifo)
	{
	switch (s)
		{
		case DATEN_IM_FIFO:						// Gibt zur�ck, wieviel Daten im Fifo sind
			return (MAXFifo + 1) - fifofree (FIFOin, FIFOout, MAXFifo);
		break;
		case DATEN_FREE_IM_FIFO:					// Gibt zur�ck, wieviel Bytes noch frei sind
			return (fifofree (FIFOin, FIFOout, MAXFifo) - 1);
		break;
		}
	return 0;
	}

//****************************************************************************
// Daten vom Fifo holen 
//****************************************************************************
int fifo_POP (char *Fifo, int *FIFOout, int MaxFIFO)
	{
	char x;
	// Werte muessen nun vom FIFO geladen werden
	x = Fifo[*FIFOout];

	// FIFO Counter Berechnen
	*FIFOout = incFifo (*FIFOout, MaxFIFO);

	return (x);
	}


//****************************************************************************
// Daten in den Fifo kopieren 
//****************************************************************************
void fifo_PUSH (char *Fifo, int *FIFOin, int MaxFIFO, char x)
	{
	if (Fifo != 0)
		{
		// Werte die uebergeben wurden, muessen nun in den FIFO geladen werden
		Fifo[*FIFOin] = x;

		// FIFO Counter Berechnen
		*FIFOin = incFifo (*FIFOin, MaxFIFO);
		}
	}

//****************************************************************************
// Fifo vereinfachung - INPUT
//****************************************************************************
char in_POP (int i32Minor)
	{
	return fifo_POP (aot_devicestruct[i32Minor].au8_Buffer_in, &aot_devicestruct[i32Minor].in_FIFOout, DEVICE_BUFFER_SIZE);
	}

void in_PUSH (char x, int i32Minor)
	{
	fifo_PUSH (aot_devicestruct[i32Minor].au8_Buffer_in, &aot_devicestruct[i32Minor].in_FIFOin, DEVICE_BUFFER_SIZE, x);
	}

int in_Status (int s, int i32Minor)
	{
	return fifo_Status (s, aot_devicestruct[i32Minor].in_FIFOin, aot_devicestruct[i32Minor].in_FIFOout, DEVICE_BUFFER_SIZE);
	}

//****************************************************************************
// Fifo vereinfachung - OUTPUT
//****************************************************************************
char out_POP (int i32Minor)
	{
	return fifo_POP (aot_devicestruct[i32Minor].au8_Buffer_out, &aot_devicestruct[i32Minor].out_FIFOout, DEVICE_BUFFER_SIZE);
	}

void out_PUSH (char x, int i32Minor)
	{
	fifo_PUSH (aot_devicestruct[i32Minor].au8_Buffer_out, &aot_devicestruct[i32Minor].out_FIFOin, DEVICE_BUFFER_SIZE, x);
	}

int out_Status (int s, int i32Minor)
	{
	return fifo_Status (s, aot_devicestruct[i32Minor].out_FIFOin, aot_devicestruct[i32Minor].out_FIFOout, DEVICE_BUFFER_SIZE);
	}

//****************************************************************************
// Kernel Driver - Clear Buffer
//****************************************************************************
static void clear_buffer (int i32Minor)
{
	unsigned int u32c;

	for (u32c = 0; u32c < DEVICE_BUFFER_SIZE; u32c++)
	{
		aot_devicestruct[i32Minor].au8_Buffer_in[u32c] = 0x00;
		aot_devicestruct[i32Minor].au8_Buffer_out[u32c] = 0x00;
		
		init_waitqueue_head(&aot_devicestruct[i32Minor].my_queue);
	}

	aot_devicestruct[i32Minor].in_FIFOin = 0x00;	// Input Zaehler
	aot_devicestruct[i32Minor].in_FIFOout = 0x00;	// Output Zaehler
	aot_devicestruct[i32Minor].out_FIFOin = 0x00;	// Input Zaehler
	aot_devicestruct[i32Minor].out_FIFOout = 0x00;	// Output Zaehler
}

//****************************************************************************
// Kernel Driver - Set Devicename
//****************************************************************************
static void set_devicename (unsigned char * pu8_name, unsigned char * pu8_tmpl, int i32Minor, unsigned int u32length)
{
	unsigned int u32x;
	signed char i8MinorL = '0';
	signed char i8MinorH = '0';
	signed int i32MinorTmp;

	if (i32Minor < DEVICENAMECOUNT)
	{
		for (u32x = 0; u32x < u32length; u32x++)
		{
			pu8_name[u32x] = pu8_tmpl[u32x];
		}

		if (i32Minor > 1)
		{
			i32MinorTmp = i32Minor - 2;
			if (i32MinorTmp < 10)
			{
				i8MinorH = '0';
				i8MinorL = i32MinorTmp | 0x30;
			}
			else if (i32MinorTmp < 20)
			{
				i8MinorH = '1';
				i8MinorL = (i32MinorTmp - 10) | 0x30;
			}

			if (i8MinorH != '0')
			{
				pu8_name[u32x++] = i8MinorH;
			}
			pu8_name[u32x++] = i8MinorL;
		}
		pu8_name[u32x++] = 0x00;
	}
}

//****************************************************************************
// Kernel Driver - Open
//****************************************************************************
static int driver_open (struct inode *geraete_datei, struct file *instanz )
{
	return 0;
}

//****************************************************************************
// Kernel Driver - Close
//****************************************************************************
static int driver_close (struct inode *geraete_datei, struct file *instanz )
{
	return 0;
}

//****************************************************************************
// Kernel Driver - Read
//****************************************************************************
static ssize_t driver_read (struct file *instanz, char __user *userbuffer, size_t count, loff_t *offset)
{
	unsigned long not_copied, to_copy;
	char au8_Buffer[2];
	unsigned int u32length;
	signed int i32Minor;

	// Minor Nummer ermitteln
	i32Minor = iminor (instanz->f_path.dentry->d_inode);

	if (in_Status (DATEN_IM_FIFO, i32Minor) == 0)
	{
		aot_devicestruct[i32Minor].i32my_queue_flag = 0;
		wait_event_interruptible_timeout (aot_devicestruct[i32Minor].my_queue, aot_devicestruct[i32Minor].i32my_queue_flag == 1, 10 * HZ);
	}

	if (in_Status (DATEN_IM_FIFO, i32Minor) > 0)
	{
		u32length = 1;
		au8_Buffer[0] = in_POP (i32Minor);
	}
	else
	{
		u32length = 0;
	}

	// Daten zum user senden
	to_copy = min (count, u32length);
	not_copied = copy_to_user (userbuffer, au8_Buffer, to_copy);

	// Offset berechnen
	*offset += to_copy - not_copied;

	return (to_copy - not_copied);
}

//****************************************************************************
// Kernel Driver - Write
//****************************************************************************
static ssize_t driver_write( struct file *instanz, const char __user *userbuffer, size_t count, loff_t *offset)
{
	size_t to_copy, not_copied;
	signed int i32Minor = 9999;
	unsigned int u32length = count;
	char au8_Buffer[2];

	if (u32length > 1)
	{
		u32length = 1;
	}

	// Daten in den Kernel holen
	to_copy = min (count, u32length);
	not_copied = copy_from_user (au8_Buffer, userbuffer, to_copy );

	// Minor Nummer ermitteln
	i32Minor = iminor (instanz->f_path.dentry->d_inode);

	switch (i32Minor)
	{
	case AOT_UART_COM:
		{
		if (in_Status (DATEN_FREE_IM_FIFO, i32Minor) > 0) { in_PUSH (au8_Buffer[0], AOT_IPED_COM0); }
		aot_devicestruct[AOT_IPED_COM0].i32my_queue_flag = 1;
		wake_up_interruptible (&aot_devicestruct[AOT_IPED_COM0].my_queue);
		}
	break;
	case AOT_DEBG_COM:
		{
		if (in_Status (DATEN_FREE_IM_FIFO, i32Minor) > 0) { in_PUSH (au8_Buffer[0], i32Minor); }
		aot_devicestruct[i32Minor].i32my_queue_flag = 1;
		wake_up_interruptible (&aot_devicestruct[i32Minor].my_queue);
		}
	break;
	case AOT_IPED_COM0:
		{
		if (in_Status (DATEN_FREE_IM_FIFO, i32Minor) > 0) { in_PUSH (au8_Buffer[0], AOT_UART_COM); }
		aot_devicestruct[AOT_UART_COM].i32my_queue_flag = 1;
		wake_up_interruptible (&aot_devicestruct[AOT_UART_COM].my_queue);
		}
	break;
	}

	// Offset berechnen
	*offset += to_copy-not_copied;

	return (to_copy - not_copied);
}

//****************************************************************************
// Kernel Driver - fops
//****************************************************************************
static struct file_operations fops = {
	.owner= THIS_MODULE,
	.read= driver_read,
	.write = driver_write,
	.open= driver_open,
	.release= driver_close,
};

//****************************************************************************
// Kernel Driver - Init
//****************************************************************************
static int __init mod_init( void )
{
	unsigned int u32Minor;

	// Set the devicename
	for (u32Minor = 0; u32Minor < DEVICENAMECOUNT; u32Minor++)
	{
		clear_buffer (u32Minor);

		if (u32Minor == 0)
		{
		set_devicename (aot_devicestruct[u32Minor].au8_devicename, au8_devicename_com_tmpl, u32Minor, DEVICENAMELENGTH);
		}
		else if (u32Minor == 1)
		{
		set_devicename (aot_devicestruct[u32Minor].au8_devicename, au8_devicename_dbg_tmpl, u32Minor, DEVICENAMELENGTH);
		}
		else
		{
		set_devicename (aot_devicestruct[u32Minor].au8_devicename, au8_devicename_tmpl, u32Minor, DEVICENAMELENGTH);
		}
	}

	for (u32Minor = 0; u32Minor < DEVICENAMECOUNT; u32Minor++)
	{
		if (alloc_chrdev_region(&aot_devicestruct[u32Minor].dev_number, u32Minor, 1, aot_devicestruct[0].au8_devicename) < 0)
		{
			return -EIO;
		}
	}

	driver_object = cdev_alloc(); /* Anmeldeobjekt reservieren */
	if (driver_object == NULL)
	{
		goto free_device_number;
	}

	driver_object->owner = THIS_MODULE;
	driver_object->ops = &fops;

	for (u32Minor = 0; u32Minor < DEVICENAMECOUNT; u32Minor++)
	{
		if (cdev_add(driver_object, aot_devicestruct[u32Minor].dev_number, 1))
		{
			goto free_cdev;
		}
	}

	/* Eintrag im Sysfs, damit Udev den Geraetedateieintrag erzeugt. */
	aot_class = class_create(THIS_MODULE, au8_devicename_class );
	if (IS_ERR( aot_class ))
	{
		pr_err( "no udev support\n");
		goto free_cdev;
	}

	for (u32Minor = 0; u32Minor < DEVICENAMECOUNT; u32Minor++)
	{
		aot_devicestruct[u32Minor].aot_dev = device_create (aot_class, NULL, aot_devicestruct[u32Minor].dev_number, NULL, "%s", aot_devicestruct[u32Minor].au8_devicename );
		if (IS_ERR( aot_devicestruct[u32Minor].aot_dev ))
		{
			pr_err( "device_create failed\n");
			goto free_class;
		}
	}
	return 0;

free_class:
	class_destroy( aot_class );
free_cdev:
	kobject_put( &driver_object->kobj );
free_device_number:
	unregister_chrdev_region(aot_devicestruct[0].dev_number, 1);
	return -EIO;
}

//****************************************************************************
// Kernel Driver - Exit
//****************************************************************************
static void __exit mod_exit( void )
{
	/* Loeschen des Sysfs-Eintrags und damit der Geraetedatei */
	device_destroy( aot_class, aot_devicestruct[0].dev_number );
	class_destroy( aot_class );
	/* Abmelden des Treibers */
	cdev_del( driver_object );
	unregister_chrdev_region(aot_devicestruct[0].dev_number, 1);
	return;
}

//****************************************************************************
// Kernel Driver - Module
//****************************************************************************
module_init( mod_init );
module_exit( mod_exit );

//****************************************************************************
// Kernel Driver - Metainformation
//****************************************************************************
MODULE_AUTHOR("Art of Technology AG, Daniel Schwab");
MODULE_LICENSE("Proprietary");
MODULE_DESCRIPTION("Art of Technology IPED protocol");
