# README #


### Version 1.3.3 ###
FW Update system call replaced with cmd line call, Timeout to send next cmd 10s

### Version 1.3.2 ###
Protokoll ihex modified, every answer takes as a ERR_OK for firmware update

### Version 1.3.1 ###
Add FW update - multi device

### Version 1.3.0 ###
Add FW update

### Version 1.2.2 ###
Add missing container Type

### Version 1.2.1 ###
Bad CRC return ERR_AGAIN
serialdriver interpretation OFF

### Version 1.2.0 ###
Hex output and Transmit output defined as humanreadable.

### Version 1.1.8 ###
hex to bin length check removed

### Version 1.1.7 ###
changed to stdint
bugfix response interchange_protocol_decode
Stack allocating removed inside Interchange Protocoll

### Version 1.1.5 ###
Interact with Kernel

### Version 1.1.4 ###
Bugfix Debug Output

### Version 1.1.3 ###
Small patch for the cmd line tool.
Bug in the function main_receive_data.c, do not use the Variable u32_position.

### Version 1.1.2 ###
Data output now with 1 single line. Data separated with: ";"
Line end with "\r\n"
Bug detected in the cmd line Tool.

### Version 1.1.1 ###
Some small fixes in the interchange protocol.

### Version 1.0.0 ###
interchange_protocol_encode_decode [-d data] [-p COM] [-t type] [-l length] [-a] [-s] [-c] [-r] [-x] [-h] [-?] [-H] [-R]

### option  Description ###

* -h      help
* -?      help
* -d      data
* -t      type
* -l      length data
* -a      add command
* -s      add sync
* -c      add container
* -x      send all encoded data
* -r      recevie encoded data, decode it and write it to stdout
* -p      set default com port
* -e      run encode and decode examples (if debug enabled)
* -H      Print buffer, hex coded
* -R      send 0xFF 1.5x buffer size, resync and flush buffer

### 1. Example CMD ###
Send: TYPE_VEHICLE_TYPE_1 with 4 Bytes data

* interchange_protocol_encode_decode -p /dev/ttyS2 -t 8 -l 4 -d 0x00000001 -a -s -x

### 2. Example Container ###
Send: TYPE_SCHLOSS_BATTERY_1 with 2 Bytes data, inside a container

* interchange_protocol_encode_decode -p /dev/ttyS2 -t 5 -l 2 -d 0x0009 -a -t 2 -c -s -x

### 3. Example Container ###
Send: TYPE_VEHICLE_TYPE_1 with 4 Bytes data, TYPE_SCHLOSS_BATTERY_1 with 2 Bytes data, inside a container

* interchange_protocol_encode_decode -p /dev/ttyS2 -t 8 -l 4 -d 0x00000001 -a -t 5 -l 2 -d 0x0009 -a -t 2 -c -s -x

### 4. Example Receive ###
Receive:

* interchange_protocol_encode_decode -p /dev/ttyS2 -r

### 5. Example Flush ###
Flush:

* interchange_protocol_encode_decode -p /dev/ttyS2 -R
