/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: MAIN_READ_FILE_H
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/Publibike/Fahrradausleihstation/Cmd_Line_Tool/trunk/main_read_file.h $
 *  $Revision: 20184 $
 *      $Date: 2017-07-11 13:59:36 +0200 (Di, 11 Jul 2017) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 *************************************************************************** */

#ifndef __MAIN_READ_FILE_H
#define __MAIN_READ_FILE_H

#include "interchange_protocol_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  global constants                                                         |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  function prototypes for C                                                |
+--------------------------------------------------------------------------*/
void main_file_read_data (uint8_t *pu8_default_drive, uint8_t pu8_mikrokontroller, uint8_t *pu8_device);
uint8_t hextobin_sub (uint8_t u8x);

#ifdef __cplusplus
}
#endif

#endif // __MAIN_READ_FILE_H
/* eof */
