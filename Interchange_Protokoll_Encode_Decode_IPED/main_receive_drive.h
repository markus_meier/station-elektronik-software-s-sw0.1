/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: MAIN_PIPE_H
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/Publibike/Fahrradausleihstation/Cmd_Line_Tool/trunk/main_pipe.h $
 *  $Revision: 20184 $
 *      $Date: 2017-07-11 13:59:36 +0200 (Di, 11 Jul 2017) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 *************************************************************************** */

#ifndef __MAIN_RECEIVE_DRIVE_H
#define __MAIN_RECEIVE_DRIVE_H

#include "interchange_protocol_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  global constants                                                         |
+--------------------------------------------------------------------------*/
#define TIMEOUT     10      // Timeout in seconds

/*--------------------------------------------------------------------------+
|  function prototypes for C                                                |
+--------------------------------------------------------------------------*/
void main_drive_receive_data (uint8_t *pu8_default_drive);


#ifdef __cplusplus
}
#endif

#endif // __MAIN_RECEIVE_DRIVE_H
/* eof */
