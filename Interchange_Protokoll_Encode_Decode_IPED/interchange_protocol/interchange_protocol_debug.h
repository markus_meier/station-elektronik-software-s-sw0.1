/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: INTERCHANGE_PROTOCOL_DEBUG_H
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/Publibike/Fahrradausleihstation/Interchange_Protocol/trunk/interchange_protocol_debug.h $
 *  $Revision: 20378 $
 *      $Date: 2017-08-24 15:54:47 +0200 (Do, 24 Aug 2017) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 *************************************************************************** */

#ifndef INTERCHANGE_PROTOCOL_DEBUG_H
#define INTERCHANGE_PROTOCOL_DEBUG_H

#ifdef __cplusplus
extern "C" {
#endif

/* --------------------------------------------------------------------------+
|  global types                                                              |
+-------------------------------------------------------------------------- */


/* --------------------------------------------------------------------------+
|  global constants                                                          |
+-------------------------------------------------------------------------- */


/* --------------------------------------------------------------------------+
|  function prototypes for C                                                 |
+-------------------------------------------------------------------------- */


/*--------------------------------------------------------------------------+
|  Macro                                                |
+--------------------------------------------------------------------------*/
#ifdef TR_DBG
	#define TRACE_DBG(...)      printf (__VA_ARGS__)
#else
	#define TRACE_DBG(...)
#endif


#ifdef __cplusplus
}
#endif

#endif // INTERCHANGE_PROTOCOL_DEBUG_H
/* eof */
