/* ***************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: DEBUG_H
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/Publibike/Fahrradausleihstation/Interchange_Protocol/trunk/interchange_protocol_type.h $
 *  $Revision: 20380 $
 *      $Date: 2017-08-24 16:21:39 +0200 (Do, 24 Aug 2017) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 *************************************************************************** */

#ifndef __INTERCHANGE_PROTOCOL_TYPE_H
#define __INTERCHANGE_PROTOCOL_TYPE_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  global constants                                                         |
+--------------------------------------------------------------------------*/
typedef enum tag_Interchange_Protocol_Type
{                                                          ///< The description at: https://publibike.atlassian.net/wiki/spaces/SYS/pages/42931830/ICOM+-+Programming
    TYPE_DEFAULT                                 = 0x0000, ///< DEFAULT for UNKNOWN TYPE
    TYPE_PROTOCOL_VERSION                        = 0x0001, ///< 
    TYPE_CONTAINER_GENERIC_1                     = 0x0002, ///< 
    TYPE_RESPONSE_1                              = 0x0003, ///< 
    TYPE_BETRIEBSNUMMER_1                        = 0x0004, ///< 
    TYPE_SCHLOSS_BATTERY_1                       = 0x0005, ///< 
    TYPE_EBIKE_BATTERY_1                         = 0x0006, ///< 
    TYPE_STATUS_1                                = 0x0007, ///< 
    TYPE_VEHICLE_TYPE_1                          = 0x0008, ///< 
    TYPE_UNLOCK_APP_1                            = 0x000A, ///< 
    TYPE_INTERACT_1                              = 0x000B, ///< 
    TYPE_SEARCH_FOR_SMARTPHONE_1                 = 0x000C, ///< 
    TYPE_SEARCH_FOR_STATION_1                    = 0x000D, ///< 
    TYPE_SMARTLOCK_STATUS_1                      = 0x000E, ///<
    TYPE_UNLOCK_STATION_1                        = 0x000F, ///< 
    TYPE_RENT_APP_1                              = 0x0010, ///< 
    TYPE_RENT_STATION_1                          = 0x0011, ///< 
    TYPE_APP_1                                   = 0x0012, ///< 
    TYPE_RFID_1                                  = 0x0013, ///<
    TYPE_USER_ID_1                               = 0x0014, ///< 
    TYPE_PUBLIC_KEY_1                            = 0x0015, ///< 
    TYPE_CHALLENGE_1                             = 0x0016, ///< 
    TYPE_SIGNATURE_APP_1                         = 0x0017, ///< 
    TYPE_SIGNATURE_SMARTLOCK_1                   = 0x0018, ///< 
    TYPE_SIGNATURE_STATION_1                     = 0x0019, ///< 
    TYPE_CREDENTIALS_1                           = 0x001A, ///<
    TYPE_SW_VERSION_1                            = 0x001B, ///<
    TYPE_DISCONNECT_1                            = 0x001C, ///<
    TYPE_RENTAL_RETURN_1                         = 0x001D, ///<
    TYPE_RIDE_DURATION_1                         = 0x001E, ///<
    TYPE_NUMBER_OF_INTERMEDIATE_STOPS_1          = 0x001F, ///<
    TYPE_NUMBER_OF_EMERGENCY_STOPS_1             = 0x0020, ///<
    TYPE_STATION_BATTERY_1                       = 0x0021, ///<
    TYPE_STATION_BACKUP_BATTERY_1                = 0x0022, ///<
    TYPE_STATION_DEBUG_1                         = 0x0023, ///<
    TYPE_LOCK_ID_1                               = 0x0024, ///<
    TYPE_STATION_TIMEOUT_1                       = 0x0025, ///<
    TYPE_LOCK_CONFIG_1                           = 0x0026, ///<
    TYPE_AMD_TIMEOUT_1                           = 0x0027, ///<
    TYPE_E_BUTTON_DELAY_1                        = 0x0028, ///<
    TYPE_MAX_RENTAL_TIME_1                       = 0x0029, ///<
    TYPE_EBIKE_SUPPORT_LEVEL_1                   = 0x002A, ///<
    TYPE_RECONNECT_DELAY_1                       = 0x002B, ///<
    TYPE_MESSAGE_DISPLAY_1                       = 0x002C, ///<
    TYPE_FW_LOCK_READY_1                         = 0x002D, ///<
    TYPE_FW_NRFL_READY_1                         = 0x002E, ///<
    TYPE_FW_UPDATE_GET_LOCK_1                    = 0x002F, ///<
    TYPE_FW_UPDATE_PREPARE_LOCK_1                = 0x0030, ///<
    TYPE_FW_UPDATE_IMAGE_LOCK_1                  = 0x0031, ///<
    TYPE_FW_UPDATE_CRC_LOCK_1                    = 0x0032, ///<
    TYPE_FW_UPDATE_EXECUTE_LOCK_1                = 0x0033, ///<
    TYPE_FW_UPDATE_GET_NRFSTATION_1              = 0x0034, ///< Do not use - for future use.
    TYPE_FW_UPDATE_PREPARE_NRFSTATION_1          = 0x0035, ///<
    TYPE_FW_UPDATE_IMAGE_NRFSTATION_1            = 0x0036, ///<
    TYPE_FW_UPDATE_CRC_NRFSTATION_1              = 0x0037, ///<
    TYPE_FW_UPDATE_EXECUTE_NRFSTATION_1          = 0x0038, ///<
    TYPE_FW_UPDATE_GET_NRFLOCK_1                 = 0x0039, ///<
    TYPE_FW_UPDATE_PREPARE_NRFLOCK_1             = 0x003A, ///<
    TYPE_FW_UPDATE_IMAGE_NRFLOCK_1               = 0x003B, ///<
    TYPE_FW_UPDATE_CRC_NRFLOCK_1                 = 0x003C, ///<
    TYPE_FW_UPDATE_EXECUTE_NRFLOCK_1             = 0x003D, ///<
    TYPE_RESPONSE_SECURE_1                       = 0x003E, ///<
    TYPE_LAST                                            , ///<
} Interchange_Protocol_Type;


typedef struct tag_Interchange_Protocol_Header
{
    uint16_t u16Type;                               ///< Type - Little-endian
    uint16_t u16Length;                             ///< Length - Little-endian
    uint8_t  au8Data[];                             ///< Data
} Interchange_Protocol_Header;


#define IPH_MIN_SIZE				(sizeof(Interchange_Protocol_Header))
#define IPED_SYNC					0x5555
#define IPED_SYNC_SIZE				0x0002
#define IPH_CRC_SIZE				0x0002
#define IPED_SYNC_HEADER_SIZE		(IPH_MIN_SIZE + IPED_SYNC_SIZE)


/*--------------------------------------------------------------------------+
|  function prototypes for C                                                |
+--------------------------------------------------------------------------*/


#ifdef __cplusplus
}
#endif

#endif // __INTERCHANGE_PROTOCOL_TYPE_H
/* eof */
