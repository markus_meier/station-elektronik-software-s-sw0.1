/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: E_H
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/Publibike/Fahrradausleihstation/Interchange_Protocol/trunk/interchange_protocol_error.h $
 *  $Revision: 20378 $
 *      $Date: 2017-08-24 15:54:47 +0200 (Do, 24 Aug 2017) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 *************************************************************************** */

#ifndef __INTERCHANGE_PROTOCOL_ERROR_H
#define __INTERCHANGE_PROTOCOL_ERROR_H


#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  global constants                                                         |
+--------------------------------------------------------------------------*/
typedef enum tag_Interchange_Protocol_Error
{
    ERR_OK                        = 0x00000000, ///< Used as confirmation of transfer on serial port
    ERR_ERROR                     = 0x00000001, ///< any error
    ERR_AGAIN                     = 0x00000002, ///< CRC error or other problem. Ask for repeating the request
    ERR_BUSY                      = 0x00000003, ///< Device or resource busy
    ERR_NOMEM                     = 0x00000004, ///< Not enough space
    ERR_TIMEDOUT                  = 0x00000005, ///< Connection timed out
    ERR_NOSYS                     = 0x00000006, ///< Function not implemented (NO SUCH SYSTEM CALL)
    ERR_WAITING_FOR_DATA          = 0x00000007, ///< Waiting for more data
    ERR_NAK                       = 0x00000008, ///< Message not acknowledged
    ERR_NO_SYNC_FOUND             = 0x00000009, ///< No Sync
    ERR_SYNC_PARTLY               = 0x0000000A, ///< part of Sync, found a 0x55 but not 0x5555
    ERR_PROTOCOL_TOO_OLD          = 0x0000000B, ///< Protocoll not supported/Allowed any more
    ERR_DISCONNECT_APP            = 0x0000000C, ///< Lock main processor commanded cancel for SEARCH_FOR_SMARTPHONE
    ERR_NO_DATA                   = 0x0000000D, ///< No Data
    ERR_WRONG_DATA_SIZE           = 0x0000000E, ///< Data length greater than buffer
    ERR_CONTAINER_DECODE_STOP     = 0x0000000F, ///< Stop decode inside a container
    ERR_DATA_RECEIVED_CORRECT     = 0x00000010, ///< Data received correct

    BUS_OK                        = 0x00010000, ///< Business response OK (e.g. start rental)
    BUS_PERSON_UNKNOWN            = 0x00010001, ///< Person unknown
    BUS_PERSON_PAYMENT_DECLINED   = 0x00010002, ///< Payment method of person has been declined
    BUS_PERSON_BLOCKED            = 0x00010003, ///< Person is blocked
    BUS_VEHICLE_UNKNOWN           = 0x00010004, ///< Vehicle is unknown
    BUS_VEHICLE_BLOCKED           = 0x00010005, ///< Vehicle is blocked
    BUS_STATION_NOT_ALLOWED       = 0x00010006, ///< Station not allowed
} Interchange_Protocol_Error;


/*--------------------------------------------------------------------------+
|  function prototypes for C                                                |
+--------------------------------------------------------------------------*/



#ifdef __cplusplus
}
#endif

#endif // __INTERCHANGE_PROTOCOL_ERROR_H
/* eof */
