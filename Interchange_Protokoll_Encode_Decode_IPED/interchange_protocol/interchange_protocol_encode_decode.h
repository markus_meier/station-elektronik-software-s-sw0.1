/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: INTERCHANGE_PROTOCOL_ENCODE_DECODE_H
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/Publibike/Fahrradausleihstation/Interchange_Protocol/trunk/interchange_protocol_encode_decode.h $
 *  $Revision: 20378 $
 *      $Date: 2017-08-24 15:54:47 +0200 (Do, 24 Aug 2017) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 *************************************************************************** */

#ifndef __INTERCHANGE_PROTOCOL_ENCODE_DECODE_H
#define __INTERCHANGE_PROTOCOL_ENCODE_DECODE_H

#include "interchange_protocol_type.h"
#include "interchange_protocol_error.h"
#include "interchange_protocol_cmd_function.h"

#ifdef __cplusplus
extern "C" {
#endif

/* --------------------------------------------------------------------------+
|  global types                                                              |
+-------------------------------------------------------------------------- */
typedef struct tag_IPED
{
    uint8_t	*pu8_iped_data;					///< Data Buffer
    uint16_t	u16Bufferlength;				///< Buffer length
    Interchange_Protocol_Header *iph;		///< Length - Little-Endian
    uint8_t 	*iph_base_addr;					///< First CMD Start Address
    uint16_t	u16Container_length;			///< Container length
    uint32_t	u32position;					///< Position
} IPED_base_struct;

/* --------------------------------------------------------------------------+
|  global constants                                                          |
+-------------------------------------------------------------------------- */
#define IPED_MAX_LENGTH_PROTOCOL_SIZE	1032	// Look at: https://publibike.atlassian.net/wiki/spaces/SYS/pages/42273869/Internal+Communication
#define IPED_LENGTH_BUFFER				1100	// Buffer greater than: IPED_MAX_LENGTH_PROTOCOL_SIZE include reserve
#define IPED_MOVE_SIZE					(IPED_LENGTH_BUFFER - 10)	//

#define ERASE_SYNC	0x01
#define SEARCH_SYNC	0x00

#define ENCODE_ADD_CMD			0x01
#define ENCODE_ADD_CONTAINER	0x02
#define ENCODE_ADD_SYNC			0x03

#define COUNTDOWN	40

/* --------------------------------------------------------------------------+
|  function prototypes for C                                                 |
+-------------------------------------------------------------------------- */
// Decode
uint32_t interchange_protocol_init (IPED_base_struct *iped_base, uint8_t *pu8_iped_data, uint16_t u16length, uint32_t u32position);
uint32_t interchange_protocol_search_sync (uint8_t *pu8_data, uint16_t u16length, uint32_t *u32position, uint8_t u8_flag);
uint32_t interchange_protocol_decode (uint8_t *pu8_data, uint16_t u16length, uint32_t *u32pos, uint8_t *au8_Buffer);

// function
uint32_t interchange_protocol_check_container (Interchange_Protocol_Header *iph);
uint32_t interchange_protocol_move (uint8_t *pu8_data, uint16_t u16length, uint32_t u32jump);

// Encode
uint32_t interchange_protocol_encode (uint8_t u8cmd, uint16_t u16Type, uint8_t *au8_text_to_encode, uint16_t u16srclength, uint8_t *au8_encoded_data, uint16_t *u16dstlength);


/*--------------------------------------------------------------------------+
|  Macro                                                |
+--------------------------------------------------------------------------*/
#if defined (ARCH_ARM_OPENWRT) || defined (ARCH_X86) || defined (ARM_ST32)
uint32_t HUMANREADABLE_32 (uint32_t u32value);
uint16_t HUMANREADABLE_16 (uint16_t u16value);
#endif

#if defined (ARCH_ARM)
#define	HUMANREADABLE_16(n)	(n)
#define	HUMANREADABLE_32(n)	(n)
#endif

#ifdef __cplusplus
}
#endif

#endif // __INTERCHANGE_PROTOCOL_ENCODE_DECODE_H
/* eof */
