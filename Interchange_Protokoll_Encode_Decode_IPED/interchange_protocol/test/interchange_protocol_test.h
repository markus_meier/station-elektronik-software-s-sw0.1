/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: Test
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/Publibike/Fahrradausleihstation/Interchange_Protocol/trunk/test/interchange_protocol_test.h $
 *  $Revision: 20378 $
 *      $Date: 2017-08-24 15:54:47 +0200 (Do, 24 Aug 2017) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 *************************************************************************** */

#ifndef INTERCHANGE_PROTOCOL_TEST_INTERCHANGE_PROTOCOL_TEST_H_
#define INTERCHANGE_PROTOCOL_TEST_INTERCHANGE_PROTOCOL_TEST_H_

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  global constants                                                         |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  function prototypes for C                                                |
+--------------------------------------------------------------------------*/
void interchange_protocol_test (void);


#ifdef __cplusplus
}
#endif

#endif /* INTERCHANGE_PROTOCOL_TEST_INTERCHANGE_PROTOCOL_TEST_H_ */
/* eof */
