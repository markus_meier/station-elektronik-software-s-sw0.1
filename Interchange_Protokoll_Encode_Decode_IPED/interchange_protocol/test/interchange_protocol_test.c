/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: Test
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/Publibike/Fahrradausleihstation/Interchange_Protocol/trunk/test/interchange_protocol_test.c $
 *  $Revision: 20378 $
 *      $Date: 2017-08-24 15:54:47 +0200 (Do, 24 Aug 2017) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************

/* pragma's and makefile-generated #define's ------------------------------*/

/* headers for this module ------------------------------------------------*/

/* standard-headers (ANSI) ------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* headers from other modules ---------------------------------------------*/
#include "interchange_protocol_test.h"
#include "../interchange_protocol_type.h"
#include "../interchange_protocol_error.h"
#include "../interchange_protocol_encode_decode.h"
#include "../interchange_protocol_debug.h"


/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  module-global variables                                                  |
+--------------------------------------------------------------------------*/
/*
 * Test String
 */
uint8_t au8_text_ok[] = { 0x01, 0x33, 0x33, 0x55, 0x55, 0x00, 0x08, 0x00, 0x04, 0x00, 0x00, 0x00, 0x01, 0x3B, 0xB4, 0x01, 0x04, 0x07, 0x55, 0x55, 0x00, 0x02, 0x00, 0x1C, 0x39, 0x1D, 0x00, 0x08, 0x00, 0x04, 0x00, 0x00, 0x00, 0x01, 0x3B, 0xB4, 0x00, 0x05, 0x00, 0x02, 0x00, 0x09, 0xD2, 0x0E, 0x00, 0x08, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x2B, 0x95, 0xFE, 0xFD, 0xFE, 0xFD, 0xFE, 0xFD, 0xFC, 0xFB, 0xFA, 0xFF, 0xFF, 0xFF };

// defect end
uint8_t au8_text_defect_A[] = { 0x01, 0x33, 0x33, 0x55, 0x55, 0x00, 0x08, 0x00, 0x04, 0x00, 0x00, 0x00, 0x01, 0x3B, 0xB4, 0x01, 0x04, 0x07, 0x55, 0x55, 0x00, 0x02, 0x00, 0x1C, 0x39, 0x1D, 0x00, 0x08, 0x00, 0x04, 0x00, 0x00, 0x00, 0x01, 0x3B, 0xB4, 0x00, 0x05, 0x00, 0x02, 0x00, 0x09, 0xD2, 0x0E, 0x00, 0x08, 0x00, 0x04, 0x00, 0x00, 0xFD, 0xFE, 0xFD, 0xFE, 0xFD, 0xFC, 0xFB, 0xFA, 0xFD, 0xFE, 0xFD, 0xFE, 0xFD, 0xFC, 0xFB, 0xFA, 0xFD, 0xFE, 0xFD, 0xFE, 0xFD, 0xFC, 0xFB, 0xFA };

// container not finished, continue with a new CMD
uint8_t au8_text_defect_B[] = { 0x01, 0x33, 0x33, 0x55, 0x55, 0x00, 0x08, 0x00, 0x04, 0x00, 0x00, 0x00, 0x01, 0x3B, 0xB4, 0x01, 0x04, 0x07, 0x55, 0x55, 0x00, 0x02, 0x00, 0x1C, 0x39, 0x1D, 0x00, 0x08, 0x00, 0x04, 0x00, 0x00, 0x00, 0x01, 0x3B, 0xB4, 0x00, 0x05, 0x55, 0x55, 0x00, 0x08, 0x00, 0x04, 0x00, 0x00, 0x00, 0x01, 0x3B, 0xB4 };

uint8_t au8_text_defect_C[] = { 0x55, 0x55, 0x00, 0x0e, 0x00, 0x3a, 0x08, 0xd8, 0x00, 0x24, 0x00, 0x18, 0x30, 0x30, 0x31, 0x64, 0x30, 0x30, 0x33, 0x30, 0x35, 0x37, 0x33, 0x36, 0x35, 0x30, 0x31, 0x31, 0x32, 0x30, 0x33, 0x32, 0x33, 0x35, 0x33, 0x36, 0xa7, 0x2b, 0x00, 0x05, 0x00, 0x02, 0x1d, 0xdf, 0x0c, 0x9a, 0x00, 0x05, 0x00, 0x02, 0x00, 0x00, 0x43, 0x27, 0x00, 0x1b, 0x00, 0x06, 0x56, 0x30, 0x2e, 0x32, 0x2e, 0x31, 0xdb, 0x88 };

uint8_t au8_text_to_encode_A[] = { 0x00, 0x00, 0x00, 0x01 };

uint8_t au8_text_to_encode_B[] = { 0x00, 0x07 };

uint8_t au8_text_to_encode_C[] = { 0x01, 0x02, 0x03, 0x04 };


/*--------------------------------------------------------------------------+
|  declaration of local functions (helper functions)                        |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/**
 * Callback: TYPE_CONTAINER_GENERIC_1
 */
uint32_t test_callback_TYPE_DEFAULT (Interchange_Protocol_Header *iph)
{
	TRACE_DBG ("CALL TYPE_DEFAULT\n");

	return ERR_OK;
}



/*-------------------------------------------------------------------------*/
/**
 * Callback: TYPE_VEHICLE_TYPE_1
 */
uint32_t test_callback_TYPE_VEHICLE_TYPE_1 (Interchange_Protocol_Header *iph)
{
	if (4 >= HUMANREADABLE_16 (iph->u16Length))
	{
		switch (HUMANREADABLE_32 (*(uint32_t *)iph->au8Data))
		{
			case 0:
			{
				TRACE_DBG ("CALL TYPE_VEHICLE_TYPE_1: Bike\n");
			}
			break;
			case 1:
			{
				TRACE_DBG ("CALL TYPE_VEHICLE_TYPE_1: E-Bike\n");
			}
			break;
			default:
			{
				TRACE_DBG ("CALL TYPE_VEHICLE_TYPE_1: 0x%08X\n", (unsigned int)(HUMANREADABLE_32 (*(uint32_t *)iph->au8Data)));
			}
		}
	}

	return ERR_OK;
}


/*-------------------------------------------------------------------------*/
/**
 * Callback: TYPE_SCHLOSS_BATTERY_1
 */
uint32_t test_callback_TYPE_SCHLOSS_BATTERY_1 (Interchange_Protocol_Header *iph)
{
	if (2 >= HUMANREADABLE_16 (iph->u16Length))
	{
		TRACE_DBG ("CALL TYPE_SCHLOSS_BATTERY_1: %u\n", (unsigned int)HUMANREADABLE_16 (*(uint32_t *)iph->au8Data));
	}

	return ERR_OK;
}


/*-------------------------------------------------------------------------*/
/**
 * Callback: TYPE_CONTAINER_GENERIC_1
 */
uint32_t test_callback_TYPE_CONTAINER_GENERIC_1 (Interchange_Protocol_Header *iph)
{
	TRACE_DBG ("CALL TYPE_CONTAINER_GENERIC_1\n");

	return ERR_OK;
}


/*-------------------------------------------------------------------------*/
/**Example
 */
void interchange_protocol_test (void)
{
	uint32_t		u32_err = ERR_ERROR;
	uint32_t		u32_position = 0;
	uint16_t		u16_length = 0;
	uint32_t		u32x = 0;
	uint8_t			au8_encoded_data[IPED_LENGTH_BUFFER];
	uint8_t			au8_Buffer[IPED_LENGTH_BUFFER];

	/*-------------------------------------------------------------------------*/

	// Reset functions
	interchange_cmd_function_init();

	// Set default function
	interchange_set_function (test_callback_TYPE_DEFAULT, TYPE_DEFAULT);

	// Set functions
	interchange_set_function (test_callback_TYPE_SCHLOSS_BATTERY_1, TYPE_SCHLOSS_BATTERY_1);
	interchange_set_function (test_callback_TYPE_VEHICLE_TYPE_1, TYPE_VEHICLE_TYPE_1);
	interchange_set_function (test_callback_TYPE_CONTAINER_GENERIC_1, TYPE_CONTAINER_GENERIC_1);

	/*-------------------------------------------------------------------------*/

	// Decode Data
	u32_err = interchange_protocol_decode (au8_text_ok, sizeof(au8_text_ok), &u32_position, au8_Buffer);

	// print ERROR Code
	TRACE_DBG ("ERR_CODE: %u - %u\n", (unsigned int)u32_err, (unsigned int)u32_position);
	TRACE_DBG ("*************************************\n");

	/*-------------------------------------------------------------------------*/

	// Decode Data
	u32_err = interchange_protocol_decode (au8_text_defect_A, sizeof(au8_text_defect_A), &u32_position, au8_Buffer);

	// print ERROR Code
	TRACE_DBG ("ERR_CODE: %u - %u\n", (unsigned int)u32_err, (unsigned int)u32_position);
	TRACE_DBG ("*************************************\n");

	/*-------------------------------------------------------------------------*/
	// Decode Data
	u32_err = interchange_protocol_decode (au8_text_defect_B, sizeof(au8_text_defect_B), &u32_position, au8_Buffer);

	// print ERROR Code
	TRACE_DBG ("ERR_CODE: %u - %u\n", (unsigned int)u32_err, (unsigned int)u32_position);
	TRACE_DBG ("*************************************\n");

	/*-------------------------------------------------------------------------*/

	// Encode Data - CMD
	u16_length = 0;
	u32_err = interchange_protocol_encode (ENCODE_ADD_CMD, TYPE_VEHICLE_TYPE_1, au8_text_to_encode_A, 0x04, au8_encoded_data, &u16_length);
	u32_err = interchange_protocol_encode (ENCODE_ADD_SYNC, TYPE_DEFAULT, NULL, 0x00, au8_encoded_data, &u16_length);

	// Show the data
	TRACE_DBG ("HEX: ");
	for (u32x = 0; u32x < u16_length; u32x++)
	{
		TRACE_DBG ("%02X ", au8_encoded_data[u32x]);
	}
	TRACE_DBG ("\n");
	TRACE_DBG ("*************************************\n");

	/*-------------------------------------------------------------------------*/

	// Decode Data
	u32_err = interchange_protocol_decode (au8_encoded_data, sizeof(au8_encoded_data), &u32_position, au8_Buffer);

	// print ERROR Code
	TRACE_DBG ("ERR_CODE: %u - %u\n", (unsigned int)u32_err, (unsigned int)u32_position);
	TRACE_DBG ("*************************************\n");

	/*-------------------------------------------------------------------------*/

	// Encode Data - CMD
	u16_length = 0;
	u32_err = interchange_protocol_encode (ENCODE_ADD_CMD, TYPE_VEHICLE_TYPE_1, au8_text_to_encode_A, 0x04, au8_encoded_data, &u16_length);
	u32_err = interchange_protocol_encode (ENCODE_ADD_CMD, TYPE_SCHLOSS_BATTERY_1, au8_text_to_encode_B, 0x02, au8_encoded_data, &u16_length);
	u32_err = interchange_protocol_encode (ENCODE_ADD_CONTAINER, TYPE_CONTAINER_GENERIC_1, NULL, 0x00, au8_encoded_data, &u16_length);
	u32_err = interchange_protocol_encode (ENCODE_ADD_SYNC, TYPE_DEFAULT, NULL, 0x00, au8_encoded_data, &u16_length);

	// Show the data
	TRACE_DBG ("HEX: ");
	for (u32x = 0; u32x < u16_length; u32x++)
	{
		TRACE_DBG ("%02X ", au8_encoded_data[u32x]);
	}
	TRACE_DBG ("\n");
	TRACE_DBG ("*************************************\n");

	/*-------------------------------------------------------------------------*/

	// Decode Data
	u32_err = interchange_protocol_decode (au8_encoded_data, sizeof(au8_encoded_data), &u32_position, au8_Buffer);

	// print ERROR Code
	TRACE_DBG ("ERR_CODE: %u - %u\n", (unsigned int)u32_err, (unsigned int)u32_position);
	TRACE_DBG ("*************************************\n");

	/*-------------------------------------------------------------------------*/

	// Encode Data - CMD
	u16_length = 0;
	u32_err = interchange_protocol_encode (ENCODE_ADD_CMD, TYPE_PROTOCOL_VERSION, au8_text_to_encode_C, 0x04, au8_encoded_data, &u16_length);
	u32_err = interchange_protocol_encode (ENCODE_ADD_CONTAINER, TYPE_CONTAINER_GENERIC_1, NULL, 0x00, au8_encoded_data, &u16_length);
	u32_err = interchange_protocol_encode (ENCODE_ADD_SYNC, TYPE_DEFAULT, NULL, 0x00, au8_encoded_data, &u16_length);

	// Show the data
	TRACE_DBG ("HEX: ");
	for (u32x = 0; u32x < u16_length; u32x++)
	{
		TRACE_DBG ("%02X ", au8_encoded_data[u32x]);
	}
	TRACE_DBG ("\n");
	TRACE_DBG ("*************************************\n");

	/*-------------------------------------------------------------------------*/

	// Decode Data
	u32_err = interchange_protocol_decode (au8_encoded_data, sizeof(au8_encoded_data), &u32_position, au8_Buffer);

	// print ERROR Code
	TRACE_DBG ("ERR_CODE: %u - %u\n", (unsigned int)u32_err, (unsigned int)u32_position);
	TRACE_DBG ("*************************************\n");

	/*-------------------------------------------------------------------------*/
/*
	// Decode Data
	u32_err = interchange_protocol_decode (au8_text_defect_C, sizeof(au8_encoded_data), &u32_position, au8_Buffer);

	// print ERROR Code
	TRACE_DBG ("ERR_CODE: %u - %u\n", (unsigned int)u32_err, (unsigned int)u32_position);
	TRACE_DBG ("*************************************\n");
*/
}





/* eof */
