/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: CMD_FUNCTION_H
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/Publibike/Fahrradausleihstation/Interchange_Protocol/trunk/interchange_protocol_cmd_function.h $
 *  $Revision: 20378 $
 *      $Date: 2017-08-24 15:54:47 +0200 (Do, 24 Aug 2017) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 *************************************************************************** */

#ifndef __INTERCHANGE_PROTOCOL_CMD_FUNCTION_H
#define __INTERCHANGE_PROTOCOL_CMD_FUNCTION_H

#include "interchange_protocol_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/
typedef uint32_t (*IPED_call_function)(Interchange_Protocol_Header *iph);


/*--------------------------------------------------------------------------+
|  global constants                                                         |
+--------------------------------------------------------------------------*/
typedef struct tag_Interchange_function_call
{
	IPED_call_function function;                       ///< function
} Interchange_function_call;


/*--------------------------------------------------------------------------+
|  function prototypes for C                                                |
+--------------------------------------------------------------------------*/
uint32_t interchange_cmd_function_init (void);
uint32_t interchange_set_function (void *function, uint16_t u16type);
uint32_t interchange_call_function (uint16_t u16type, Interchange_Protocol_Header *iph);


#ifdef __cplusplus
}
#endif

#endif // __INTERCHANGE_PROTOCOL_CMD_FUNCTION_H
/* eof */
