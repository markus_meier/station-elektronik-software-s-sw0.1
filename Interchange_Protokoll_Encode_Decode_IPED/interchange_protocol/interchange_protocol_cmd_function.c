/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: Type and Function
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/Publibike/Fahrradausleihstation/Interchange_Protocol/trunk/interchange_protocol_cmd_function.c $
 *  $Revision: 20378 $
 *      $Date: 2017-08-24 15:54:47 +0200 (Do, 24 Aug 2017) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************

/* pragma's and makefile-generated #define's ------------------------------*/

/* headers for this module ------------------------------------------------*/

/* standard-headers (ANSI) ------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* headers from other modules ---------------------------------------------*/
#include "interchange_protocol_cmd_function.h"
#include "interchange_protocol_type.h"
#include "interchange_protocol_encode_decode.h"
#include "interchange_protocol_debug.h"


/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  module-global variables                                                  |
+--------------------------------------------------------------------------*/
Interchange_function_call	IFC[TYPE_LAST];


/*--------------------------------------------------------------------------+
|  declaration of local functions (helper functions)                        |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/**
 * init function table
 */
uint32_t interchange_cmd_function_init (void)
{
	uint32_t	u32x;

	for (u32x = TYPE_DEFAULT; u32x < TYPE_LAST; u32x++)
	{
		IFC[u32x].function = NULL;
	}

	return ERR_OK;
}


/*-------------------------------------------------------------------------*/
/**
 * Reset function table
 */
uint32_t interchange_set_function (void *function, uint16_t u16type)
{
	uint32_t	u32_err = ERR_ERROR;

	if ( (u16type < TYPE_LAST) && (u16type > TYPE_DEFAULT) )
	{
		IFC[u16type].function = function;
		u32_err = ERR_OK;
	}
	else if (u16type == TYPE_DEFAULT)
	{
		IFC[u16type].function = function;
		u32_err = ERR_OK;
	}

	return u32_err;
}


/*-------------------------------------------------------------------------*/
/**
 * Call function
 */
uint32_t interchange_call_function (uint16_t u16type, Interchange_Protocol_Header *iph)
{
	uint32_t	u32_err = ERR_ERROR;

	TRACE_DBG ("call_function: %x\n", u16type);

	if ( (u16type < TYPE_LAST) && (u16type > TYPE_DEFAULT) )
	{
		if (IFC[u16type].function == NULL)
		{
			if (IFC[TYPE_DEFAULT].function != NULL)
			{
				u32_err = IFC[TYPE_DEFAULT].function (iph);
			}
		}
		else
		{
			u32_err = IFC[u16type].function (iph);
		}
	}
	else // TYPE_DEFAULT
	{
		if (IFC[TYPE_DEFAULT].function != NULL)
		{
			u32_err = IFC[TYPE_DEFAULT].function (iph);
		}
	}

	return u32_err;
}




/* eof */
