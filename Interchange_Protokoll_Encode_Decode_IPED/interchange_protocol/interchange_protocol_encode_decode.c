/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: interchange_protocol_encode_decode
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/Publibike/Fahrradausleihstation/Interchange_Protocol/trunk/interchange_protocol_encode_decode.c $
 *  $Revision: 20380 $
 *      $Date: 2017-08-24 16:21:39 +0200 (Do, 24 Aug 2017) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 **************************************************************************** */

/* pragma's and makefile-generated #define's ------------------------------*/

/* headers for this module ------------------------------------------------*/

/* standard-headers (ANSI) ------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* headers from other modules ---------------------------------------------*/
#include "interchange_protocol_encode_decode.h"
#include "interchange_protocol_debug.h"
#include "interchange_protocol_type.h"

/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  module-global variables                                                  |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  declaration of local functions (helper functions)                        |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/
#if defined (ARCH_ARM_OPENWRT) || defined (ARCH_X86) || defined (ARM_ST32)
uint32_t HUMANREADABLE_32 (uint32_t u32value)
{
	uint32_t u32result = 0;

	u32result |= (u32value & 0x000000FF) << 24;
	u32result |= (u32value & 0x0000FF00) << 8;
	u32result |= (u32value & 0x00FF0000) >> 8;
	u32result |= (u32value & 0xFF000000) >> 24;

	return u32result;
}

uint16_t HUMANREADABLE_16 (uint16_t u16value)
{
	uint16_t u16result = 0;

	u16result |= (u16value & 0x00FF) << 8;
	u16result |= (u16value & 0xFF00) >> 8;

	return u16result;
}
#endif

uint32_t interchange_protocol_check_container (Interchange_Protocol_Header *iph)
{
	uint32_t u32_err = ERR_ERROR;

	switch (HUMANREADABLE_16(iph->u16Type))
	{
		case TYPE_CONTAINER_GENERIC_1:
		case TYPE_SEARCH_FOR_SMARTPHONE_1:
		case TYPE_SEARCH_FOR_STATION_1:
		case TYPE_UNLOCK_APP_1:
		case TYPE_UNLOCK_STATION_1:
		case TYPE_RENT_APP_1:
		case TYPE_RENT_STATION_1:
		case TYPE_APP_1:
		case TYPE_CREDENTIALS_1:
		case TYPE_SMARTLOCK_STATUS_1:
		case TYPE_RENTAL_RETURN_1:
		case TYPE_RESPONSE_SECURE_1:
		case TYPE_LOCK_CONFIG_1:
		{
			u32_err = ERR_OK;
		}
		break;
		default:
		{
			u32_err = ERR_ERROR;
		}
	}

	return u32_err;
}


/*-------------------------------------------------------------------------*/
/**
 * CRC - CRC-CCITT (0xFFFF) - https://www.lammertbies.nl/comm/info/crc-calculation.html
 */
static uint16_t crc16 (const uint8_t* data_p, uint8_t u8_length)
{
	uint8_t	u8x;
	uint16_t	u16crc = 0xFFFF;

	TRACE_DBG ("LENGTH %u - ", u8_length);

	while (u8_length--)
	{
		TRACE_DBG ("%02X ", *data_p);

		u8x = u16crc >> 8 ^ *data_p++;
		u8x ^= u8x >> 4;
		u16crc = (u16crc << 8) ^ ((uint16_t)(u8x << 12)) ^ ((uint16_t)(u8x << 5)) ^ ((uint16_t)u8x);
	}

	TRACE_DBG ("CRC16: %04X\n", u16crc);
	return u16crc;
}


static uint32_t interchange_protocol_cmd_crc16 (Interchange_Protocol_Header *iph, uint16_t u16crc)
{
	uint32_t u32_err = ERR_AGAIN;
	uint16_t u16length = 0;

	TRACE_DBG ("CRC16: %04X\n", u16crc);

	u16length = HUMANREADABLE_16(iph->u16Length) + IPH_MIN_SIZE;

	if (crc16 ((uint8_t *)iph, u16length) == u16crc)
	{
		u32_err = ERR_OK;
	}

	return u32_err;
}


static uint32_t interchange_protocol_container_crc16 (Interchange_Protocol_Header *iph, uint16_t u16crc)
{
	uint32_t u32_err = ERR_AGAIN;

	TRACE_DBG ("CRC16: %04X\n", u16crc);

	if (crc16 ((uint8_t *)iph, 0x04) == u16crc)
	{
		u32_err = ERR_OK;
	}

	return u32_err;
}


/*-------------------------------------------------------------------------*/
/**
 * Check Buffer size
 */
static uint32_t interchange_protocol_check_size (IPED_base_struct *iped_base, Interchange_Protocol_Header *iph)
{
	uint32_t u32_err = ERR_ERROR;
	uint16_t u16length = HUMANREADABLE_16(iph->u16Length);

	TRACE_DBG ("CHECK SIZE: length %u : %u\n", (unsigned int)u16length, (unsigned int)iped_base->u16Bufferlength);
	
	if ( IPED_MAX_LENGTH_PROTOCOL_SIZE >= u16length )
	{
		if (iped_base->u16Bufferlength >= (u16length + IPED_SYNC_HEADER_SIZE))
		{
			TRACE_DBG ("CHECK SIZE: OK\n");
			u32_err = ERR_OK;
		}
		else
		{	// Out of memory
			TRACE_DBG ("CHECK SIZE: ERR_WAITING_FOR_DATA\n");
			u32_err = ERR_WAITING_FOR_DATA;
		}
	}
	else
	{
		u32_err = ERR_WRONG_DATA_SIZE;
	}

	return u32_err;
}


/*-------------------------------------------------------------------------*/
/**
 * Reset working struct
 */
static uint32_t interchange_protocol_reset (IPED_base_struct *iped_base)
{
	memset (iped_base, 0xFF, sizeof(IPED_base_struct));
	return (ERR_OK);
}


/*-------------------------------------------------------------------------*/
/**
 * Search Sync
 */
uint32_t interchange_protocol_search_sync (uint8_t *pu8_data, uint16_t u16length, uint32_t *u32position, uint8_t u8_flag)
{
	uint32_t u32_err = ERR_NO_SYNC_FOUND;
	uint16_t u16Sync = 0;
	uint16_t u16pos = 0;
	Interchange_Protocol_Header *iph;
	IPED_base_struct iped_base_local;

	if (u16length > IPED_SYNC_HEADER_SIZE)
	{
		// Go through the buffer
		for (u16pos = 0; u16pos < u16length; u16pos++)
		{
			// Check 0xFF

			if (pu8_data[u16pos] == (IPED_SYNC & 0xFF) )
			{
				if (u32_err == ERR_SYNC_PARTLY)
				{	// SYNC found
					if (u8_flag == SEARCH_SYNC)
					{
						TRACE_DBG ("SYNC FOUND\n");
						u32_err = ERR_OK;					// Sync found
						break;
					}
					else if (u8_flag == ERASE_SYNC)
					{
						TRACE_DBG ("SYNC ERASE\n");
						pu8_data[u16pos - 1] = 0x00;		// Erase sync
						pu8_data[u16pos    ] = 0x00;
						u32_err = ERR_OK;
						break;
					}
				}
				else
				{	// SYNC partly
					TRACE_DBG ("SYNC PARTLY\n");
					u32_err = ERR_SYNC_PARTLY;			// First part found
				}
			}
			// Set the position always
			*u32position = u16pos;
		}
	}
	else
	{
		TRACE_DBG ("SYNC ERR_WAITING_FOR_DATA\n");
		u32_err = ERR_WAITING_FOR_DATA;
	}

	return u32_err;
}


/*-------------------------------------------------------------------------*/
/**
 * Initialization
 */
uint32_t interchange_protocol_init (IPED_base_struct *iped_base, uint8_t *pu8_iped_data, uint16_t u16length, uint32_t u32position)
{
	uint32_t u32_err = ERR_WAITING_FOR_DATA;

	u32position = u32position + IPED_SYNC_SIZE;

	TRACE_DBG ("protocol init: %u >= %u\n", (unsigned int)u16length, (unsigned int)IPED_SYNC_HEADER_SIZE);

	if (u16length >= IPED_SYNC_HEADER_SIZE )
	{
		TRACE_DBG ("PASS 2.1\n");
		if (ERR_OK == interchange_protocol_reset (iped_base))
		{
			if (u16length > u32position)
			{
				TRACE_DBG ("PASS 2.2\n");
				// Fill the struct with data
				iped_base->pu8_iped_data = pu8_iped_data + u32position;
				iped_base->u16Bufferlength = u16length - u32position;
				iped_base->u16Container_length = 0;
				iped_base->iph = NULL;
				iped_base->iph_base_addr = NULL;
				iped_base->u32position = 0;
				TRACE_DBG ("protocol init u16Bufferlength: %u u16length: %u u32position %u \n", (unsigned int)iped_base->u16Bufferlength, (unsigned int)u16length, (unsigned int)u32position);
				u32_err = ERR_OK;
			}
		}
	}

	return u32_err;
}


/*-------------------------------------------------------------------------*/
/**
 * Decode cmd
 */
uint32_t interchange_protocol_decode_cmd (Interchange_Protocol_Header *iph)
{
	uint32_t	u32_err = ERR_ERROR;
	uint16_t	u16crc = 0;

	// It's a CMD
	TRACE_DBG ("CMD found - check CRC\n");
	// get crc16
	u16crc = ((uint16_t *) (((uint8_t *)iph) + HUMANREADABLE_16(iph->u16Length) + IPH_MIN_SIZE))[0];
	// Check CRC
	u32_err = interchange_protocol_cmd_crc16 (iph, HUMANREADABLE_16(u16crc));
	if (ERR_OK == u32_err)
	{
		u32_err = interchange_call_function (HUMANREADABLE_16(iph->u16Type), iph);
	}

	return u32_err;
}


/*-------------------------------------------------------------------------*/
/**
 * Next CMD
 */
uint32_t interchange_protocol_next_cmd (IPED_base_struct *iped_base)
{
	uint32_t u32_err = ERR_OK;

	iped_base->iph = (Interchange_Protocol_Header *)  (((uint8_t *)iped_base->iph) + sizeof(Interchange_Protocol_Header) + HUMANREADABLE_16(iped_base->iph->u16Length) + IPH_CRC_SIZE);

	return u32_err;
}


/*-------------------------------------------------------------------------*/
/**
 * Main function - Start working
 */
static uint32_t interchange_protocol_decode_start (IPED_base_struct *iped_base)
{
	uint32_t u32_err = ERR_ERROR;
	uint32_t u32count = COUNTDOWN;
	uint16_t u16Sync = 0;
	uint16_t u16crc = 0;
	Interchange_Protocol_Header *iph;
	IPED_base_struct iped_base_local;

	TRACE_DBG ("PASS 3.1\n");

	// check min size
	iph = (Interchange_Protocol_Header *)iped_base->pu8_iped_data;
	TRACE_DBG ("PASS 3.2 - Bufferlength: %u\n", (unsigned int)iped_base->u16Bufferlength);
	if (iped_base->u16Bufferlength >= IPED_SYNC_HEADER_SIZE)
	{
		TRACE_DBG ("PASS 3.3\n");
		// Check size
		u32_err = interchange_protocol_check_size (iped_base, iph);
		TRACE_DBG ("PASS 3.4\n");
		if (ERR_OK == u32_err)
		{
			u32_err = interchange_protocol_check_container (iph);
			if (ERR_OK == u32_err)
			{	// *******************************************
				// Yes, it's a container
				// *******************************************
				TRACE_DBG ("Container found\n");
				u32_err = interchange_protocol_container_crc16 (iph, HUMANREADABLE_16(((uint16_t *) (((uint8_t *)iph) + IPH_MIN_SIZE))[0]) );
				if (ERR_OK == u32_err)
				{
					iped_base->u16Container_length = HUMANREADABLE_16(iph->u16Length);

					// Call Container callback
					u32_err = interchange_call_function (HUMANREADABLE_16(iph->u16Type), iph);
					if (ERR_OK == u32_err)
					{	// decode data inside container
						iped_base->iph = (Interchange_Protocol_Header *)  (((uint8_t *)iph) + sizeof(Interchange_Protocol_Header) + IPH_CRC_SIZE);
						iped_base->iph_base_addr = (uint8_t *) iped_base->iph;

						u32count = COUNTDOWN;
						do
						{
							u32_err = interchange_protocol_decode_cmd (iped_base->iph);
							if (u32_err == ERR_OK)
							{
								u32_err = interchange_protocol_next_cmd (iped_base);
							}
							else
							{
								break;
							}
							u32count--;
						}
						while ( ((void *)iped_base->iph < (void *)(iped_base->iph_base_addr + iped_base->u16Container_length)) && (u32count > 0x00) );
					}
					else if (ERR_CONTAINER_DECODE_STOP == u32_err)
					{	// don't decode container data
						u32_err = ERR_OK;
					}
				}
			}
			else
			{	// *******************************************
				// It's a CMD
				// *******************************************
				TRACE_DBG ("CMD found\n");
				// Decode
				u32_err = interchange_protocol_decode_cmd (iph);
			}
		}
	}
	else
	{
		TRACE_DBG ("PASS 3.e2\n");
		u32_err = ERR_WAITING_FOR_DATA;
	}

	if (u32_err == ERR_OK)
	{
		// Everything OK, and the data are correct decoded.
		u32_err = ERR_DATA_RECEIVED_CORRECT;
	}

	return u32_err;
}


/*-------------------------------------------------------------------------*/
/**
 * Main function - Decode
 */
uint32_t interchange_protocol_decode (uint8_t *pu8_data, uint16_t u16length, uint32_t *u32pos, uint8_t *au8_Buffer)
{
	uint32_t 				u32_err = ERR_ERROR;
	IPED_base_struct		iped_base;
	uint32_t				u32count = COUNTDOWN;

	// prepare Buffer
	if (u16length <= IPED_LENGTH_BUFFER)
	{
		memcpy (au8_Buffer, pu8_data, u16length);

		u32count = COUNTDOWN;
		do
			{
				TRACE_DBG ("PASS 1 - %u\n", u16length);
				u32_err = interchange_protocol_search_sync(au8_Buffer, u16length, u32pos, SEARCH_SYNC);
				if (ERR_OK == u32_err)
				{	// init
					TRACE_DBG ("PASS 2 - pos: %u - length %u\n", (unsigned int)(*u32pos), (unsigned int)u16length);
					if (ERR_OK == interchange_protocol_init (&iped_base, au8_Buffer, u16length, (*u32pos)))
					{	// decode
						TRACE_DBG ("PASS 3\n");
						u32_err = interchange_protocol_decode_start(&iped_base);
						if (ERR_DATA_RECEIVED_CORRECT == u32_err)
						{
							TRACE_DBG ("PASS 4 OK\n");
							interchange_protocol_search_sync(au8_Buffer, u16length, u32pos, ERASE_SYNC);
							interchange_protocol_search_sync(au8_Buffer, u16length, u32pos, SEARCH_SYNC);
						}
						else if (u32_err == ERR_WRONG_DATA_SIZE)
						{
							TRACE_DBG ("PASS 4 WRONG DATA SIZE\n");
							interchange_protocol_search_sync(au8_Buffer, u16length, u32pos, ERASE_SYNC);
						}
						else
						{
							TRACE_DBG ("PASS 4 NOK\n");
						}
					}
				}
				u32count--;
			}
		while ( (u32_err == ERR_OK) && (u32count > 0x00) );
	}
	else
	{
		TRACE_DBG ("PASS 6\n");
		u32_err = ERR_NOMEM;
	}

	return u32_err;
}


/*-------------------------------------------------------------------------*/
/**
 * Main function - move data
 */
uint32_t interchange_protocol_move (uint8_t *pu8_data, uint16_t u16length, uint32_t u32jump)
{
	int32_t i32x = 0;

	if (u16length > 1)
	{
		for (i32x = u16length - 1; i32x >= 0; i32x--)
		{
			pu8_data[i32x + u32jump] = pu8_data[i32x];
		}
	}

	return ERR_OK;
}


/*-------------------------------------------------------------------------*/
/**
 * Main function - Encode
 */
uint32_t interchange_protocol_encode (uint8_t u8cmd, uint16_t u16Type, uint8_t *au8_text_to_encode, uint16_t u16srclength, uint8_t *au8_encoded_data, uint16_t *u16dstlength)
{
	Interchange_Protocol_Header		*iph;
	uint32_t u32_err				= ERR_OK;
	uint16_t u16crc					= 0;

	switch (u8cmd)
	{
		case ENCODE_ADD_CMD:
		{
			TRACE_DBG ("ENCODE_CMD\n");
			iph = (Interchange_Protocol_Header *)(au8_encoded_data + *u16dstlength);
			iph->u16Type = HUMANREADABLE_16(u16Type);
			iph->u16Length = HUMANREADABLE_16(u16srclength);
			memcpy (iph->au8Data, au8_text_to_encode, u16srclength);
			u16crc = crc16 ((au8_encoded_data + *u16dstlength), IPH_MIN_SIZE + u16srclength);
			((uint16_t *)(au8_encoded_data + (*u16dstlength) + IPH_MIN_SIZE + u16srclength))[0] = HUMANREADABLE_16(u16crc);
			*u16dstlength = (*u16dstlength) + IPH_MIN_SIZE + u16srclength + IPH_CRC_SIZE;
		}
		break;
		case ENCODE_ADD_CONTAINER:
		{
			TRACE_DBG ("ENCODE_CONTAINER type: %u\n", (unsigned int)u16Type);
			interchange_protocol_move (au8_encoded_data, (*u16dstlength), 0x06);
			iph = (Interchange_Protocol_Header *)au8_encoded_data;
			iph->u16Type = HUMANREADABLE_16(u16Type);
			iph->u16Length = HUMANREADABLE_16(*u16dstlength);
			u16crc = crc16 (au8_encoded_data, IPH_MIN_SIZE);
			(((uint16_t *)(au8_encoded_data + IPH_MIN_SIZE)))[0] = HUMANREADABLE_16(u16crc);
			*u16dstlength = (*u16dstlength) + IPH_MIN_SIZE + IPH_CRC_SIZE;
		}
		break;
		case ENCODE_ADD_SYNC:
		{
			TRACE_DBG ("ENCODE_ADD_SYNC\n");
			interchange_protocol_move (au8_encoded_data, (*u16dstlength), 0x02);
			((uint16_t *)au8_encoded_data)[0] = IPED_SYNC;
			*u16dstlength = *u16dstlength + IPED_SYNC_SIZE;
		}
		break;
		default:
			TRACE_DBG ("ERR_ERROR\n");
			u32_err = ERR_ERROR;
	}

	return u32_err;
}


/* eof */
