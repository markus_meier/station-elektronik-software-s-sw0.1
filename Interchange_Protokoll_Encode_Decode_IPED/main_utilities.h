/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: MAIN_UTILITIES_H_
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/Publibike/Fahrradausleihstation/Cmd_Line_Tool/trunk/main_utilities.h $
 *  $Revision: 20248 $
 *      $Date: 2017-07-13 11:28:53 +0200 (Do, 13 Jul 2017) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 *************************************************************************** */

#ifndef MAIN_UTILITIES_H_
#define MAIN_UTILITIES_H_

#include "interchange_protocol_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  global constants                                                         |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  function prototypes for C                                                |
+--------------------------------------------------------------------------*/
uint8_t *hextobin (uint8_t *pu8_Buffer, int32_t i32Blength, uint8_t *pu8_data, int32_t i32length);


#ifdef __cplusplus
}
#endif

#endif /* MAIN_UTILITIES_H_ */
/* eof */
