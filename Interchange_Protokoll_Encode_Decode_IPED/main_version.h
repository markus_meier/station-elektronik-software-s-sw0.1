/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Publibike
 *
 *     Module: main
 *      State:
 * Originator: Schwab
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/Publibike/Fahrradausleihstation/Cmd_Line_Tool/trunk/main_version.h $
 *  $Revision: 20284 $
 *      $Date: 2017-07-19 18:50:32 +0200 (Mi, 19 Jul 2017) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 **************************************************************************** */

#ifndef VERS_H
#define VERS_H

/* Includes needed by this header  ----------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
|  global constants                                                         |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/
#define 	VERSION_CMD_LINE_SW		"1.3.3"

/*--------------------------------------------------------------------------+
|  Open Issues / Known Limitations                                          |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  Change List (tags im SVN repository)                                     |
+--------------------------------------------------------------------------*/

/* @file */

/*

Version 1.0.0
-------------
First stable Release.

Version 1.0.1
-------------
Add example Receive.

Version 1.0.2
-------------
Add version.h
Default port changed to: /dev/ttyS2

Version 1.1.0
-------------
Add ERR_CONTAINER_DECODE_STOP to stop decode data, inside a container

Version 1.1.1
-------------
Corrections in Buffer size: IPED_MAX_LENGTH_PROTOCOL_SIZE

Version 1.1.2
-------------
Data output now with 1 single line. Data separated with: ";"
Line end with "\r\n"

Version 1.1.3
-------------
New ERR code for correct decoding.
Buffer handling a bug detectet for the Linux Version.

Version 1.1.4
-------------
Bugfix Container Data output.

Version 1.1.5
-------------
Modified to interact with Kernel.

Version 1.1.6
-------------
Changed to stdint
Bugfixed: interchange_protocol_encode -> error return code.

Version 1.1.7
-------------
Memory access modified

Version 1.1.8
-------------
Length check, hex to bin removed.

Version 1.2.0
-------------
HEX output as humanreadable defined!!

Version 1.2.1
-------------
Bad CRC return ERR_AGAIN

Version 1.2.2
-------------
Liste der Container erweitert.

Version 1.3.0
-------------
Start FW update integration.

Version 1.3.1
-------------
FW update multi device

Version 1.3.2
-------------
Protokoll ihex modified, every answer takes as a ERR_OK for firmware update

Version 1.3.3
-------------
FW Update system call replaced with cmd line call, Timeout to send next cmd 10s

*/


#ifdef __cplusplus
#endif


#endif // VERS_H

/* eof */
