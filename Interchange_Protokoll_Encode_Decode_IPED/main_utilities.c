/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Publibike
 *
 *     Module: main_utilities
 *      State:
 * Originator: Schwab
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/Publibike/Fahrradausleihstation/Cmd_Line_Tool/trunk/main_utilities.c $
 *  $Revision: 20248 $
 *      $Date: 2017-07-13 11:28:53 +0200 (Do, 13 Jul 2017) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************

/* pragma's and makefile-generated #define's ------------------------------*/

/* headers for this module ------------------------------------------------*/

/* standard-headers (ANSI) ------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* headers from other modules ---------------------------------------------*/
#include "main_utilities.h"
#include "interchange_protocol_type.h"
#include "interchange_protocol_debug.h"


/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  module-global variables                                                  |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  declaration of local functions (helper functions)                        |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/**
 * Hex to Bin Sub function
 */
uint8_t hextobin_sub (uint8_t u8x)
{
	if ( (u8x >= 0) && (u8x <= 9) )
	{
		u8x = 0xF0 & u8x;
	}
	else if ( (u8x >= 'A') && (u8x <= 'F') )
	{
		u8x = u8x - 55;
	}
	else if ( (u8x >= 'a') && (u8x <= 'f') )
	{
		u8x = u8x - 87;
	}


	return u8x;
}


/*-------------------------------------------------------------------------*/
/**
 * Hex to Bin
 */
uint8_t *hextobin (uint8_t *pu8_Buffer, int32_t i32Blength, uint8_t *pu8_data, int32_t i32length)
{
	uint32_t  u32x = 0;
	uint8_t   u32c = 0;

	memset (pu8_Buffer, 0x00, i32Blength);

	if ( (pu8_data[0] == '0') && (pu8_data[1] == 'x') )
	{
		for (u32x = 0; u32x < (i32length * 2); u32x = u32x + 2)
		{
			if (u32c < i32Blength)
			{
				pu8_Buffer[u32c] = (0xF0 & (hextobin_sub(pu8_data[u32x + 2]) << 4)) | (0x0F & (hextobin_sub(pu8_data[u32x + 3])) );
				u32c++;
			}
		}
	}

return pu8_Buffer;
}



/* eof */






