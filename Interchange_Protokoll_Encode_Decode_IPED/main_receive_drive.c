/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Publibike
 *
 *     Module: main_receive_drive
 *      State:
 * Originator: Schwab
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/Publibike/Fahrradausleihstation/Cmd_Line_Tool/trunk/main_receive_drive.c $
 *  $Revision: 20259 $
 *      $Date: 2017-07-14 17:55:55 +0200 (Fr, 14 Jul 2017) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************

/* pragma's and makefile-generated #define's ------------------------------*/

/* headers for this module ------------------------------------------------*/

/* standard-headers (ANSI) ------------------------------------------------*/

#include "main_receive_drive.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */

#include <time.h>

/* headers from other modules ---------------------------------------------*/
#include "interchange_protocol_encode_decode.h"
#include "interchange_protocol_type.h"
#include "interchange_protocol_error.h"
#include "interchange_protocol_cmd_function.h"
#include "interchange_protocol_debug.h"


/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  module-global variables                                                  |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  declaration of local functions (helper functions)                        |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/**
 * open drive
 */
int drive_open (uint8_t *pu8device, uint32_t u32flag)
{
    int fd; /* File descriptor for the port */

    fd = open(pu8device, u32flag);
    if (fd == -1)
    {
        perror("open_drive: Unable to open");
    }

    return (fd);
}

/*-------------------------------------------------------------------------*/
/**
 * read drive
 */
int drive_read (int fd, uint8_t *pu8_data, uint32_t u32length)
{
    return (read(fd, pu8_data, u32length));
}


/*-------------------------------------------------------------------------*/
/**
 * close drive
 */
int drive_close (int fd)
{
    return (close(fd));
}

/*-------------------------------------------------------------------------*/
/**
 * pipe
 */
void main_drive_receive_data (uint8_t *pu8_default_drive)
{
    int32_t   fd;
    uint8_t   pu8_data[IPED_LENGTH_BUFFER];
    int32_t   i32_length;
    uint64_t  u64_time;
    int32_t   i32_input_position = 0;

    uint8_t   au8_Buffer[IPED_LENGTH_BUFFER];
    uint8_t   au8_encode_data[IPED_LENGTH_BUFFER];

    // Get data - Serial Port
    fd = drive_open (pu8_default_drive, O_RDONLY);
    if (fd != -1)
    {
        i32_input_position = 0;
        do
        {
            u64_time = time(NULL) + TIMEOUT;
            i32_length = drive_read (fd, (uint8_t *)(au8_Buffer), 1);

            if (i32_length > 0)
            {
                // Timeout
                if (u64_time < time(NULL))
                {
                    TRACE_DBG ("timeout\n");
                    memset (au8_encode_data, 0xFF, IPED_LENGTH_BUFFER);
                    i32_input_position = i32_length;
                }

                if ( (au8_Buffer[0] == '\r') || (au8_Buffer[0] == '\n') )
                {
                    au8_encode_data[i32_input_position++] = 0x00;

                    sprintf (au8_Buffer, "interchange_protocol_encode_decode %s", au8_encode_data);
                    printf ("%s\n", au8_Buffer);
                    system (au8_Buffer);
                    i32_input_position = 0x00;
                }
                else
                {
                    // Add Data
                    au8_encode_data[i32_input_position++] = au8_Buffer[0];
                }
            }
        }
        while (i32_length >= 0);
        drive_close (fd);
    }
}

/* eof */






