#!/bin/bash

STAGING_DIR=/home/user/at91/openwrt/staging_dir
PFAD=$(find $STAGING_DIR/toolchain-arm*/bin/ | grep arm-openwrt-linux-as | sed 's/\/arm-openwrt-linux-as//g')
PATH=$PFAD:$PATH

echo "**********************************"
echo "make clean"
echo "----------"
make clean

echo "**********************************"
echo "make"
echo "----"

if [ "$*" ]; then
	echo "DEBUG ON"
	make CC=arm-openwrt-linux-gcc STAGING_DIR=$PFAD ARCH=openwrt_with_debug
else
	make CC=arm-openwrt-linux-gcc STAGING_DIR=$PFAD ARCH=openwrt
fi

exit
