/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Publibike
 *
 *     Module: main
 *      State: 
 * Originator: Schwab
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/Publibike/Fahrradausleihstation/Cmd_Line_Tool/trunk/main.c $
 *  $Revision: 20269 $
 *      $Date: 2017-07-18 11:50:45 +0200 (Di, 18 Jul 2017) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 **************************************************************************** */

/* pragma's and makefile-generated #define's ------------------------------*/

/* headers for this module ------------------------------------------------*/

/* standard-headers (ANSI) ------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

/* headers from other modules ---------------------------------------------*/
#include "interchange_protocol_encode_decode.h"
#include "interchange_protocol_type.h"
#include "interchange_protocol_error.h"
#include "interchange_protocol_cmd_function.h"
#include "interchange_protocol_debug.h"
#include "interchange_protocol_test.h"
#include "main_serial.h"
#include "main_utilities.h"
#include "main_receive_data.h"
#include "main_receive_drive.h"
#include "main_version.h"
#include "main_read_file.h"

/*
compile x86 for Tests:
----------------------
make clean; make CC=gcc ARCH=openwrt
make clean; make CC=gcc ARCH=arm
make clean; make CC=gcc ARCH=x86

compile Raspberry for Tests:
----------------------------
make clean; make CC=gcc ARCH=raspberry

compile OpenWRT for real use:
-----------------------------
bash compile.sh                   // works only, if the development environment is installed for openwrt.

*/


/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/
#define MAX_NAME_DEFAULT_COM	40

/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  module-global variables                                                  |
+--------------------------------------------------------------------------*/


/*
 * Default Port
 */
unsigned char au8_default_com[MAX_NAME_DEFAULT_COM] = "/dev/ttyS2";
unsigned char au8_default_drive[MAX_NAME_DEFAULT_COM] = "/dev/aot_UART_COM";


/*--------------------------------------------------------------------------+
|  declaration of local functions (helper functions)                        |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/




/*-------------------------------------------------------------------------*/
/**
 * main
 */
int main (int argc, char *argv[])
{
	int			opt = 0;

	int32_t		fd;
	int32_t		i32example = 0;
    int32_t		i32device = 0;
	int32_t   	i32receivedata = 0;
	int32_t   	i32fileread = 0;
	uint8_t		*pu8type = NULL;
	uint8_t		*pu8length = NULL;
	uint8_t		*pu8data = NULL;
	uint8_t		*pu8filename = NULL;
	uint8_t     *pu8Mikrokontroller = NULL;

	uint8_t		au8_encoded_data[IPED_LENGTH_BUFFER];
	uint8_t		au8_Buffer[IPED_LENGTH_BUFFER];
	uint32_t	u32_position = 0;
	uint32_t	u32_err = ERR_ERROR;
	int32_t		i32_length = 0;
	uint16_t	u16_length = 0;
	uint32_t	u32x = 0;

	uint32_t	u32_test = 0x000000FF;
	uint8_t 	*p;
	uint32_t	u32_c;

	/*-------------------------------------------------------------------------
	| decode cmd line data
	-------------------------------------------------------------------------*/
	// Reset Buffer
	memset (au8_encoded_data, 0x00, IPED_LENGTH_BUFFER);

	while ((opt = getopt(argc, argv, "d:p:t:l:ascirxh?eHRvVf:M:")) != -1)
	{
		switch(opt)
		{
			case 'V':
				printf ("Version: %s, %s, %s\n", VERSION_CMD_LINE_SW, __DATE__, __TIME__);
			break;
			case 'v':
				printf ("Version: %s, %s, %s\n", VERSION_CMD_LINE_SW, __DATE__, __TIME__);
			break;
			case 'H':
				printf ("DATA HEX: ");
				// Show the data
				for (u32x = 0; u32x < u16_length; u32x++)
				{
					printf ("%02X ", au8_encoded_data[u32x]);
				}
				printf ("\n");
			break;
			case 'd':
				pu8data = optarg;
				TRACE_DBG ("DATA: %s\n", pu8data);
			break;
			case 'f':
				pu8filename = optarg;
				i32fileread = 1;
				TRACE_DBG ("Filename: %s\n", pu8filename);
			break;
			case 'M':
				pu8Mikrokontroller = optarg;
				TRACE_DBG ("Filename: %s\n", pu8Mikrokontroller);
			break;
			case 'p':
				if (strlen(optarg) < MAX_NAME_DEFAULT_COM)
				{
					memset (au8_default_com, 0x00, MAX_NAME_DEFAULT_COM);
					memcpy (au8_default_com, optarg, strlen(optarg));
					TRACE_DBG ("Default port set to: %s\n", au8_default_com);
				}
				else
				{
					TRACE_DBG ("Default port: name to long\n");
				}
			break;
	    	case 't':
	    		pu8type = optarg;
	    		TRACE_DBG("TYPE: %s\n", pu8type);
	    	break;
	    	case 'l':
	    		pu8length = optarg;
	    		TRACE_DBG("LENGTH: %s\n", pu8length);
	    	break;
	    	case 'a':
	    		TRACE_DBG("ADD COMMAND\n");
	    		if ( (pu8length != NULL) && (pu8type != NULL) && (pu8data != NULL) )
	    		{
	    			u32_err = interchange_protocol_encode (ENCODE_ADD_CMD, atoi(pu8type), hextobin(au8_Buffer, IPED_LENGTH_BUFFER, pu8data, atoi(pu8length)), atoi(pu8length), au8_encoded_data, &u16_length);
	    		}
	    	break;
	    	case 's':
	    		TRACE_DBG("ADD SYNC\n");
	    		u32_err = interchange_protocol_encode (ENCODE_ADD_SYNC, TYPE_DEFAULT, NULL, 0x00, au8_encoded_data, &u16_length);
	    	break;
	    	case 'c':
	    		TRACE_DBG("ADD CONTAINER\n");
	    		u32_err = interchange_protocol_encode (ENCODE_ADD_CONTAINER, atoi(pu8type), NULL, 0x00, au8_encoded_data, &u16_length);
	    	break;
	    	case 'x':
	    		// Send data - Serial Port
				fd = serial_open (au8_default_com, O_WRONLY);
				if (fd != -1)
				{
					serial_write (fd, au8_encoded_data, u16_length);
					serial_close (fd);
				}
	    	break;
	    	case 'R':
	    		memset (au8_Buffer, 0xFF, IPED_LENGTH_BUFFER);
	    		// Send data 0xFF - Serial Port
				fd = serial_open (au8_default_com, O_WRONLY);
				if (fd != -1)
				{
					for (u32x = 0; u32x < 3; u32x++)
					{
						serial_write (fd, au8_Buffer, (IPED_LENGTH_BUFFER / 2));
					}
					serial_close (fd);
				}
	    	break;
	    	case 'r':
	    		TRACE_DBG("RECEIVE DATA\n");
	    		i32receivedata = 1;
	    	break;
	    	case 'e':
	    		i32example = 1;
	    	break;
            case 'i':
            	i32device = 1;
            break;
	    	case 'h':
	    	case '?':
	    		printf ("\n");
	    		printf ("%s [-d data] [-p COM] [-t type] [-l length] [-f filename.hex] [-M x] [-a] [-s] [-c] [-i] [-r] [-x] [-h] [-?]\n", argv[0]);
	    		printf ("\n");
	    		printf ("option\tDescription\n");
	    		printf ("------\t-----------\n");
	    		printf ("-h\thelp\n");
	    		printf ("-?\thelp\n");
	    		printf ("-d\tdata\n");
	    		printf ("-t\ttype\n");
	    		printf ("-l\tlength\n");
	    		printf ("-a\tadd command\n");
	    		printf ("-s\tadd sync\n");
	    		printf ("-c\tadd container\n");
	    		printf ("-x\tsend all encoded data\n");
	    		printf ("-r\trecevie encoded data, decode it and write it to stdout\n");
	    		printf ("-p\tset default com port\n");
	    		printf ("-e\trun encode and decode examples (if debug enabled)\n");
	    		printf ("-f\tfilename.hex\n");
	    		printf ("-H\tPrint buffer, hex coded\n");
	    		printf ("-R\tsend 0xFF 1.5x buffer size, resync and flush buffer\n");
	    		printf ("-i\tData in/out with pipes\n");
				printf ("-M\t1 = nRF Station, 2 = nRF Smartlock, 3 = Smartlock\n");
	    		printf ("\n");
	    		printf ("1. Example CMD\n");
	    		printf ("-----------\n");
	    		printf ("Send: TYPE_VEHICLE_TYPE_1 with 4 Bytes data\n");
	    		printf ("interchange_protocol_encode_decode -p /dev/ttyS2 -t 8 -l 4 -d 0x00000001 -a -s -x\n");
	    		printf ("\n");
	    		printf ("2. Example Container\n");
	    		printf ("-------------------\n");
	    		printf ("Send: TYPE_SCHLOSS_BATTERY_1 with 2 Bytes data, inside a container\n");
	    		printf ("interchange_protocol_encode_decode -p /dev/ttyS2 -t 5 -l 2 -d 0x0009 -a -t 2 -c -s -x\n");
	    		printf ("\n");
	    		printf ("3. Example Container\n");
	    		printf ("-------------------\n");
	    		printf ("Send: TYPE_VEHICLE_TYPE_1 with 4 Bytes data, TYPE_SCHLOSS_BATTERY_1 with 2 Bytes data, inside a container\n");
	    		printf ("interchange_protocol_encode_decode -p /dev/ttyS2 -t 8 -l 4 -d 0x00000001 -a -t 5 -l 2 -d 0x0009 -a -t 2 -c -s -x\n");
	    		printf ("\n");
	    		printf ("4. Example Container\n");
	    		printf ("-------------------\n");
	    		printf ("Send: FW update\n");
	    		printf (" * TYPE_FW_UPDATE_PREPARE_LOCK_1 (not shown)\n");
	    		printf (" * TYPE_FW_UPDATE_IMAGE_LOCK_1 (from file, look example)\n");
	    		printf (" * TYPE_FW_UPDATE_CRC_LOCK_1 (not shown)\n");
	    		printf (" * TYPE_FW_UPDATE_EXECUTE_LOCK_1 (not shown)\n");
	    		printf ("interchange_protocol_encode_decode -p /dev/aot_iped_COM0 -M 3 -f FirmwareLock.ihex\n");
	    		printf ("\n");
	    		printf ("5. Example Receive\n");
	    		printf ("-------------------\n");
	    		printf ("Receive:\n");
	    		printf ("interchange_protocol_encode_decode -p /dev/ttyS2 -r\n");
	    		printf ("\n");
	    		break;
		}
	}

	/*-------------------------------------------------------------------------
	| run decode
	-------------------------------------------------------------------------*/
	if (i32receivedata == 1)
	{
		main_receive_data (au8_default_com);
	}

    /*-------------------------------------------------------------------------
    | run pipes
    -------------------------------------------------------------------------*/
    if (i32device == 1)
    {
        main_drive_receive_data (au8_default_drive);
    }

    /*-------------------------------------------------------------------------
    | run pipes
    -------------------------------------------------------------------------*/
    if ( (i32fileread == 1) && (pu8Mikrokontroller != NULL) )
    {
    	main_file_read_data (pu8filename, pu8Mikrokontroller[0], au8_default_com);
    }


	/*-------------------------------------------------------------------------
	| run example
	-------------------------------------------------------------------------*/
	if (i32example == 1)
	{
		TRACE_DBG("RUN EXAMPLE\n");
		interchange_protocol_test();
	}

	/*-------------------------------------------------------------------------
	| Program exit
	-------------------------------------------------------------------------*/
	exit (EXIT_SUCCESS);
}

/* eof */
