/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Fahrradausleihstation
 *
 *     Module: MAIN_UTILITIES_H_
 *      State: Not formally tested
 * Originator: Schwab
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/Publibike/Fahrradausleihstation/Cmd_Line_Tool/trunk/main_receive_data.h $
 *  $Revision: 20259 $
 *      $Date: 2017-07-14 17:55:55 +0200 (Fr, 14 Jul 2017) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 *************************************************************************** */

#ifndef MAIN_RECEIVE_DATA_H_
#define MAIN_RECEIVE_DATA_H_

#include "interchange_protocol_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------+
|  global types                                                             |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  global constants                                                         |
+--------------------------------------------------------------------------*/
#define TIMEOUT		10		// Timeout in seconds

/*--------------------------------------------------------------------------+
|  function prototypes for C                                                |
+--------------------------------------------------------------------------*/
void main_receive_data (uint8_t *au8_default_com);


#ifdef __cplusplus
}
#endif

#endif /* MAIN_RECEIVE_DATA_H_ */
/* eof */
