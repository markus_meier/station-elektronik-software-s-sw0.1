/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Publibike
 *
 *     Module: main_read_file
 *      State:
 * Originator: Schwab
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/Publibike/Fahrradausleihstation/Cmd_Line_Tool/trunk/main_read_file.c $
 *  $Revision: 20259 $
 *      $Date: 2017-07-14 17:55:55 +0200 (Fr, 14 Jul 2017) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************

/* pragma's and makefile-generated #define's ------------------------------*/

/* headers for this module ------------------------------------------------*/

/* standard-headers (ANSI) ------------------------------------------------*/

#include "main_read_file.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <time.h>

#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */

/* headers from other modules ---------------------------------------------*/
#include "interchange_protocol_encode_decode.h"
#include "interchange_protocol_type.h"
#include "interchange_protocol_error.h"
#include "interchange_protocol_cmd_function.h"
#include "interchange_protocol_debug.h"
#include "main_utilities.h"


/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  module-global variables                                                  |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  declaration of local functions (helper functions)                        |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/**
 * open drive
 */
int file_open (uint8_t *pu8device, uint32_t u32flag)
{
    int fd; /* File descriptor for the port */

    fd = open(pu8device, u32flag);
    if (fd == -1)
    {
        perror("open_file: Unable to open");
    }

    return (fd);
}

/*-------------------------------------------------------------------------*/
/**
 * read drive
 */
int file_read (int fd, uint8_t *pu8_data, uint32_t u32length)
{
    return (read(fd, pu8_data, u32length));
}

/*-------------------------------------------------------------------------*/
/**
 * write drive
 */
int file_write (int fd, uint8_t *pu8_data, uint32_t u32length)
{
    return (write(fd, pu8_data, u32length));
}

/*-------------------------------------------------------------------------*/
/**
 * close file
 */
int file_close (int fd)
{
    return (close(fd));
}

/*-------------------------------------------------------------------------*/
/**
 * search start
 */
int file_search_start (int u32_z, uint8_t *au8_Filebuffer)
{
	uint32_t u32_x;

	// Start suchen
	for (u32_x = 0; u32_x < (IPED_LENGTH_BUFFER * IPED_LENGTH_BUFFER); u32_x++)
	{
		if (au8_Filebuffer[u32_x] == ':')
			{
				return (u32_x);
			}
	}

	return ((IPED_LENGTH_BUFFER * IPED_LENGTH_BUFFER) + 100);
}


/*-------------------------------------------------------------------------*/
/**
 * search start
 */
int file_search_stop (int u32_z, uint8_t *au8_Filebuffer)
{
	uint32_t u32_x;

	// Start suchen
	for (u32_x = 0; u32_x < (IPED_LENGTH_BUFFER * IPED_LENGTH_BUFFER); u32_x++)
	{
		if ( (au8_Filebuffer[u32_x] == '\n') || (au8_Filebuffer[u32_x] == '\r') )
			{
				return (u32_x);
			}
	}

	return ((IPED_LENGTH_BUFFER * IPED_LENGTH_BUFFER) + 100);
}




/*-------------------------------------------------------------------------*/
/**
 * main_file_read_data
 */
void main_file_read_data (uint8_t *pu8_filename, uint8_t u8_Nummer, uint8_t *pu8_device)
{
	uint8_t   au8_Filebuffer[IPED_LENGTH_BUFFER * IPED_LENGTH_BUFFER];
	int32_t   fd_read;
	int32_t   fd_write;
    uint8_t   pu8_data[IPED_LENGTH_BUFFER];
    uint8_t   au8_buffer[IPED_LENGTH_BUFFER];
    int32_t   i32_length;
    int32_t   i32_count;
    int32_t   i32_start = 0;
    int32_t   i32_stop = 0;

    uint64_t  u64_time;
    int32_t   i32_input_position = 0;
    uint8_t   u8_data;
    uint32_t  u32_cc;
    uint8_t   u8_mikrokontroller = 0xFF;

    uint8_t   au8_encode_data[IPED_LENGTH_BUFFER];
    uint8_t   au8_Buffer[IPED_LENGTH_BUFFER];
    uint8_t   au8_tmp[IPED_LENGTH_BUFFER];

    if (pu8_device == NULL)
    {
    	return ;
    }

    if (u8_Nummer == '1')
    {
    	u8_mikrokontroller = TYPE_FW_UPDATE_IMAGE_NRFSTATION_1;
    }
    else if (u8_Nummer == '2')
    {
    	u8_mikrokontroller = TYPE_FW_UPDATE_IMAGE_NRFLOCK_1;
    }
    else if (u8_Nummer == '3')
    {
    	u8_mikrokontroller = TYPE_FW_UPDATE_IMAGE_LOCK_1;
    }
    else
    {
    	return ;
    }

    memset (au8_Filebuffer, 0x00, (IPED_LENGTH_BUFFER * IPED_LENGTH_BUFFER));

    // File laden
    fd_read = file_open (pu8_filename, O_RDONLY);
    if (fd_read != -1)
    {
        // Load File
    	i32_length = 0;
    	do
    	{
    		i32_count = file_read (fd_read, &au8_Filebuffer[i32_length], 15);
    		if ( (i32_count > 0) && (i32_length < IPED_LENGTH_BUFFER * IPED_LENGTH_BUFFER) )
    		{
    			i32_length = i32_length + i32_count;
    			//printf ("%d\n", i32_length);
    		}
    	}
    	while (i32_count > 0);
		file_close(fd_read);

		// File senden
		do
		{
			i32_start = file_search_start (0, au8_Filebuffer);
			if (i32_start > 0x00)
			{
				memset (au8_Filebuffer, ' ', i32_start);
			}
			i32_stop = file_search_stop (0, au8_Filebuffer);

			// Received
			u32_cc = 0;
			memset (au8_tmp, 0x00, IPED_LENGTH_BUFFER);

			for (i32_count = i32_start; i32_count < i32_stop; i32_count++)
			{
				if (au8_Filebuffer[i32_count] != ':')
				{
//	    			printf ("%c", au8_Filebuffer[i32_count]);
					au8_tmp[u32_cc] = au8_Filebuffer[i32_count];
					u32_cc++;
				}
			}
			au8_tmp[i32_count] = 0x00;
			u32_cc = u32_cc / 2;
//			printf ("\n");

			// Send Image
			sprintf (au8_Buffer, "echo \"-t %d -l %d -d 0x%s -a -s -x\" > %s", u8_mikrokontroller, u32_cc, au8_tmp, pu8_device);
			//printf ("%s", au8_Buffer);
			system (au8_Buffer);

/*			// Diese Variante ist zwar schneller, funktioniert aber nicht.
			fd_write = file_open (pu8_device, O_WRONLY);
			if (fd_write != -1)
			{
				// write to device
				file_write (fd_write, au8_Buffer, strlen(au8_Buffer));
				printf ("WRITE: %d : %s\n", (int) strlen(au8_Buffer), au8_Buffer);
				file_close(fd_write);
			}
*/
			fd_read = file_open (pu8_device, O_RDONLY);
			if (fd_read != -1)
			{
				// Auf Antwort warten
				printf ("READ: ");
				do
				{
					//memset (au8_buffer, 0x00, IPED_LENGTH_BUFFER);
					i32_count = file_read (fd_read, au8_buffer, 1);
					printf ("%c", au8_buffer[0]);
				}
				while ( (au8_buffer[0] != '\n') && (au8_buffer[0] != '\r') && (i32_count > 0));
				printf ("READ END\n");
				file_close(fd_read);
			}

			memset (au8_Filebuffer, ' ', i32_stop);
		}
		while ( i32_length > file_search_start (0, au8_Filebuffer));
    }
}

/* eof */
