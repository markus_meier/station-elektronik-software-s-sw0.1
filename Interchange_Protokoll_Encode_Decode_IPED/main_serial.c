/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Publibike
 *
 *     Module: main_serial
 *      State:
 * Originator: Schwab
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/Publibike/Fahrradausleihstation/Cmd_Line_Tool/trunk/main_serial.c $
 *  $Revision: 20259 $
 *      $Date: 2017-07-14 17:55:55 +0200 (Fr, 14 Jul 2017) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************

/* pragma's and makefile-generated #define's ------------------------------*/

/* headers for this module ------------------------------------------------*/

/* standard-headers (ANSI) ------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */

/* headers from other modules ---------------------------------------------*/
#include "interchange_protocol_encode_decode.h"
#include "interchange_protocol_type.h"
#include "interchange_protocol_error.h"
#include "interchange_protocol_cmd_function.h"
#include "interchange_protocol_debug.h"
#include "main_serial.h"


/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  module-global variables                                                  |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  declaration of local functions (helper functions)                        |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/**
 * set baudrate tty
 */
void serial_baudrate (int fd)
{
	struct termios options;

	/*
	 * Get the current options for the port...
	 */
	tcgetattr(fd, &options);

	/*
	 * Set the baud rates to 115200
	 */

	cfsetispeed(&options, B115200);
	cfsetospeed(&options, B115200);

    /*
     * Enable the receiver and set local mode...
     */
    options.c_cflag |= (CLOCAL | CREAD);

    /*
     * shut off xon/xoff ctrl...
     */
	options.c_iflag &= ~(IXON | IXOFF | IXANY);

    /*
     * shut off
     */
	options.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG);

    /*
     * shut off
     */
	options.c_iflag &= ~(INLCR | ICRNL);

    /*
     * shut off
     */
	options.c_iflag&=~(IGNCR | IUTF8);
	options.c_oflag&=~(ONLCR | OCRNL);

	/*
     * disable break processing
     */
	options.c_iflag &= ~IGNBRK;

    /*
     * no signaling chars, no echo,
     */
	options.c_lflag = 0;

    /*
     * 8N1
     */
    options.c_cflag &= ~PARENB;
    options.c_cflag &= ~CSTOPB;
    options.c_cflag &= ~CSIZE;
    options.c_cflag |= CS8;

    /*
     * Set the new options for the port...
     */
    tcsetattr(fd, TCSANOW, &options);
}


/*-------------------------------------------------------------------------*/
/**
 * open tty
 */
int serial_open (uint8_t *pu8device, uint32_t u32flag)
{
	int fd; /* File descriptor for the port */


	fd = open(pu8device, O_NOCTTY | O_NDELAY | u32flag);
	if (fd == -1)
	{
		perror("open_port: Unable to open");
	}
	else
	{
		fcntl(fd, F_SETFL, 0);			// Read blocking
		//fcntl(fd, F_SETFL, FNDELAY);	// Read non Blocking
		serial_baudrate (fd);
	}

	return (fd);
}


/*-------------------------------------------------------------------------*/
/**
 * write tty
 */
int serial_write (int fd, uint8_t *pu8_data, uint32_t u32length)
{
	uint32_t	u32x;

#if (1)

	uint32_t	u32_out_pos = 0;
	uint32_t	u32_out_length;
	uint32_t	u32_count = IPED_LENGTH_BUFFER;

	// Don't write more than 60 bytes. Then make a short break.
	do
	{

		if (u32length > 60)
		{
			u32_out_length = 60;
		}
		else
		{
			u32_out_length = u32length;
		}

		TRACE_DBG("WRITE DATA: length %u - wait 100ms\n", (unsigned int)u32_out_length);
		usleep (100000);	// 100ms sleep
		u32x = write(fd, (pu8_data + u32_out_pos), u32_out_length);

		if (u32x > 0)
		{
			u32_out_pos = u32_out_pos + u32x;
			u32length = u32length - u32_out_length;
		}

		u32_count--;	// Count down
	}
	while ( (u32length > 0) && (u32x > 0) && (u32_count > 0) );

#else

	u32x = write(fd, pu8_data, u32length);

#endif

	return (u32x);
}


/*-------------------------------------------------------------------------*/
/**
 * read tty
 */
int serial_read (int fd, uint8_t *pu8_data, uint32_t u32length)
{
	return (read(fd, pu8_data, u32length));
}


/*-------------------------------------------------------------------------*/
/**
 * close tty
 */
int serial_close (int fd)
{
	return (close(fd));
}


/* eof */






