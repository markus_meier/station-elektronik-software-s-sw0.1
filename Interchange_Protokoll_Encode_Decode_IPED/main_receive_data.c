/****************************************************************************
 *
 *   Customer: Publibike
 *   Project#: 17-03
 *       Name: Publibike
 *
 *     Module: main_receive_data
 *      State:
 * Originator: Schwab
 *
 *   $HeadURL: svn://192.168.0.100/aot/Software/Publibike/Fahrradausleihstation/Cmd_Line_Tool/trunk/main_receive_data.c $
 *  $Revision: 20284 $
 *      $Date: 2017-07-19 18:50:32 +0200 (Mi, 19 Jul 2017) $
 *    $Author: Schwab $
 *
 *  Developed by Art of Technology AG, 2017
 *
 ****************************************************************************

/* pragma's and makefile-generated #define's ------------------------------*/

/* headers for this module ------------------------------------------------*/

/* standard-headers (ANSI) ------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>

/* headers from other modules ---------------------------------------------*/
#include "interchange_protocol_encode_decode.h"
#include "interchange_protocol_type.h"
#include "interchange_protocol_error.h"
#include "interchange_protocol_cmd_function.h"
#include "interchange_protocol_debug.h"
#include "interchange_protocol_test.h"
#include "main_serial.h"
#include "main_utilities.h"
#include "main_receive_data.h"


/*--------------------------------------------------------------------------+
|  local constants and macros                                               |
+--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------+
|  module-global types                                                      |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  module-global variables                                                  |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  declaration of local functions (helper functions)                        |
+--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------+
|  functions                                                                |
+--------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------*/
/**
 * Callback: TYPE_CONTAINER_GENERIC_1
 */
uint32_t main_callback_TYPE_DEFAULT (Interchange_Protocol_Header *iph)
{
	uint32_t u32err = ERR_OK;
	uint32_t u32x = 0;
	uint32_t u32offset = 0;
	uint16_t u16Type = HUMANREADABLE_16 (iph->u16Type);
	uint16_t u16length = HUMANREADABLE_16 (iph->u16Length);

	TRACE_DBG ("CALL TYPE_DEFAULT\n");

	if (ERR_OK == interchange_protocol_check_container (iph))
	{
		printf ("CONTAINER ");
		u32offset = IPH_CRC_SIZE;

		// Just an example
		/*
		if (u8flag == 0x00)
		{
			u32err = ERR_CONTAINER_DECODE_STOP;
		}
		else
		{
			u32err = ERR_OK;
		}
		*/
	}
	else
	{
		printf ("COMMAND ");
		u32offset = 0;
	}

	printf ("TYPE=%u LENGTH=%u DATA=0x", (unsigned int)u16Type, (unsigned int)u16length);

	for (u32x = 0; u32x < u16length; u32x++)
	{
		printf ("%02x", (unsigned int)iph->au8Data[u32x + u32offset]);
	}

	printf (";");

	return u32err;
}


/*-------------------------------------------------------------------------*/
/**
 * receive data
 */
void main_receive_data (uint8_t *au8_default_com)
{
	uint32_t	u32_err = ERR_ERROR;
	int32_t		i32_length = 0;

	int32_t		fd;
	int32_t		i32_input_position = 0;
	uint32_t 	u32_position = 0;
	uint64_t	u64_time = 0;
	uint32_t	u32x = 0;

	uint8_t		au8_Buffer[IPED_LENGTH_BUFFER];
	uint8_t		au8_encoded_data[IPED_LENGTH_BUFFER];
	uint8_t		au8_internal_Buffer[IPED_LENGTH_BUFFER];

	// Reset functions
	interchange_cmd_function_init();

	// Set default function
	interchange_set_function (main_callback_TYPE_DEFAULT, TYPE_DEFAULT);

	// Get data - Serial Port
	fd = serial_open (au8_default_com, O_RDONLY);
	if (fd != -1)
	{
		i32_input_position = 0;
		do
		{
			u64_time = time(NULL) + TIMEOUT;
			i32_length = serial_read (fd, (uint8_t *)(au8_Buffer), 1); // IPED_LENGTH_BUFFER / 3

			if (i32_length > 0)
			{
				// Timeout
				if (u64_time < time(NULL))
				{
					TRACE_DBG ("timeout\n");
					memset (au8_encoded_data, 0xFF, IPED_LENGTH_BUFFER);
					i32_input_position = i32_length;
				}

				// Add Data
				memcpy (au8_encoded_data + i32_input_position, au8_Buffer, i32_length);

				// Calculate input position
				i32_input_position = i32_input_position + i32_length;

				// Bug u32_position in response value, do not use.
				// Decode Data - The function calculates always with a length of: IPED_LENGTH_BUFFER
				// Don't use messages larger than 1024 Bytes!!!
				u32_err = interchange_protocol_decode (au8_encoded_data, i32_input_position, &u32_position, au8_internal_Buffer);
				switch (u32_err)
				{
				case ERR_DATA_RECEIVED_CORRECT:
					// move buffer
					TRACE_DBG ("ERR_DATA_RECEIVED_CORRECT: %d : %u\n", (int)i32_input_position, (unsigned int)u32_position);
					printf ("\r\n");
					memset (au8_encoded_data, 0x00, IPED_LENGTH_BUFFER);
					i32_input_position = 0x00;
				break;
				case ERR_NO_SYNC_FOUND:
					TRACE_DBG ("ERR_NO_SYNC_FOUND: %d : %u\n", (int)i32_input_position, (unsigned int)u32_position);
					memset (au8_encoded_data, 0x00, IPED_LENGTH_BUFFER);
					i32_input_position = 0x00;
				break;
				case ERR_NOMEM:
					TRACE_DBG ("ERR_NOMEM: %d : %u\n", (int)i32_input_position, (unsigned int)u32_position);
					memset (au8_encoded_data, 0x00, IPED_LENGTH_BUFFER);
					i32_input_position = 0x00;
				break;
				}

				TRACE_DBG ("*************************************\n");
				// print ERR Code
				TRACE_DBG ("ERR: %u : %d : %u\n", (unsigned int)u32_err, (int)i32_input_position, (unsigned int)u32_position);
				TRACE_DBG ("*************************************\n");
                fflush(stdout);
			}
		}
		while (i32_length > 0);
		serial_close (fd);
	}
}

/* eof */
