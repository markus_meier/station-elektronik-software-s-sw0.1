import time
import unittest
from datetime import datetime
from distutils.version import LooseVersion

import urllib3

from generated.swagger_client import Rental
from generated.swagger_client.rest import ApiException
from station import StationConfiguration
from station.backend.rentals_api_helper import RentalsApiHelper


class TestRentalsApiHelper(unittest.TestCase):
    STATION_ID = 1
    VEHICLE_ID = 2
    ACCESS_MEDIA_ID = 2

    def setUp(self):
        self.config = StationConfiguration("./test/station/station_configuration.cfg")
        self.api = RentalsApiHelper(self.config)

    @unittest.skipIf(LooseVersion(urllib3.__version__) < LooseVersion("1.21.1"), "urllib3 version too old")
    def test_shouldAddRentals(self):
        start_rental = Rental(self.VEHICLE_ID, self.ACCESS_MEDIA_ID, datetime.now().isoformat())
        time.sleep(1)
        stop_rental = Rental(self.VEHICLE_ID, self.ACCESS_MEDIA_ID, None, datetime.now().isoformat(), 42, 1, 0, False)
        rentals = [start_rental, stop_rental]
        self.api.put(self.STATION_ID, rentals)

    @unittest.skipIf(LooseVersion(urllib3.__version__) < LooseVersion("1.21.1"), "urllib3 version too old")
    def test_shouldNotAddRentalWithUnknownStation(self):
        with self.assertRaises(ApiException):
            self.api.put(self.STATION_ID, [])

    @unittest.skipIf(LooseVersion(urllib3.__version__) < LooseVersion("1.21.1"), "urllib3 version too old")
    def test_shouldAddStartAndStopRentals(self):
        self.api.put_start_rental(self.STATION_ID, self.VEHICLE_ID, self.ACCESS_MEDIA_ID, datetime.now())
        time.sleep(1)
        self.api.put_stop_rental(self.STATION_ID, self.VEHICLE_ID, 42, datetime.now(), 1, 2, False,
                                 self.ACCESS_MEDIA_ID)
