import unittest

from generated.swagger_client.models.system_configuration import SystemConfiguration
from station import StationConfiguration
from station.backend.configuration_api_helper import ConfigurationApiHelper


class TestConfigurationApiHelper(unittest.TestCase):
    STATION_ID = 2

    def setUp(self):
        self.config = StationConfiguration("./test/station/station_configuration.cfg")
        self.api = ConfigurationApiHelper(self.config)

    def test_shouldGetConfiguration(self):
        data = self.api.fetch(self.STATION_ID)
        self.assertIsNotNone(data)
        self.assertIsInstance(data, SystemConfiguration)
