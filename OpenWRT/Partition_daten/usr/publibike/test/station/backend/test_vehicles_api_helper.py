import unittest

from generated.swagger_client.models.vehicle_container import VehicleContainer
from station import StationConfiguration
from station.backend.vehicles_api_helper import VehiclesApiHelper


class TestVehiclesApiHelper(unittest.TestCase):
    STATION_ID = 2
    UPDATE_TOKEN = ""

    def setUp(self):
        self.config = StationConfiguration("./test/station/station_configuration.cfg")
        self.api = VehiclesApiHelper(self.config)

    def test_shouldGetAllVehicles(self):
        data = self.api.fetch(self.STATION_ID, self.UPDATE_TOKEN)
        self.assertIsNotNone(data)
        self.assertIsInstance(data, VehicleContainer)
        self.assertIsNotNone(data.update_token)
        self.assertGreater(len(data.vehicles), 0)

    def test_shouldGetNoNewVehiclesWithCurrentUpdateToken(self):
        # Fetch all vehicles
        data = self.api.fetch(self.STATION_ID, self.UPDATE_TOKEN)
        self.assertIsNotNone(data)
        self.assertIsNotNone(data.update_token)

        # Fetch again with update_token we just got
        data2 = self.api.fetch(self.STATION_ID, data.update_token)
        self.assertIsNotNone(data2)
        self.assertIsInstance(data2, VehicleContainer)
        self.assertEqual(len(data2.vehicles), 0)
