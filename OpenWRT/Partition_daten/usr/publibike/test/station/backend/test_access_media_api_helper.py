import unittest

from generated.swagger_client.models.access_media_container import AccessMediaContainer
from station import StationConfiguration
from station.backend.access_media_api_helper import AccessMediaApiHelper


class TestAccessMediaApi(unittest.TestCase):
    def setUp(self):
        self.config = StationConfiguration("./test/station/station_configuration.cfg")
        self.api = AccessMediaApiHelper(self.config)

    def test_shouldFetchAccessMedias(self):
        data = self.api.fetch("")
        self.assertIsNotNone(data)
        self.assertIsInstance(data, AccessMediaContainer)
        self.assertTrue(len(data.update_token) > 0)
        self.assertIsNotNone(data.data)
