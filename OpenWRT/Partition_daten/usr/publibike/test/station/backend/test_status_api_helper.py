import unittest
from distutils.version import LooseVersion

import urllib3

from generated.swagger_client.models.station_status import StationStatus
from generated.swagger_client.models.station_status_response import StationStatusResponse
from generated.swagger_client.rest import ApiException
from station import StationConfiguration
from station.backend.status_api_helper import StatusApiHelper


class TestStatusApiHelper(unittest.TestCase):
    STATION_ID = 2

    def setUp(self):
        self.config = StationConfiguration("./test/station/station_configuration.cfg")
        self.api = StatusApiHelper(self.config)
        self.status = StationStatus(42, [])

    @unittest.skipIf(LooseVersion(urllib3.__version__) < LooseVersion("1.21.1"), "urllib3 version too old")
    def test_shouldUpdateStation(self):
        data = self.api.put(self.STATION_ID, self.status)
        self.assertIsNotNone(data)
        self.assertIsInstance(data, StationStatusResponse)
        self.assertIsNotNone(data.station_status)
        self.assertIsNotNone(data.blocked_vehicles)

    @unittest.skipIf(LooseVersion(urllib3.__version__) < LooseVersion("1.21.1"), "urllib3 version too old")
    def test_shouldNotUpdateWithUnknownStation(self):
        with self.assertRaises(ApiException):
            self.api.put(0, self.status)
