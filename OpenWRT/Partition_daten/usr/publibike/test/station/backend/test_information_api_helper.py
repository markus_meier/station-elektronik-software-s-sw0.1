import unittest

from generated.swagger_client.models.internal_information import InternalInformation
from station import StationConfiguration
from station.backend.information_api_helper import InformationApiHelper


class TestInformationApiHelper(unittest.TestCase):
    def setUp(self):
        self.config = StationConfiguration("./test/station/station_configuration.cfg")
        self.api = InformationApiHelper(self.config)

    def test_shouldGetInformation(self):
        data = self.api.fetch()
        self.assertIsNotNone(data)
        self.assertIsInstance(data, InternalInformation)
        self.assertTrue(len(data.version) > 0)
        self.assertTrue(len(data.start_time) > 0)
