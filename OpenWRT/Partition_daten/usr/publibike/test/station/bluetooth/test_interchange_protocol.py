import string
import unittest

from station.bluetooth.interchange_protocol import InterchangeProtocol
from station.bluetooth.interchange_protocol_error import InterchangeProtocolError
from station.bluetooth.interchange_protocol_type import InterchangeProtocolType
from station.bluetooth.parse_exception import ParseException


class TestInterchangeProtocol(unittest.TestCase):
    def setUp(self):
        self.interchange_protocol = InterchangeProtocol()

    def tearDown(self):
        pass

    def test_shouldParseCommand(self):
        cmd = "{6} {0}{1} {2}{3} {4}{5}".format(InterchangeProtocol.CMD_OUT_PART_TYPE,
                                                InterchangeProtocolType.TYPE_DEFAULT,
                                                InterchangeProtocol.CMD_OUT_PART_LENGTH, 1,
                                                InterchangeProtocol.CMD_OUT_PART_DATA, '42',
                                                InterchangeProtocol.CMD_OUT_PART_COMMAND)
        res = self.interchange_protocol.parse_cli_output(cmd)
        self.assertEqual(len(res), 1)
        cmd_type, length, data = res[0]
        self.assertEqual(cmd_type, InterchangeProtocolType.TYPE_DEFAULT)
        self.assertEqual(length, 1)
        self.assertEqual(data, '42')

    def test_shouldParseCommandWithHexGreater10(self):
        cmd = "{6} {0}{1} {2}{3} {4}{5}".format(InterchangeProtocol.CMD_OUT_PART_TYPE,
                                                InterchangeProtocolType.TYPE_APP_1,
                                                InterchangeProtocol.CMD_OUT_PART_LENGTH, 1,
                                                InterchangeProtocol.CMD_OUT_PART_DATA, '42',
                                                InterchangeProtocol.CMD_OUT_PART_COMMAND)
        res = self.interchange_protocol.parse_cli_output(cmd)
        self.assertEqual(len(res), 1)
        cmd_type, length, data = res[0]
        self.assertEqual(cmd_type, InterchangeProtocolType.TYPE_APP_1)
        self.assertEqual(length, 1)
        self.assertEqual(data, '42')

    def test_shouldParseVehicleType(self):
        cmd = "COMMAND TYPE=8 LENGTH=4 DATA=0x00000001"
        res = self.interchange_protocol.parse_cli_output(cmd)
        self.assertEqual(len(res), 1)
        cmd_type, length, data = res[0]
        self.assertEqual(cmd_type, InterchangeProtocolType.TYPE_VEHICLE_TYPE_1)
        self.assertEqual(length, 4)
        self.assertEqual(data, '00000001')

    def test_shouldParseSchlossBattery(self):
        cmd = "COMMAND TYPE=5 LENGTH=2 DATA=0x0009"
        res = self.interchange_protocol.parse_cli_output(cmd)
        self.assertEqual(len(res), 1)
        cmd_type, length, data = res[0]
        self.assertEqual(cmd_type, InterchangeProtocolType.TYPE_SCHLOSS_BATTERY_1)
        self.assertEqual(length, 2)
        self.assertEqual(data, '0009')

    def test_shouldParseContainer(self):
        # Containers are separated by ; and on one line (CONTAINER data ; COMMAND 1 ; COMMAND 2 ...)
        container = ("CONTAINER TYPE=2 LENGTH=18 DATA=0xd8d300080004000000013bb4000500020009 ; " +
                     "COMMAND TYPE=8 LENGTH=4 DATA=0x00000001 ; COMMAND TYPE=5 LENGTH=2 DATA=0x0009")
        res = self.interchange_protocol.parse_cli_output(container)
        self.assertEqual(len(res), 3)
        cmd_type, length, data = res[0]
        self.assertEqual(cmd_type, InterchangeProtocolType.TYPE_CONTAINER_GENERIC_1)
        # we don't care about the data in the container
        cmd_type, length, data = res[1]
        self.assertEqual(cmd_type, InterchangeProtocolType.TYPE_VEHICLE_TYPE_1)
        self.assertEqual(length, 4)
        self.assertEqual(data, '00000001')
        cmd_type, length, data = res[2]
        self.assertEqual(cmd_type, InterchangeProtocolType.TYPE_SCHLOSS_BATTERY_1)
        self.assertEqual(length, 2)
        self.assertEqual(data, '0009')

    def test_shouldThrowOnIncompleteCommand(self):
        cmd = "COMMAND"
        with self.assertRaises(ParseException):
            self.interchange_protocol.parse_cli_output(cmd)

    def test_shouldThrowOnMissingType(self):
        cmd = "COMMAND LENGTH=1 DATA=0x0009"
        with self.assertRaises(ParseException):
            self.interchange_protocol.parse_cli_output(cmd)

    def test_shouldThrowOnSizeMismatch(self):
        cmd = "COMMAND TYPE=5 LENGTH=1 DATA=0x0009"
        with self.assertRaises(ParseException):
            self.interchange_protocol.parse_cli_output(cmd)

    def test_shouldIgnoreEmptyLine(self):
        res = self.interchange_protocol.parse_cli_output("")
        self.assertEqual(len(res), 0)

    def test_shouldParseAppRentalData(self):
        # we don't really care about the container here (we trust the cli tool), thus the data is empty
        data = [(InterchangeProtocolType.TYPE_RENT_STATION_1, 0, ""),
                (InterchangeProtocolType.TYPE_BETRIEBSNUMMER_1, 6, "313233343536"),
                (InterchangeProtocolType.TYPE_PUBLIC_KEY_1, 33,
                 "303030303030303030303030303030303030303030303030303030303030303030")]
        out = self.interchange_protocol.parse_data(data)
        self.assertIsNotNone(out)
        self.assertEqual(out[InterchangeProtocol.DATA_TYPE], InterchangeProtocol.DATA_TYPE_START_RENTAL)
        self.assertEqual(out[InterchangeProtocol.DATA_BETRIEBSNUMMER], "123456")
        self.assertEqual(out[InterchangeProtocol.DATA_APP_PUBLIC_KEY], "000000000000000000000000000000000")

    def test_shouldParseRentalReturnData(self):
        # we don't really care about the container here (we trust the cli tool), thus the data is empty
        data = [(InterchangeProtocolType.TYPE_RENTAL_RETURN_1, 0, ""),
                (InterchangeProtocolType.TYPE_BETRIEBSNUMMER_1, 6, "313233343536"),
                (InterchangeProtocolType.TYPE_PUBLIC_KEY_1, 33,
                 "303030303030303030303030303030303030303030303030303030303030303030"),
                (InterchangeProtocolType.TYPE_RIDE_DURATION_1, 4, "00000042"),
                (InterchangeProtocolType.TYPE_NUMBER_OF_INTERMEDIATE_STOPS_1, 2, "0001"),
                (InterchangeProtocolType.TYPE_NUMBER_OF_EMERGENCY_STOPS_1, 2, "FFFF")]
        out = self.interchange_protocol.parse_data(data)
        self.assertIsNotNone(out)
        self.assertEqual(out[InterchangeProtocol.DATA_TYPE], InterchangeProtocol.DATA_TYPE_STOP_RENTAL)
        self.assertEqual(out[InterchangeProtocol.DATA_BETRIEBSNUMMER], "123456")
        self.assertEqual(out[InterchangeProtocol.DATA_APP_PUBLIC_KEY], "000000000000000000000000000000000")
        self.assertEqual(out[InterchangeProtocol.DATA_RIDE_DURATION], 66)
        self.assertEqual(out[InterchangeProtocol.DATA_INTERMEDIATE_STOPS], 1)
        self.assertEqual(out[InterchangeProtocol.DATA_EMERGENCY_STOPS], 0xFFFF)

    def test_shouldCreateCommand(self):
        res = self.interchange_protocol.create_command((InterchangeProtocolType.TYPE_RESPONSE_1, 4, '12345678'))
        self.assertEqual(string.strip(res), "-t 3 -l 4 -d 0x12345678")
        res = self.interchange_protocol.create_command((InterchangeProtocolType.TYPE_RESPONSE_1, 4, '12345678'),
                                                       append=True)
        self.assertEqual(string.strip(res), "-t 3 -l 4 -d 0x12345678 -a")

    def test_shouldCreateResponseCommand(self):
        res = self.interchange_protocol.create_response_command(InterchangeProtocolError.ERR_OK)
        self.assertEqual(string.strip(res), "-t 3 -l 4 -d 0x{0:08X} -s -x".format(InterchangeProtocolError.ERR_OK))

    def test_shouldCreateContainer(self):
        res = self.interchange_protocol.create_container(InterchangeProtocolType.TYPE_RENT_APP_1,
                                                         [(InterchangeProtocolType.TYPE_RESPONSE_1, 4, '12345678')])
        self.assertEqual(string.strip(res), "-t 3 -l 4 -d 0x12345678 -a -t 16 -c -s -x")

        res = self.interchange_protocol.create_container(InterchangeProtocolType.TYPE_RENT_APP_1,
                                                         [(InterchangeProtocolType.TYPE_RESPONSE_1, 4, '12345678'),
                                                          (InterchangeProtocolType.TYPE_RESPONSE_1, 4, '12121212')])
        self.assertEqual(string.strip(res), "-t 3 -l 4 -d 0x12345678 -a -t 3 -l 4 -d 0x12121212 -a -t 16 -c -s -x")

    def test_shouldHandleDisconnect(self):
        data = [(InterchangeProtocolType.TYPE_DISCONNECT_1, 0, "")]
        out = self.interchange_protocol.parse_data(data)
        self.assertIsNotNone(out)
        self.assertEqual(out[InterchangeProtocol.DATA_TYPE], InterchangeProtocol.DATA_TYPE_DISCONNECT)
