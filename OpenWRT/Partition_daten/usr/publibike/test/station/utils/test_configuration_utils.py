import unittest

from station import StationConfiguration
from station.backend.configuration_api_helper import ConfigurationApiHelper
from station.database.configuration_table import ConfigurationTable
from station.database.database import Database
from station.database.firmware_table import FirmwareTable
from station.utils.configuration_utils import ConfigurationUtils


class TestVehicleUtils(unittest.TestCase):
    def setUp(self):
        self.config = StationConfiguration("./test/station/station_configuration.cfg")
        self.api = ConfigurationApiHelper(self.config)
        self.database = Database(self.config)
        self.database.connect()
        self.configuration_table = ConfigurationTable(self.database)
        self.firmware_table = FirmwareTable(self.database)
        self.configuration_utils = ConfigurationUtils(self.api, self.configuration_table, self.firmware_table,
                                                      self.config)

        # Clean the DB for each test
        self.configuration_table.create_table(commit=True)
        self.firmware_table.create_table()

    def tearDown(self):
        self.configuration_table.drop_table(commit=True)
        self.database.connection().close()

    def test_shouldAddFirmwareFromBackend(self):
        self.configuration_utils.update_from_backend()
        self._verify_table_contains_entries(FirmwareTable.FIRMWARE_TABLE)

    def test_shouldAddStationConfigurationsFromBackend(self):
        self.configuration_utils.update_from_backend()
        self._verify_table_contains_entries(ConfigurationTable.STATION_CONFIGURATION_TABLE)

    def test_shouldAddSmartlockConfigurationsFromBackend(self):
        self.configuration_utils.update_from_backend()
        self._verify_table_contains_entries(ConfigurationTable.SMARTLOCK_CONFIGURATION_TABLE)

    def _verify_table_contains_entries(self, table=""):
        cursor = self.database.cursor()
        cursor.execute("SELECT COUNT(*) AS count FROM " + table + ";")
        self.assertGreater(cursor.fetchone()["count"], 0)
