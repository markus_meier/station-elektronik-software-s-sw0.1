import unittest
from distutils.version import LooseVersion

import urllib3

from station import StationConfiguration
from station.backend.status_api_helper import StatusApiHelper
from station.database.database import Database
from station.database.status_table import StatusTable
from station.utils.status_utils import StatusUtils


class TestStatusUtils(unittest.TestCase):
    def setUp(self):
        self.config = StationConfiguration("./test/station/station_configuration.cfg")
        self.api = StatusApiHelper(self.config)
        self.database = Database(self.config)
        self.database.connect()
        self.status_table = StatusTable(self.database)
        self.status_utils = StatusUtils(self.api, self.status_table, self.config)

        # Clean the DB for each test
        self.status_table.drop_table(commit=False)
        self.status_table.create_table(commit=True)

    def tearDown(self):
        self.status_table.drop_table(commit=True)
        self.database.connection().close()

    def test_shouldAddStatusEntry(self):
        self.assertEqual(len(self.status_table.get_status_updates()), 0)
        self.status_utils.add_status_entry(vehicle_id=1, smartlock_battery_level=2, ebike_battery_level=3)
        self.assertEqual(len(self.status_table.get_status_updates()), 1)

    @unittest.skipIf(LooseVersion(urllib3.__version__) < LooseVersion("1.21.1"), "urllib3 version too old")
    def test_shouldPushStatusEntryToBackend(self):
        # Add a status entry
        self.status_utils.add_status_entry(vehicle_id=1, smartlock_battery_level=2, ebike_battery_level=3)
        # Push it to the backend
        self.status_utils.push_to_backend(42)
        # Check that it's gone from the DB
        self.assertEqual(len(self.status_table.get_status_updates()), 0)
