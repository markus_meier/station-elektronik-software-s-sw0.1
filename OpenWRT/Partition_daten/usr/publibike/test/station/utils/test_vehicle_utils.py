import random
import unittest
import uuid

from generated.swagger_client import VehicleContainerVehicles
from station import StationConfiguration
from station.backend.vehicles_api_helper import VehiclesApiHelper
from station.backend_constants import BackendConstants
from station.database.database import Database
from station.database.updatetoken_table import UpdatetokenTable
from station.database.vehicle_table import VehicleTable
from station.utils.vehicle_utils import VehicleUtils


class TestVehicleUtils(unittest.TestCase):
    def setUp(self):
        self.config = StationConfiguration("./test/station/station_configuration.cfg")
        self.api = VehiclesApiHelper(self.config)
        self.database = Database(self.config)
        self.database.connect()
        self.vehicle_table = VehicleTable(self.database)
        self.updatetoken_table = UpdatetokenTable(self.database)
        self.vehicle_utils = VehicleUtils(self.api, self.vehicle_table, self.config, self.updatetoken_table)

        # Clean the DB for each test
        self.vehicle_table.create_table(commit=True)
        self.updatetoken_table.create_table()

    def tearDown(self):
        self.database.connection().rollback()
        self.database.connection().close()

    def test_shouldAddNewVehicle(self):
        vehicle_id = random.randint(100000, 900000)
        vehicle = VehicleContainerVehicles(vehicle_id, BackendConstants.VEHICLE_STATE_ACTIVE,
                                           BackendConstants.VEHICLE_TYPE_EBIKE, str(uuid.uuid4()))
        self.vehicle_utils._update_vehicles_in_db([vehicle])
        v = self.vehicle_table.get_vehicle(vehicle.id)
        self.assertIsNotNone(v)
        self.assertEqual(v[VehicleTable.VEHICLE_ID], vehicle.id)
        self.assertEqual(v[VehicleTable.VEHICLE_STATE_ID], vehicle.state_id)
        self.assertEqual(v[VehicleTable.VEHICLE_TYPE_ID], vehicle.type_id)
        self.assertEqual(v[VehicleTable.VEHICLE_SMARTLOCK_SERIAL_NUMBER], vehicle.smartlock_serial_number)

    def test_shouldModifyVehicle(self):
        # Add a vehicle to the DB
        vehicle_id = random.randint(100000, 900000)
        vehicle = VehicleContainerVehicles(vehicle_id, BackendConstants.VEHICLE_STATE_ACTIVE,
                                           BackendConstants.VEHICLE_TYPE_BIKE, str(uuid.uuid4()))
        self.vehicle_table.add_vehicle(vehicle, True)

        # update state
        vehicle.state_id = BackendConstants.VEHICLE_STATE_BROKEN
        self.vehicle_utils._update_vehicles_in_db([vehicle])

        # verify that the state has been updated
        v = self.vehicle_table.get_vehicle(vehicle.id)
        self.assertIsNotNone(v)
        self.assertEqual(v[VehicleTable.VEHICLE_STATE_ID], vehicle.state_id)

    def test_shouldDeleteVehicle(self):
        # Add a vehicle to the DB
        vehicle_id = random.randint(100000, 900000)
        vehicle = VehicleContainerVehicles(vehicle_id, BackendConstants.VEHICLE_STATE_ACTIVE,
                                           BackendConstants.VEHICLE_TYPE_BIKE, str(uuid.uuid4()))
        self.vehicle_table.add_vehicle(vehicle, True)

        # set state to deleted
        vehicle.state_id = BackendConstants.VEHICLE_STATE_DELETED
        self.vehicle_utils._update_vehicles_in_db([vehicle])

        # verify that the state has been updated
        v = self.vehicle_table.get_vehicle(vehicle.id)
        self.assertIsNone(v)

    def test_shouldAddVehiclesFromBackend(self):
        # Trigger update from backend
        self.vehicle_utils.update_from_backend()

        # check if something was added
        cursor = self.database.cursor()
        cursor.execute("SELECT COUNT(*) AS count FROM " + VehicleTable.VEHICLE_TABLE + ";")
        count = cursor.fetchone()
        self.assertIsNotNone(count)
        self.assertGreater(count["count"], 0)

        # Check that updatetoken was stored
        self.assertTrue(len(self.updatetoken_table.get_vehicle_updatetoken()) > 0)
