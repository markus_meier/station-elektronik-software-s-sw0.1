import string
import unittest

from generated.swagger_client import AccessMediaContainerAccessMedias, AccessMediaContainerData, \
    VehicleContainerVehicles
from station import StationConfiguration
from station.backend import AccessMediaApiHelper
from station.backend.rentals_api_helper import RentalsApiHelper
from station.backend.status_api_helper import StatusApiHelper
from station.backend.vehicles_api_helper import VehiclesApiHelper
from station.backend_constants import BackendConstants
from station.bluetooth.interchange_protocol_error import InterchangeProtocolError
from station.bluetooth.interchange_protocol_type import InterchangeProtocolType
from station.database.access_media_table import AccessMediaTable
from station.database.database import Database
from station.database.rental_table import RentalTable
from station.database.status_table import StatusTable
from station.database.updatetoken_table import UpdatetokenTable
from station.database.vehicle_table import VehicleTable
from station.utils.access_media_utils import AccessMediaUtils
from station.utils.bluetooth_utils import BluetoothUtils
from station.utils.rental_utils import RentalUtils
from station.utils.status_utils import StatusUtils
from station.utils.vehicle_utils import VehicleUtils


class TestBluetoothUtils(unittest.TestCase):
    def setUp(self):
        self.config = StationConfiguration("./test/station/station_configuration.cfg")
        self.database = Database(self.config)
        self.database.connect()

        self.updatetoken_table = UpdatetokenTable(self.database)

        self.access_media_table = AccessMediaTable(self.database)
        self.access_media_helper = AccessMediaApiHelper(self.config)
        self.access_media_utils = AccessMediaUtils(self.access_media_helper, self.access_media_table, self.config,
                                                   self.updatetoken_table)

        self.status_api_helper = StatusApiHelper(self.config)
        self.status_table = StatusTable(self.database)
        self.status_utils = StatusUtils(self.status_api_helper, self.status_table, self.config)

        self.rental_api_helper = RentalsApiHelper(self.config)
        self.rental_table = RentalTable(self.database)
        self.rental_utils = RentalUtils(self.rental_api_helper, self.rental_table, self.config)

        self.vehicle_api_helper = VehiclesApiHelper(self.config)
        self.vehicle_table = VehicleTable(self.database)
        self.vehicle_utils = VehicleUtils(self.vehicle_api_helper, self.vehicle_table, self.config,
                                          self.updatetoken_table)

        self.bluetooth_utils = BluetoothUtils(self.access_media_utils, self.status_utils, self.rental_utils,
                                              self.vehicle_utils)

        # Clean the DB for each test
        self.updatetoken_table.create_table(commit=True)
        self.access_media_table.create_table(commit=True)
        self.status_table.create_table(commit=True)
        self.rental_table.create_table(commit=True)
        self.vehicle_table.create_table(commit=True)

        # create requests
        self.app_start_rental_request = ("CONTAINER TYPE={} LENGTH=0 DATA=0x ; "
                                         "COMMAND TYPE={} LENGTH=12 DATA=0x313233343536373839303132 ; "
                                         "COMMAND TYPE={} LENGTH=4 DATA=0x30313031 ; "
                                         "COMMAND TYPE={} LENGTH=4 DATA=0x00000001 ; "
                                         "COMMAND TYPE={} LENGTH=2 DATA=0x0001 ; "
                                         "COMMAND TYPE={} LENGTH=2 DATA=0x0002"
                                         "".format(InterchangeProtocolType.TYPE_RENT_STATION_1,
                                                   InterchangeProtocolType.TYPE_LOCK_ID_1,
                                                   InterchangeProtocolType.TYPE_PUBLIC_KEY_1,
                                                   InterchangeProtocolType.TYPE_USER_ID_1,
                                                   InterchangeProtocolType.TYPE_SCHLOSS_BATTERY_1,
                                                   InterchangeProtocolType.TYPE_EBIKE_BATTERY_1))
        self.rfid_start_rental_request = ("CONTAINER TYPE={} LENGTH=0 DATA=0x ; "
                                          "COMMAND TYPE={} LENGTH=12 DATA=0x313233343536373839303132 ; "
                                          "COMMAND TYPE={} LENGTH=4 DATA=0x33343334 ; "
                                          "COMMAND TYPE={} LENGTH=2 DATA=0x0001 ; "
                                          "COMMAND TYPE={} LENGTH=2 DATA=0x0002"
                                          "".format(InterchangeProtocolType.TYPE_RENT_STATION_1,
                                                    InterchangeProtocolType.TYPE_LOCK_ID_1,
                                                    InterchangeProtocolType.TYPE_RFID_1,
                                                    InterchangeProtocolType.TYPE_SCHLOSS_BATTERY_1,
                                                    InterchangeProtocolType.TYPE_EBIKE_BATTERY_1))
        self.app_stop_rental_request = ("CONTAINER TYPE={} LENGTH=0 DATA=0x ; "
                                        "COMMAND TYPE={} LENGTH=12 DATA=0x313233343536373839303132 ; "
                                        "COMMAND TYPE={} LENGTH=4 DATA=0x30313031 ; "
                                        "COMMAND TYPE={} LENGTH=4 DATA=0x0000002A ; "
                                        "COMMAND TYPE={} LENGTH=2 DATA=0x0010 ; "
                                        "COMMAND TYPE={} LENGTH=2 DATA=0x2000"
                                        "".format(InterchangeProtocolType.TYPE_RENTAL_RETURN_1,
                                                  InterchangeProtocolType.TYPE_LOCK_ID_1,
                                                  InterchangeProtocolType.TYPE_PUBLIC_KEY_1,
                                                  InterchangeProtocolType.TYPE_RIDE_DURATION_1,
                                                  InterchangeProtocolType.TYPE_NUMBER_OF_INTERMEDIATE_STOPS_1,
                                                  InterchangeProtocolType.TYPE_NUMBER_OF_EMERGENCY_STOPS_1))
        self.rfid_stop_rental_request = ("CONTAINER TYPE={} LENGTH=0 DATA=0x ; "
                                         "COMMAND TYPE={} LENGTH=12 DATA=0x313233343536373839303132 ; "
                                         "COMMAND TYPE={} LENGTH=4 DATA=0x32333233 ; "
                                         "COMMAND TYPE={} LENGTH=4 DATA=0x0000002A ; "
                                         "COMMAND TYPE={} LENGTH=2 DATA=0x0010 ; "
                                         "COMMAND TYPE={} LENGTH=2 DATA=0x2000"
                                         "".format(InterchangeProtocolType.TYPE_RENTAL_RETURN_1,
                                                   InterchangeProtocolType.TYPE_LOCK_ID_1,
                                                   InterchangeProtocolType.TYPE_RFID_1,
                                                   InterchangeProtocolType.TYPE_RIDE_DURATION_1,
                                                   InterchangeProtocolType.TYPE_NUMBER_OF_INTERMEDIATE_STOPS_1,
                                                   InterchangeProtocolType.TYPE_NUMBER_OF_EMERGENCY_STOPS_1))
        self.status_update_request = ("CONTAINER TYPE={} LENGTH=0 DATA=0x ; "
                                      "COMMAND TYPE={} LENGTH=2 DATA=0x0001 ; "
                                      "COMMAND TYPE={} LENGTH=2 DATA=0x0002 ; "
                                      "COMMAND TYPE={} LENGTH=12 DATA=0x313233343536373839303132 ; "
                                      "COMMAND TYPE={} LENGTH=1 DATA=0x01"
                                      "".format(InterchangeProtocolType.TYPE_SMARTLOCK_STATUS_1,
                                                InterchangeProtocolType.TYPE_SCHLOSS_BATTERY_1,
                                                InterchangeProtocolType.TYPE_EBIKE_BATTERY_1,
                                                InterchangeProtocolType.TYPE_LOCK_ID_1,
                                                InterchangeProtocolType.TYPE_SW_VERSION_1))

    def tearDown(self):
        self.updatetoken_table.drop_table(commit=True)
        self.access_media_table.drop_table(commit=True)
        self.status_table.drop_table(commit=True)
        self.rental_table.drop_table(commit=True)
        self.vehicle_table.drop_table(commit=True)

        self.database.connection().close()

    # Phone tests
    def test_shouldNotAllowAppStartRentalForUnknownPerson(self):
        res = self.bluetooth_utils.process_incoming_request(self.app_start_rental_request)
        self.assertEqual(string.strip(res),
                         "-t 3 -l 4 -d 0x{0:08X} -s -x".format(InterchangeProtocolError.BUS_PERSON_UNKNOWN))

    def test_shouldNotAllowAppStartRentalForPaymentDeclinedPerson(self):
        self._create_person(BackendConstants.PERSON_STATE_PAYMENT_DECLINED,
                            self._create_access_media(BackendConstants.ACCESS_MEDIA_TYPE_PHONE, "0101"))
        res = self.bluetooth_utils.process_incoming_request(self.app_start_rental_request)
        self.assertEqual(string.strip(res),
                         "-t 3 -l 4 -d 0x{0:08X} -s -x".format(InterchangeProtocolError.BUS_PERSON_PAYMENT_DECLINED))

    def test_shouldNotAllowAppStartRentalForBlockedPerson(self):
        self._create_person(BackendConstants.PERSON_STATE_BLOCKED,
                            self._create_access_media(BackendConstants.ACCESS_MEDIA_TYPE_PHONE, "0101"))
        res = self.bluetooth_utils.process_incoming_request(self.app_start_rental_request)
        self.assertEqual(string.strip(res),
                         "-t 3 -l 4 -d 0x{0:08X} -s -x".format(InterchangeProtocolError.BUS_PERSON_BLOCKED))

    def test_shouldNotAllowStartAppRentalForUnknownVehicle(self):
        self._create_person(BackendConstants.PERSON_STATE_ACTIVE,
                            self._create_access_media(BackendConstants.ACCESS_MEDIA_TYPE_PHONE, "0101"))
        res = self.bluetooth_utils.process_incoming_request(self.app_start_rental_request)
        self.assertEqual(string.strip(res),
                         "-t 3 -l 4 -d 0x{0:08X} -s -x".format(InterchangeProtocolError.BUS_VEHICLE_UNKNOWN))

    def test_shouldNotAllowAppStartRentalForBlockedVehicle(self):
        self._create_person(BackendConstants.PERSON_STATE_ACTIVE,
                            self._create_access_media(BackendConstants.ACCESS_MEDIA_TYPE_PHONE, "0101"))
        self._create_vehicle(BackendConstants.VEHICLE_STATE_BLOCKED, "123456789012", "123456")
        res = self.bluetooth_utils.process_incoming_request(self.app_start_rental_request)
        self.assertEqual(string.strip(res),
                         "-t 3 -l 4 -d 0x{0:08X} -s -x".format(InterchangeProtocolError.BUS_VEHICLE_BLOCKED))

    def test_shouldAllowAppStartRental(self):
        self._create_person(BackendConstants.PERSON_STATE_ACTIVE,
                            self._create_access_media(BackendConstants.ACCESS_MEDIA_TYPE_PHONE, "0101"))
        self._create_vehicle(BackendConstants.VEHICLE_STATE_ACTIVE, "123456789012", "123456")
        res = self.bluetooth_utils.process_incoming_request(self.app_start_rental_request)
        self.assertEqual(string.strip(res), "-t 20 -l 2 -d 0x31 -a "
                                            "-t 41 -l 2 -d 0xFFFF -a "
                                            "-t 42 -l 1 -d 0x05 -a "
                                            "-t 21 -l 8 -d 0x30313031 -a "
                                            "-t 26 -c -s -x")

    def test_shouldStoreAppStartRental(self):
        self.assertEqual(len(self.rental_table.get_start_rentals()), 0)

        self._create_person(BackendConstants.PERSON_STATE_ACTIVE,
                            self._create_access_media(BackendConstants.ACCESS_MEDIA_TYPE_PHONE, "0101"))
        self._create_vehicle(BackendConstants.VEHICLE_STATE_ACTIVE, "123456789012", "123456")
        self.bluetooth_utils.process_incoming_request(self.app_start_rental_request)

        self.assertEqual(len(self.rental_table.get_start_rentals()), 1)

    def test_shouldStoreStatusUpdateOnAppStartRental(self):
        self.assertEqual(len(self.status_table.get_status_updates()), 0)

        self._create_person(BackendConstants.PERSON_STATE_ACTIVE,
                            self._create_access_media(BackendConstants.ACCESS_MEDIA_TYPE_PHONE, "0101"))
        self._create_vehicle(BackendConstants.VEHICLE_STATE_ACTIVE, "123456789012", "123456")
        self.bluetooth_utils.process_incoming_request(self.app_start_rental_request)

        self.assertEqual(len(self.status_table.get_status_updates()), 1)

    # RFID tests
    def test_shouldNotAllowRFIDStartRentalForUnknownPerson(self):
        res = self.bluetooth_utils.process_incoming_request(self.rfid_start_rental_request)
        self.assertEqual(string.strip(res),
                         "-t 3 -l 4 -d 0x{0:08X} -s -x".format(InterchangeProtocolError.BUS_PERSON_UNKNOWN))

    def test_shouldNotAllowRFIDStartRentalForPaymentDeclinedPerson(self):
        self._create_person(BackendConstants.PERSON_STATE_PAYMENT_DECLINED,
                            self._create_access_media(BackendConstants.ACCESS_MEDIA_TYPE_RFID, "3434"))
        res = self.bluetooth_utils.process_incoming_request(self.rfid_start_rental_request)
        self.assertEqual(string.strip(res),
                         "-t 3 -l 4 -d 0x{0:08X} -s -x".format(InterchangeProtocolError.BUS_PERSON_PAYMENT_DECLINED))

    def test_shouldNotAllowRFIDStartRentalForBlockedPerson(self):
        self._create_person(BackendConstants.PERSON_STATE_BLOCKED,
                            self._create_access_media(BackendConstants.ACCESS_MEDIA_TYPE_RFID, "3434"))
        res = self.bluetooth_utils.process_incoming_request(self.rfid_start_rental_request)
        self.assertEqual(string.strip(res),
                         "-t 3 -l 4 -d 0x{0:08X} -s -x".format(InterchangeProtocolError.BUS_PERSON_BLOCKED))

    def test_shouldNotAllowRFIDStartRentalForUnknownVehicle(self):
        self._create_person(BackendConstants.PERSON_STATE_ACTIVE,
                            self._create_access_media(BackendConstants.ACCESS_MEDIA_TYPE_RFID, "3434"))
        res = self.bluetooth_utils.process_incoming_request(self.rfid_start_rental_request)
        self.assertEqual(string.strip(res),
                         "-t 3 -l 4 -d 0x{0:08X} -s -x".format(InterchangeProtocolError.BUS_VEHICLE_UNKNOWN))

    def test_shouldNotAllowRFIDStartRentalForBlockedVehicle(self):
        self._create_person(BackendConstants.PERSON_STATE_ACTIVE,
                            self._create_access_media(BackendConstants.ACCESS_MEDIA_TYPE_RFID, "3434"))
        self._create_vehicle(BackendConstants.VEHICLE_STATE_BLOCKED, "123456789012", "123456")
        res = self.bluetooth_utils.process_incoming_request(self.rfid_start_rental_request)
        self.assertEqual(string.strip(res),
                         "-t 3 -l 4 -d 0x{0:08X} -s -x".format(InterchangeProtocolError.BUS_VEHICLE_BLOCKED))

    def test_shouldAllowRFIDStartRental(self):
        self._create_person(BackendConstants.PERSON_STATE_ACTIVE,
                            self._create_access_media(BackendConstants.ACCESS_MEDIA_TYPE_RFID, "3434"))
        self._create_vehicle(BackendConstants.VEHICLE_STATE_ACTIVE, "123456789012", "123456")
        res = self.bluetooth_utils.process_incoming_request(self.rfid_start_rental_request)
        self.assertEqual(string.strip(res), "-t 20 -l 2 -d 0x31 -a "
                                            "-t 41 -l 2 -d 0xFFFF -a "
                                            "-t 42 -l 1 -d 0x05 -a "
                                            "-t 19 -l 8 -d 0x33343334 -a "
                                            "-t 26 -c -s -x")

    def test_shouldStoreRFIDStartRental(self):
        self.assertEqual(len(self.rental_table.get_start_rentals()), 0)

        self._create_person(BackendConstants.PERSON_STATE_ACTIVE,
                            self._create_access_media(BackendConstants.ACCESS_MEDIA_TYPE_RFID, "3434"))
        self._create_vehicle(BackendConstants.VEHICLE_STATE_ACTIVE, "123456789012", "123456")
        self.bluetooth_utils.process_incoming_request(self.rfid_start_rental_request)

        self.assertEqual(len(self.rental_table.get_start_rentals()), 1)

    def test_shouldStoreStatusOnRFIDStartRental(self):
        self.assertEqual(len(self.status_table.get_status_updates()), 0)

        self._create_person(BackendConstants.PERSON_STATE_ACTIVE,
                            self._create_access_media(BackendConstants.ACCESS_MEDIA_TYPE_RFID, "3434"))
        self._create_vehicle(BackendConstants.VEHICLE_STATE_ACTIVE, "123456789012", "123456")
        self.bluetooth_utils.process_incoming_request(self.rfid_start_rental_request)

        self.assertEqual(len(self.status_table.get_status_updates()), 1)

    # Stop rentals
    # Phone
    def test_shouldNotAllowPhoneStopRentalForUnknownVehicle(self):
        self._create_person(BackendConstants.PERSON_STATE_ACTIVE,
                            self._create_access_media(BackendConstants.ACCESS_MEDIA_TYPE_PHONE, "0101"))
        res = self.bluetooth_utils.process_incoming_request(self.app_stop_rental_request)
        self.assertEqual(string.strip(res),
                         "-t 3 -l 4 -d 0x{0:08X} -s -x".format(InterchangeProtocolError.BUS_VEHICLE_UNKNOWN))

    def test_shouldNotAllowPhoneStopRentalForUnknownPerson(self):
        self._create_vehicle(BackendConstants.VEHICLE_STATE_ACTIVE, "123456789012", "123456")
        res = self.bluetooth_utils.process_incoming_request(self.app_stop_rental_request)
        self.assertEqual(string.strip(res),
                         "-t 3 -l 4 -d 0x{0:08X} -s -x".format(InterchangeProtocolError.BUS_PERSON_UNKNOWN))

    def test_shouldHandlePhoneStopRental(self):
        # prepare person and vehicle
        self._create_person(BackendConstants.PERSON_STATE_ACTIVE,
                            self._create_access_media(BackendConstants.ACCESS_MEDIA_TYPE_PHONE, "0101"))
        self._create_vehicle(BackendConstants.VEHICLE_STATE_ACTIVE, "123456789012", "123456")
        self.assertEqual(len(self.rental_table.get_stop_rentals()), 0)

        # send request
        res = self.bluetooth_utils.process_incoming_request(self.app_stop_rental_request)
        # and verify result and stop rental table
        self.assertEqual(string.strip(res), "-t 3 -l 4 -d 0x{0:08X} -s -x".format(InterchangeProtocolError.BUS_OK))
        self.assertEqual(len(self.rental_table.get_stop_rentals()), 1)

    # RFID
    def test_shouldNotAllowRFIDStopRentalForUnknownVehicle(self):
        self._create_person(BackendConstants.PERSON_STATE_ACTIVE,
                            self._create_access_media(BackendConstants.ACCESS_MEDIA_TYPE_RFID, "2323"))
        res = self.bluetooth_utils.process_incoming_request(self.rfid_stop_rental_request)
        self.assertEqual(string.strip(res),
                         "-t 3 -l 4 -d 0x{0:08X} -s -x".format(InterchangeProtocolError.BUS_VEHICLE_UNKNOWN))

    def test_shouldNotAllowRFIDStopRentalForUnknownPerson(self):
        self._create_vehicle(BackendConstants.VEHICLE_STATE_ACTIVE, "123456789012", "123456")
        res = self.bluetooth_utils.process_incoming_request(self.rfid_stop_rental_request)
        self.assertEqual(string.strip(res),
                         "-t 3 -l 4 -d 0x{0:08X} -s -x".format(InterchangeProtocolError.BUS_PERSON_UNKNOWN))

    def test_shouldHandleRFIDStopRental(self):
        # prepare person and vehicle
        self._create_person(BackendConstants.PERSON_STATE_ACTIVE,
                            self._create_access_media(BackendConstants.ACCESS_MEDIA_TYPE_RFID, "2323"))
        self._create_vehicle(BackendConstants.VEHICLE_STATE_ACTIVE, "123456789012", "123456")
        self.assertEqual(len(self.rental_table.get_stop_rentals()), 0)

        # send request
        res = self.bluetooth_utils.process_incoming_request(self.rfid_stop_rental_request)
        # and verify result and stop rental table
        self.assertEqual(string.strip(res), "-t 3 -l 4 -d 0x{0:08X} -s -x".format(InterchangeProtocolError.BUS_OK))
        self.assertEqual(len(self.rental_table.get_stop_rentals()), 1)

    # smartlock status
    def test_shouldNotHandleSmartLockStatusUpdateFromUnknownVehicle(self):
        self.assertEqual(len(self.status_table.get_status_updates()), 0)
        res = self.bluetooth_utils.process_incoming_request(self.status_update_request)
        self.assertEqual(string.strip(res),
                         "-t 3 -l 4 -d 0x{0:08X} -s -x".format(InterchangeProtocolError.BUS_VEHICLE_UNKNOWN))
        self.assertEqual(len(self.status_table.get_status_updates()), 0)

    def test_shouldHandleSmartLockStatusUpdateFromEbike(self):
        self._create_vehicle(BackendConstants.VEHICLE_STATE_ACTIVE, "123456789012", "012345")
        self.assertEqual(len(self.status_table.get_status_updates()), 0)
        res = self.bluetooth_utils.process_incoming_request(self.status_update_request)
        self.assertEqual(string.strip(res), "-t 8 -l 4 -d 0x00000001 -a "
                                            "-t 4 -l 6 -d 0x303132333435 -a "
                                            "-t 38 -c "
                                            "-s "
                                            "-x".format())
        db_entries = self.status_table.get_status_updates()
        self.assertEqual(len(db_entries), 1)
        self.assertIsNotNone(db_entries[0][StatusTable.VEHICLE_STATUS_SMARTLOCK_LEVEL])
        self.assertIsNotNone(db_entries[0][StatusTable.VEHICLE_STATUS_EBIKE_LEVEL])

    def test_shouldHandleSmartLockStatusUpdateFromBike(self):
        self._create_vehicle(BackendConstants.VEHICLE_STATE_ACTIVE, "123456789012", "012345")
        self.assertEqual(len(self.status_table.get_status_updates()), 0)

        status_update_request = ("CONTAINER TYPE={} LENGTH=0 DATA=0x ; "
                                 "COMMAND TYPE={} LENGTH=2 DATA=0x0001 ; "
                                 "COMMAND TYPE={} LENGTH=4 DATA=0x00000001 ; "
                                 "COMMAND TYPE={} LENGTH=12 DATA=0x313233343536373839303132 ; "
                                 "COMMAND TYPE={} LENGTH=1 DATA=0x01"
                                 "".format(InterchangeProtocolType.TYPE_SMARTLOCK_STATUS_1,
                                           InterchangeProtocolType.TYPE_SCHLOSS_BATTERY_1,
                                           InterchangeProtocolType.TYPE_VEHICLE_TYPE_1,
                                           InterchangeProtocolType.TYPE_LOCK_ID_1,
                                           InterchangeProtocolType.TYPE_SW_VERSION_1))
        res = self.bluetooth_utils.process_incoming_request(status_update_request)
        self.assertEqual(string.strip(res), "-t 8 -l 4 -d 0x00000001 -a "
                                            "-t 4 -l 6 -d 0x303132333435 -a "
                                            "-t 38 -c "
                                            "-s "
                                            "-x".format())
        db_entries = self.status_table.get_status_updates()
        self.assertEqual(len(db_entries), 1)
        self.assertIsNotNone(db_entries[0][StatusTable.VEHICLE_STATUS_SMARTLOCK_LEVEL])
        self.assertIsNone(db_entries[0][StatusTable.VEHICLE_STATUS_EBIKE_LEVEL])

    @staticmethod
    def _create_access_media(access_media_type, public_key):
        # type: (int, str) -> AccessMediaContainerAccessMedias
        return AccessMediaContainerAccessMedias(1, public_key, access_media_type,
                                                BackendConstants.ACCESS_MEDIA_STATE_ACTIVE)

    def _create_person(self, person_state, access_media):
        # type: (int, AccessMediaContainerAccessMedias) -> None
        data = AccessMediaContainerData(1, person_state, [access_media])
        self.access_media_table.add_person_with_access_media(data, True)

    def _create_vehicle(self, vehicle_state, smartlock_serial_number, betriebsnummer):
        # type: (int, str) -> None
        vehicle = VehicleContainerVehicles(1, vehicle_state,
                                           BackendConstants.VEHICLE_TYPE_BIKE, smartlock_serial_number, betriebsnummer)
        self.vehicle_table.add_vehicle(vehicle, True)
