import time
import unittest
from distutils.version import LooseVersion

import urllib3
from urllib3.exceptions import MaxRetryError

from station import StationConfiguration
from station.backend.rentals_api_helper import RentalsApiHelper
from station.database.database import Database
from station.database.rental_table import RentalTable
from station.utils.rental_utils import RentalUtils


class TestRentalUtils(unittest.TestCase):
    def setUp(self):
        self.config = StationConfiguration("./test/station/station_configuration.cfg")
        self.api = RentalsApiHelper(self.config)
        self.database = Database(self.config)
        self.database.connect()
        self.rental_table = RentalTable(self.database)
        self.rental_utils = RentalUtils(self.api, self.rental_table, self.config)

        # Clean the DB for each test
        self.rental_table.drop_table(commit=False)
        self.rental_table.create_table(commit=True)

    def tearDown(self):
        self.rental_table.drop_table(commit=True)
        self.database.connection().close()

    def test_shouldAddStartRental(self):
        self.assertEqual(len(self.rental_table.get_start_rentals()), 0)
        self.rental_utils.add_start_rental(3, 2)
        self.assertGreater(len(self.rental_table.get_start_rentals()), 0)

    def test_shouldAddStopRental(self):
        self.assertEqual(len(self.rental_table.get_stop_rentals()), 0)
        self.rental_utils.add_stop_rental(3, 42, 1, 2, 2, False)
        self.assertGreater(len(self.rental_table.get_stop_rentals()), 0)

    @unittest.skipIf(LooseVersion(urllib3.__version__) < LooseVersion("1.21.1"), "urllib3 version too old")
    def test_shouldPushStartAndStopRentalToBackend(self):
        self.rental_utils.add_start_rental(3, 2)
        # Delay stop rental a bit
        time.sleep(2)
        self.rental_utils.add_stop_rental(3, 42, 1, 2, 2, False)

        # Push to backend
        self.rental_utils.push_to_backend()

        # Ensure that our local tables are emptied
        self.assertEqual(len(self.rental_table.get_stop_rentals()), 0)
        self.assertEqual(len(self.rental_table.get_start_rentals()), 0)

    @unittest.skipIf(LooseVersion(urllib3.__version__) < LooseVersion("1.21.1"), "urllib3 version too old")
    def test_shouldPushNothingToBackend(self):
        # Push empty rental and status list to backend
        # This shouldn't raise an exception
        self.rental_utils.push_to_backend()

    @unittest.skipIf(LooseVersion(urllib3.__version__) < LooseVersion("1.21.1"), "urllib3 version too old")
    def test_shouldNotDeleteLocalDbEntriesWhenBackendIsNotAvailable(self):
        self.rental_utils.add_start_rental(3, 2)
        self.rental_utils.add_stop_rental(3, 42, 1, 2, 2, False)

        # Add a wrong endpoint
        self.api._api_client.host = "http://localhost.invalid"
        # And try to push to backend
        with self.assertRaises(MaxRetryError):
            self.rental_utils.push_to_backend()

        # Ensure that the local db entries are still available
        self.assertEqual(len(self.rental_table.get_stop_rentals()), 1)
        self.assertEqual(len(self.rental_table.get_start_rentals()), 1)

