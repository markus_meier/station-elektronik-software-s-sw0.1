import random
import unittest

from generated.swagger_client import AccessMediaContainerData, AccessMediaContainerAccessMedias
from station import StationConfiguration
from station.backend import AccessMediaApiHelper
from station.backend_constants import BackendConstants
from station.database.access_media_table import AccessMediaTable
from station.database.database import Database
from station.database.updatetoken_table import UpdatetokenTable
from station.utils.access_media_utils import AccessMediaUtils


class TestAccessMediaUtils(unittest.TestCase):
    def setUp(self):
        self.config = StationConfiguration("./test/station/station_configuration.cfg")
        self.api = AccessMediaApiHelper(self.config)
        self.database = Database(self.config)
        self.database.connect()
        self.access_media_table = AccessMediaTable(self.database)
        self.updatetoken_table = UpdatetokenTable(self.database)
        self.access_media_utils = AccessMediaUtils(self.api, self.access_media_table, self.config,
                                                   self.updatetoken_table)

        # Clean the DB for each test
        self.access_media_table.create_table(commit=True)
        self.updatetoken_table.create_table()

        # Test data
        self.person_id = random.randint(100000, 900000)
        self.access_media_id = random.randint(100000, 900000)
        self.access_media = AccessMediaContainerAccessMedias(self.access_media_id, "blubb",
                                                             BackendConstants.ACCESS_MEDIA_TYPE_PHONE,
                                                             BackendConstants.ACCESS_MEDIA_STATE_ACTIVE)
        self.data = AccessMediaContainerData(self.person_id, BackendConstants.PERSON_STATE_PENDING, [self.access_media])

    def tearDown(self):
        self.access_media_table.drop_table(commit=True)
        self.database.connection().close()

    def test_shouldAddNewPersonAndAccessMedia(self):
        self.access_media_utils._update_access_medias_in_db([self.data])
        v = self.access_media_table.get_person(self.person_id)
        self.assertIsNotNone(v)

    def test_shouldModifyPersonAndAccessMedia(self):
        # Add a person to the DB
        self.access_media_table.add_person_with_access_media(self.data, True)

        # update state
        self.data.state_id = BackendConstants.PERSON_STATE_ACTIVE
        self.access_media.access_media = "new identifier"
        self.access_media_utils._update_access_medias_in_db([self.data])

        # verify that the state has been updated
        v = self.access_media_table.get_person(self.person_id)
        self.assertIsNotNone(v)
        self.assertEqual(v[AccessMediaTable.PERSON_STATE_ID], self.data.state_id)
        cursor = self.database.cursor()
        cursor.execute("SELECT " + AccessMediaTable.ACCESS_MEDIA_ACCESS_MEDIA + " FROM " +
                       AccessMediaTable.ACCESS_MEDIA_TABLE + " WHERE " + AccessMediaTable.ACCESS_MEDIA_PERSON_ID +
                       " = ?", (self.person_id,))
        result = cursor.fetchall()
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0][AccessMediaTable.ACCESS_MEDIA_ACCESS_MEDIA], self.access_media.access_media)

    def test_shouldRemoveDisabledAccessMediaFromPerson(self):
        # Add a person to the DB
        self.access_media_table.add_person_with_access_media(self.data, True)

        # update state to inactive
        self.access_media.state = BackendConstants.ACCESS_MEDIA_STATE_INACTIVE
        self.access_media_utils._update_access_medias_in_db([self.data])

        # verify that the access media is gone
        self.assertEqual(self._get_access_media_count(), 0)

    def test_shouldDeletePersonAndAccessMedia(self):
        # Add a person to the DB
        self.access_media_table.add_person_with_access_media(self.data, True)

        # update state to deleted
        self.data.state_id = BackendConstants.PERSON_STATE_DELETED
        self.access_media_utils._update_access_medias_in_db([self.data])

        # verify that the person is gone
        v = self.access_media_table.get_person(self.person_id)
        self.assertIsNone(v)

    def test_shouldAddPersonsAndAccessMediasFromBackend(self):
        # Updatetoken should be empty
        self.assertEqual(self.updatetoken_table.get_access_media_updatetoken(), "")

        # Trigger update from backend
        self.access_media_utils.update_from_backend()

        # check if something was added
        cursor = self.database.cursor()
        cursor.execute("SELECT COUNT(*) AS count FROM " + AccessMediaTable.PERSON_TABLE + ";")
        count = cursor.fetchone()
        self.assertIsNotNone(count)
        self.assertGreater(count["count"], 0)

        self.assertGreater(self._get_access_media_count(), 0)

        # Check that updatetoken was stored
        self.assertTrue(len(self.updatetoken_table.get_access_media_updatetoken()) > 0)

    def test_shouldAddNewPersonWithoutDisabledAccessMedia(self):
        self.access_media.state = BackendConstants.ACCESS_MEDIA_STATE_INACTIVE
        self.access_media_utils._update_access_medias_in_db([self.data])
        v = self.access_media_table.get_person(self.person_id)
        self.assertIsNotNone(v)

        # verify that there's no access-media for this person
        self.assertEqual(self._get_access_media_count(), 0)

    def test_shouldRemoveAccessMedia(self):
        # Add a person to the DB
        self.access_media_table.add_person_with_access_media(self.data, True)

        # Set access media to state inactive
        self.access_media.state = BackendConstants.ACCESS_MEDIA_STATE_INACTIVE
        self.access_media_utils._update_access_medias_in_db([self.data])

        # verify that the access media has been removed from DB
        cursor = self.database.cursor()
        cursor.execute("SELECT " + AccessMediaTable.ACCESS_MEDIA_ACCESS_MEDIA + " FROM " +
                       AccessMediaTable.ACCESS_MEDIA_TABLE + " WHERE " + AccessMediaTable.ACCESS_MEDIA_ID + " = ?",
                       (self.access_media_id,))
        result = cursor.fetchone()
        self.assertIsNone(result)

    def test_shouldAddNewAccessMedia(self):
        # Add a person to the DB
        self.access_media_table.add_person_with_access_media(self.data, True)

        # Create and add additional access media
        additional_access_media_id = random.randint(100000, 900000)
        additional_access_media = AccessMediaContainerAccessMedias(additional_access_media_id, "blubb2",
                                                                   BackendConstants.ACCESS_MEDIA_TYPE_PHONE,
                                                                   BackendConstants.ACCESS_MEDIA_STATE_ACTIVE)
        data = AccessMediaContainerData(self.person_id, BackendConstants.PERSON_STATE_PENDING,
                                        [additional_access_media])
        self.access_media_utils._update_access_medias_in_db([data])

        # verify that we have two access medias now
        cursor = self.database.cursor()
        cursor.execute("SELECT " + AccessMediaTable.ACCESS_MEDIA_ACCESS_MEDIA + " FROM " +
                       AccessMediaTable.ACCESS_MEDIA_TABLE + " WHERE " + AccessMediaTable.ACCESS_MEDIA_PERSON_ID +
                       " = ?", (self.person_id,))
        result = cursor.fetchall()
        self.assertIsNotNone(result)
        self.assertEqual(len(result), 2)

    def _get_access_media_count(self):
        cursor = self.database.cursor()
        cursor.execute("SELECT COUNT(*) AS count FROM " + AccessMediaTable.ACCESS_MEDIA_TABLE + ";")
        r = cursor.fetchone()
        self.assertEqual(len(r), 1)
        return r["count"]
