import unittest

from station import StationConfiguration
from station.database.configuration_table import ConfigurationTable
from station.database.database import Database


class TestConfigurationTable(unittest.TestCase):
    def setUp(self):
        config = StationConfiguration(file_name="./test/station/station_configuration.cfg")
        self.database = Database(config)
        self.database.connect()
        self.configuration_table = ConfigurationTable(self.database)

        self.configuration_table.create_table(commit=True)

    def tearDown(self):
        self.configuration_table.drop_table(commit=True)
        self.database.connection().close()

    def test_shouldUpdateStationConfigurationTable(self):
        configuration = [{"key": "the key", "value": "the value"}]
        self.configuration_table.update_station_configuration(configuration, commit=True)
        self._check_table_contains_entries(ConfigurationTable.STATION_CONFIGURATION_TABLE, 1)

    def test_shouldUpdateSmartlockConfigurationTable(self):
        configuration = [{"key": "the key", "value": "the value"}]
        self.configuration_table.update_smartlock_configuration(configuration, commit=True)
        self._check_table_contains_entries(ConfigurationTable.SMARTLOCK_CONFIGURATION_TABLE, 1)

    def _check_table_contains_entries(self, table="", entries=0):
        cursor = self.database.cursor()
        cursor.execute("SELECT COUNT(*) AS count FROM " + table + ";")
        res = cursor.fetchone()
        self.assertIsNotNone(res)
        self.assertEqual(res["count"], entries)
