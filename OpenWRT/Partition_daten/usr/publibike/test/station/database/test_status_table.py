import unittest

from station import StationConfiguration
from station.database.database import Database
from station.database.status_table import StatusTable


class TestRentalTable(unittest.TestCase):
    def setUp(self):
        config = StationConfiguration(file_name="./test/station/station_configuration.cfg")
        self.database = Database(config)
        self.database.connect()
        self.status_table = StatusTable(self.database)

        self.status_table.drop_table(commit=False)
        self.status_table.create_table(commit=True)

    def tearDown(self):
        self.status_table.drop_table(commit=True)
        self.database.connection().close()

    def test_shouldAddStatusEntry(self):
        self.assertEqual(self._get_status_count(), 0)
        self.status_table.add_status_update(vehicle_id=1, smartlock_battery_level=10, ebike_battery_level=20,
                                            commit=True)
        self.assertEqual(self._get_status_count(), 1)

    def test_shouldGetStatusEntry(self):
        # check empty table
        result = self.status_table.get_status_updates()
        self.assertIsNotNone(result)
        self.assertEqual(len(result), 0)

        # add entry and check again
        self.status_table.add_status_update(vehicle_id=1, smartlock_battery_level=10, ebike_battery_level=20,
                                            commit=True)
        result = self.status_table.get_status_updates()
        self.assertIsNotNone(result)
        self.assertEqual(len(result), 1)

    def test_shouldDeleteEntries(self):
        self.status_table.add_status_update(vehicle_id=1, smartlock_battery_level=10, ebike_battery_level=20,
                                            commit=True)

        self.status_table.delete_entries(commit=True)
        self.assertEqual(self._get_status_count(), 0)

    def _get_status_count(self):
        cursor = self.database.cursor()
        cursor.execute("SELECT COUNT(*) AS count FROM " + StatusTable.VEHICLE_STATUS_TABLE + ";")
        return cursor.fetchone()["count"]
