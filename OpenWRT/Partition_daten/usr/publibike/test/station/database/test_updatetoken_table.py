import unittest

from station import StationConfiguration
from station.database.database import Database
from station.database.updatetoken_table import UpdatetokenTable


class TestUpdatetokenTable(unittest.TestCase):
    def setUp(self):
        config = StationConfiguration(file_name="./test/station/station_configuration.cfg")
        self.database = Database(config)
        self.database.connect()
        self.updatetoken_table = UpdatetokenTable(self.database)
        self.updatetoken_table.drop_table(commit=True)
        self.updatetoken_table.create_table(commit=True)

    def tearDown(self):
        self.updatetoken_table.drop_table(commit=True)
        self.database.connection().close()

    def test_shouldAccessUpdatetoken(self):
        # init table (again)
        self.updatetoken_table.create_table()

        # check vehicle updatetoken values
        self.assertEqual(self.updatetoken_table.get_vehicle_updatetoken(), "")
        self.updatetoken_table.set_vehicle_updatetoken("123456", True)
        self.assertEqual(self.updatetoken_table.get_vehicle_updatetoken(), "123456")

        # check access-media updatetoken values
        self.assertEqual(self.updatetoken_table.get_access_media_updatetoken(), "")
        self.updatetoken_table.set_access_media_updatetoken("42", True)
        self.assertEqual(self.updatetoken_table.get_access_media_updatetoken(), "42")

    def test_shouldNotOverrideExistingValuesWhenRecreated(self):
        self.updatetoken_table.set_vehicle_updatetoken("123456", True)
        self.updatetoken_table.create_table()
        self.assertEqual(self.updatetoken_table.get_vehicle_updatetoken(), "123456")
