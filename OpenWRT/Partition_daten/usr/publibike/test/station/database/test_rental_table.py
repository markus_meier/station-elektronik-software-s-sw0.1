import unittest

import datetime

from station import StationConfiguration
from station.database.database import Database
from station.database.rental_table import RentalTable


class TestRentalTable(unittest.TestCase):
    def setUp(self):
        config = StationConfiguration(file_name="./test/station/station_configuration.cfg")
        self.database = Database(config)
        self.database.connect()
        self.rental_table = RentalTable(self.database)

        self.rental_table.drop_table(commit=False)
        self.rental_table.create_table(commit=True)

    def tearDown(self):
        self.rental_table.drop_table(commit=True)
        self.database.connection().close()

    def test_shouldAddStartRental(self):
        self.assertEqual(self._get_start_rental_count(), 0)
        self.rental_table.add_start_rental(1, 2, datetime.datetime.now(), commit=True)
        self.assertEqual(self._get_start_rental_count(), 1)

    def test_shouldAddStopRental(self):
        self.assertEqual(self._get_stop_rental_count(), 0)
        self.rental_table.add_stop_rental(1, datetime.datetime.now(), 42, 1, 2, False, 3, commit=True)
        self.assertEqual(self._get_stop_rental_count(), 1)

    def test_shouldGetStartRentals(self):
        self.assertEqual(len(self.rental_table.get_start_rentals()), 0)
        self.rental_table.add_start_rental(1, 2, datetime.datetime.now(), commit=True)
        self.assertEqual(len(self.rental_table.get_start_rentals()), 1)

    def test_shouldGetStopRentals(self):
        self.assertEqual(len(self.rental_table.get_stop_rentals()), 0)
        self.rental_table.add_stop_rental(1, datetime.datetime.now(), 42, 1, 2, False, 3, commit=True)
        self.assertEqual(len(self.rental_table.get_stop_rentals()), 1)

    def test_shouldDeleteStartRentals(self):
        self.assertEqual(self._get_start_rental_count(), 0)

        # Add one start rental
        self.rental_table.add_start_rental(1, 2, datetime.datetime.now(), commit=True)
        self.assertEqual(self._get_start_rental_count(), 1)

        # Call delete without any id
        self.rental_table.delete_start_rentals([], commit=True)
        self.assertEqual(self._get_start_rental_count(), 1)

        # Call delete without any id
        start_rentals = self.rental_table.get_start_rentals()
        self.rental_table.delete_start_rentals(start_rentals, commit=True)
        self.assertEqual(self._get_start_rental_count(), 0)

    def test_shouldDeleteStopRentals(self):
        self.assertEqual(self._get_stop_rental_count(), 0)

        # Add one stop rental
        self.rental_table.add_stop_rental(1, datetime.datetime.now(), 42, 1, 2, False, 3, commit=True)
        self.assertEqual(self._get_stop_rental_count(), 1)

        # Call delete without ids
        self.rental_table.delete_stop_rentals([], commit=True)
        self.assertEqual(self._get_stop_rental_count(), 1)

        # And now delete the IDs we have in the DB
        stop_rentals = self.rental_table.get_stop_rentals()
        self.rental_table.delete_stop_rentals(stop_rentals, commit=True)
        self.assertEqual(self._get_stop_rental_count(), 0)

    def _get_start_rental_count(self):
        cursor = self.database.cursor()
        cursor.execute("SELECT COUNT(*) AS count FROM " + RentalTable.START_RENTAL_TABLE + ";")
        return cursor.fetchone()["count"]

    def _get_stop_rental_count(self):
        cursor = self.database.cursor()
        cursor.execute("SELECT COUNT(*) AS count FROM " + RentalTable.STOP_RENTAL_TABLE + ";")
        return cursor.fetchone()["count"]
