import unittest

from generated.swagger_client import VehicleContainerVehicles
from station.backend_constants import BackendConstants
from station.database.database import Database
from station.database.vehicle_table import VehicleTable
from station.station_configuration import StationConfiguration


class TestVehicleTable(unittest.TestCase):
    def setUp(self):
        config = StationConfiguration(file_name="./test/station/station_configuration.cfg")
        self.database = Database(config)
        self.database.connect()
        self.vehicle_table = VehicleTable(self.database)
        self.vehicle_table.create_table(commit=True)

    def tearDown(self):
        self.vehicle_table.drop_table(commit=True)
        self.database.connection().close()

    def test_shouldCreateTable(self):
        # Call it twice, so we know that deleting also works
        self.vehicle_table.create_table(commit=True)

    def test_shouldModifyVehicleEntries(self):
        # Add a vehicle
        vehicle = VehicleContainerVehicles(42, BackendConstants.VEHICLE_STATE_ACTIVE,
                                           BackendConstants.VEHICLE_TYPE_BIKE, "42", "123456")
        self.vehicle_table.add_vehicle(vehicle, True)
        v = self.vehicle_table.get_vehicle(42)
        self.assertIsNotNone(v)

        # Update vehicle
        vehicle.state_id = BackendConstants.VEHICLE_STATE_BLOCKED
        vehicle.type_id = BackendConstants.VEHICLE_TYPE_EBIKE
        vehicle.smartlock_serial_number = "43"
        vehicle.operations_number = "abcdef"
        self.vehicle_table.update_vehicle(vehicle, True)
        v = self.vehicle_table.get_vehicle(42)
        self.assertEqual(v, {"id": 42, "state_id": BackendConstants.VEHICLE_STATE_BLOCKED,
                             "type_id": BackendConstants.VEHICLE_TYPE_EBIKE, "smartlock_serial_number": "43",
                             "operations_number": "abcdef"})

        # Delete vehicle
        self.vehicle_table.remove_vehicle(42, True)
        v = self.vehicle_table.get_vehicle(42)
        self.assertIsNone(v)

    def test_shouldGetVehicleByOperationsNumber(self):
        v = self.vehicle_table.get_vehicle_from_smartlock_serial_number("42")
        self.assertIsNone(v)

        # Add a vehicle
        vehicle = VehicleContainerVehicles(42, BackendConstants.VEHICLE_STATE_ACTIVE,
                                           BackendConstants.VEHICLE_TYPE_BIKE, "42", "123456")
        self.vehicle_table.add_vehicle(vehicle, True)
        v = self.vehicle_table.get_vehicle_from_smartlock_serial_number("42")
        self.assertIsNotNone(v)
