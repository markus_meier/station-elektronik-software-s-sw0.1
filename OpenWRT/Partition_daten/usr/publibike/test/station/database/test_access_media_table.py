import random
import unittest

from generated.swagger_client import AccessMediaContainerData, AccessMediaContainerAccessMedias
from station import StationConfiguration
from station.backend_constants import BackendConstants
from station.database.access_media_table import AccessMediaTable
from station.database.database import Database


class TestAccessMediaTable(unittest.TestCase):
    def setUp(self):
        # prepare classes needed for this test
        config = StationConfiguration(file_name="./test/station/station_configuration.cfg")
        self.database = Database(config)
        self.database.connect()
        self.access_media_table = AccessMediaTable(self.database)
        # Make sure that the table exists
        self.access_media_table.create_table(commit=True)

        # Prepare test data
        self.person_id = random.randint(100000, 900000)
        self.access_media_id = random.randint(100000, 900000)
        self.access_media = AccessMediaContainerAccessMedias(self.access_media_id, "blubb",
                                                             BackendConstants.ACCESS_MEDIA_TYPE_PHONE,
                                                             BackendConstants.ACCESS_MEDIA_STATE_ACTIVE)
        self.container = AccessMediaContainerData(self.person_id, BackendConstants.PERSON_STATE_PENDING,
                                                  [self.access_media])

    def tearDown(self):
        self.access_media_table.drop_table(commit=True)
        self.database.connection().close()

    def test_shouldCreateTable(self):
        # Recreate the table once again to see that this doesn't throw
        self.access_media_table.create_table()

    def test_shouldAddPersonAndAccessMedia(self):
        self.assertIsNone(self.access_media_table.get_person(self.container.person_id))
        self.access_media_table.add_person_with_access_media(self.container, True)
        record = self.access_media_table.get_person(self.container.person_id)
        self.assertIsNotNone(record)
        self.assertEqual(record[AccessMediaTable.PERSON_ID], self.person_id)

    def test_shouldRemovePersonAndAccessMedia(self):
        self.access_media_table.add_person_with_access_media(self.container, True)
        self.assertIsNotNone(self.access_media_table.get_person(self.container.person_id))
        self.access_media_table.remove_person_with_access_media(self.person_id, True)
        self.assertIsNone(self.access_media_table.get_person(self.container.person_id))

    def test_shouldGetPersonFromAccessMedia(self):
        # access media not in DB
        res = self.access_media_table.get_person_from_access_media(self.access_media.type,
                                                                   self.access_media.access_media)
        self.assertIsNone(res)

        # Add the access media
        self.access_media_table.add_person_with_access_media(self.container, True)

        # Now we should find it
        res = self.access_media_table.get_person_from_access_media(self.access_media.type,
                                                                   self.access_media.access_media)
        self.assertIsNotNone(res)
        self.assertEqual(len(res), 3)
        self.assertGreater(res[AccessMediaTable.PERSON_ID], 0)
        self.assertGreater(res[AccessMediaTable.ACCESS_MEDIA_ID], 0)

    def test_shouldGetAccessMediaForPerson(self):
        # access media not in DB
        res = self.access_media_table.get_access_medias_for_person(self.person_id)
        self.assertEqual(len(res), 0)

        # Add the access media
        self.access_media_table.add_person_with_access_media(self.container, True)

        # Now we should find it
        res = self.access_media_table.get_access_medias_for_person(self.person_id)
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0][AccessMediaTable.ACCESS_MEDIA_TYPE_ID], BackendConstants.ACCESS_MEDIA_TYPE_PHONE)
        self.assertEqual(res[0][AccessMediaTable.ACCESS_MEDIA_ACCESS_MEDIA], "blubb")

    def test_shouldRemoveAccessMedia(self):
        # Add person + access media
        self.access_media_table.add_person_with_access_media(self.container, True)
        res = self.access_media_table.get_access_medias_for_person(self.person_id)
        self.assertEqual(len(res), 1)

        # drop access media
        self.access_media_table.delete_access_media(self.access_media_id)
        res = self.access_media_table.get_access_medias_for_person(self.person_id)
        self.assertEqual(len(res), 0)

        # check that this doesn't raise an exception
        self.access_media_table.delete_access_media(self.access_media_id)

    def test_shouldAddAccessMedia(self):
        # Add person + access media
        self.access_media_table.add_person_with_access_media(self.container, True)
        res = self.access_media_table.get_access_medias_for_person(self.person_id)
        self.assertEqual(len(res), 1)

        # add additional access media
        access_media_id = random.randint(100000, 900000)
        access_media = AccessMediaContainerAccessMedias(access_media_id, "blubb2",
                                                        BackendConstants.ACCESS_MEDIA_TYPE_PHONE,
                                                        BackendConstants.ACCESS_MEDIA_STATE_ACTIVE)
        self.access_media_table.add_access_media_for_person(access_media, self.person_id, commit=True)
        res = self.access_media_table.get_access_medias_for_person(self.person_id)
        self.assertEqual(len(res), 2)

    def test_shouldUpdatePerson(self):
        # Add person + access media
        self.access_media_table.add_person_with_access_media(self.container, True)
        res = self.access_media_table.get_access_medias_for_person(self.person_id)
        self.assertEqual(len(res), 1)

        # update person state
        self.container.state_id = BackendConstants.PERSON_STATE_ACTIVE
        self.access_media_table.update_person(self.container, commit=True)
        res = self.access_media_table.get_person(self.person_id)
        self.assertEqual(res[AccessMediaTable.PERSON_STATE_ID], BackendConstants.PERSON_STATE_ACTIVE)

    def test_shouldGetAccessMedia(self):
        res = self.access_media_table.get_access_media(self.access_media_id)
        self.assertIsNone(res)

        self.access_media_table.add_person_with_access_media(self.container, True)
        res = self.access_media_table.get_access_media(self.access_media_id)
        self.assertIsNotNone(res)

    def test_shouldUpdateAccessMedia(self):
        # Add person + access media
        self.access_media_table.add_person_with_access_media(self.container, True)
        res = self.access_media_table.get_access_medias_for_person(self.person_id)
        self.assertEqual(len(res), 1)

        # update access media
        self.access_media.access_media = "new value"
        self.access_media_table.update_access_media(self.access_media, commit=True)
        res = self.access_media_table.get_access_media(self.access_media_id)
        self.assertEqual(res[AccessMediaTable.ACCESS_MEDIA_ACCESS_MEDIA], "new value")

