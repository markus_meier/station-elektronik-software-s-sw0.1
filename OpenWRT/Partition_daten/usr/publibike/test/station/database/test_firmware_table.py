import unittest

from generated.swagger_client import Firmware
from station import StationConfiguration
from station.database.database import Database
from station.database.firmware_table import FirmwareTable


class TestFirmwareTable(unittest.TestCase):
    def setUp(self):
        config = StationConfiguration(file_name="./test/station/station_configuration.cfg")
        self.database = Database(config)
        self.database.connect()
        self.firmware_table = FirmwareTable(self.database)

    def tearDown(self):
        self.firmware_table.drop_table(commit=True)
        self.database.connection().close()

    def test_shouldUpdateFirmwareTable(self):
        # init table
        self.firmware_table.drop_table(commit=True)
        self.firmware_table.create_table(commit=True)

        # Check that table is empty
        self._verify_firmware_entry_count(0)

        # Update firmware with 2 entries
        f1 = Firmware(1, "bla", "1.0", "1.0", "url1")
        f2 = Firmware(1, "bla", "1.1", "1.1", "url1.1")
        self.firmware_table.update_firmware([f1, f2], commit=True)
        self._verify_firmware_entry_count(2)

        # Update with 1 entry --> only 1 should be there
        self.firmware_table.update_firmware([f1], commit=True)
        self._verify_firmware_entry_count(1)

    def _verify_firmware_entry_count(self, expected_count=0):
        cursor = self.database.cursor()
        cursor.execute("SELECT COUNT(*) AS count FROM " + FirmwareTable.FIRMWARE_TABLE + ";")
        res = cursor.fetchone()
        self.assertIsNotNone(res)
        self.assertEqual(res["count"], expected_count)
