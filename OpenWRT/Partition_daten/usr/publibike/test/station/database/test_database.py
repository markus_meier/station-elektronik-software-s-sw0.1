import unittest

from station.database.database import Database
from station.station_configuration import StationConfiguration


class TestDatabase(unittest.TestCase):
    def setUp(self):
        config = StationConfiguration(file_name="./test/station/station_configuration.cfg")
        self.database = Database(config)

    def test_shouldConnectAndDisconnect(self):
        cursor = self.database.connect()
        self.assertIsNotNone(cursor)
        self.database.disconnect()

    def test_shouldCreateAndFillTable(self):
        cursor = self.database.connect()
        cursor.execute("CREATE TABLE IF NOT EXISTS test (id serial PRIMARY KEY, num integer, data varchar);")
        cursor.execute("INSERT INTO test (num, data) VALUES (?, ?)", (100, "abc'def"))
        cursor.execute("SELECT * FROM test;")
        self.assertIsNotNone(cursor.fetchone())
        self.database.disconnect()

