import unittest

from station.station_configuration import StationConfiguration


class TestStationConfiguration(unittest.TestCase):
    def setUp(self):
        self.config = StationConfiguration(file_name="./test/station/station_configuration.cfg")

    def test_shouldGetBackendUrl(self):
        url = self.config.backend_url()
        self.assertTrue(len(url) > 0)

    def test_shouldGetBackendUsername(self):
        self.assertTrue(len(self.config.backend_username()) > 0)
        self.assertEqual(self.config.backend_username(), "publibike-admin")

    def test_shouldGetBackendPassword(self):
        self.assertTrue(len(self.config.backend_password()) > 0)

    def test_shouldGetDatabaseFilename(self):
        self.assertTrue(len(self.config.database_filename()) > 0)

    def test_shouldGetStationId(self):
        station_id = self.config.station_id()
        self.assertIsNotNone(station_id)
        self.assertEqual(station_id, "1")
