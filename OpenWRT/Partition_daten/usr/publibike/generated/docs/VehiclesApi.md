# swagger_client.VehiclesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**internal_stations_id_vehicles_get**](VehiclesApi.md#internal_stations_id_vehicles_get) | **GET** /internal/stations/{id}/vehicles | 


# **internal_stations_id_vehicles_get**
> VehicleContainer internal_stations_id_vehicles_get(id, updatetoken)



Returns the vehicles update since the last given update-token for this station. Will return all vehicles in case the update-token is empty or invalid. 

### Example 
```python
from __future__ import print_statement
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.VehiclesApi()
id = 56 # int | The station id
updatetoken = 'updatetoken_example' # str | The update token identifies the last update from the station.

try: 
    api_response = api_instance.internal_stations_id_vehicles_get(id, updatetoken)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VehiclesApi->internal_stations_id_vehicles_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The station id | 
 **updatetoken** | **str**| The update token identifies the last update from the station. | 

### Return type

[**VehicleContainer**](VehicleContainer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

