# Rental

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicle_id** | **int** | Technical id of a vehicle | 
**access_media_id** | **int** | Technical id of access media from start rental. | 
**start** | **str** | Date and time of rental start. Value assigned only on rental start. (ISO 8601 format) | [optional] 
**stop** | **str** | Date and time of rental stop. Value assigned only on rental stop. (ISO 8601 format) | [optional] 
**duration** | **int** | Rental duration in seconds (from SmartLock). Value assigned only on rental stop. | [optional] 
**number_of_intermediate_stops** | **int** | Number of intermediate stops (from SmartLock). Value assigned only on rental stop. | [optional] 
**number_of_emergency_stops** | **int** | Number of emergency stops (from SmartLock). Value assigned only on rental stop. | [optional] 
**rental_aborted** | **bool** | Rental was aborted. Value assigned only on rental stop. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


