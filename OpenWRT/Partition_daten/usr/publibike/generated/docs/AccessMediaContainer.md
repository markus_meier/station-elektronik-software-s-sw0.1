# AccessMediaContainer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**update_token** | **str** | token of current data-set | 
**data** | [**list[AccessMediaContainerData]**](AccessMediaContainerData.md) | binary blob (format to be defined) with the access media updates | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


