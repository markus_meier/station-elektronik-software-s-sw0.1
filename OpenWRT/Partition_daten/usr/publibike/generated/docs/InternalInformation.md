# InternalInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version** | **str** | Current version of the application | [optional] 
**start_time** | **str** | Start time of the application in ISO-8601 format | [optional] 
**num_active_sessions** | **int** | Number of active sessions in cache | [optional] 
**basic_auth_enabled** | **bool** | True if HTTP Basic Auth is enabled, false otherwise | [optional] 
**blacklisting_enabled** | **bool** | True if blacklisting enabled, false otherwise | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


