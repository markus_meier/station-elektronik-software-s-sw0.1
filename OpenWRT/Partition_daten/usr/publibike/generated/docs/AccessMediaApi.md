# swagger_client.AccessMediaApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**internal_access_medias_get**](AccessMediaApi.md#internal_access_medias_get) | **GET** /internal/access-medias | 


# **internal_access_medias_get**
> AccessMediaContainer internal_access_medias_get(updatetoken)



Returns the access media update since the last given update-token. Will return all access medias in case the update-token is empty or invalid. 

### Example 
```python
from __future__ import print_statement
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.AccessMediaApi()
updatetoken = 'updatetoken_example' # str | The update token identifies the last update from the station.

try: 
    api_response = api_instance.internal_access_medias_get(updatetoken)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AccessMediaApi->internal_access_medias_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updatetoken** | **str**| The update token identifies the last update from the station. | 

### Return type

[**AccessMediaContainer**](AccessMediaContainer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

