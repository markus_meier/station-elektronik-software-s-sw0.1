# swagger_client.StatusApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**internal_stations_id_status_put**](StatusApi.md#internal_stations_id_status_put) | **PUT** /internal/stations/{id}/status | 


# **internal_stations_id_status_put**
> StationStatusResponse internal_stations_id_status_put(id, body)



Post status update and get back vehicle updates 

### Example 
```python
from __future__ import print_statement
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.StatusApi()
id = 56 # int | The station id
body = swagger_client.StationStatus() # StationStatus | Status update from station

try: 
    api_response = api_instance.internal_stations_id_status_put(id, body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling StatusApi->internal_stations_id_status_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The station id | 
 **body** | [**StationStatus**](StationStatus.md)| Status update from station | 

### Return type

[**StationStatusResponse**](StationStatusResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

