# Firmware

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type_id** | **int** | Technical id of firmware type | 
**type_name** | **str** | Firmware type name (e.g. station-cpu, station-bluetooth, ...) | 
**hardware_version** | **str** | Hardware version that this configuration is compatible with | 
**firmware_version** | **str** | Available firmware version for this hardware version | 
**firmware_url** | **str** | URL to get the firmware | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


