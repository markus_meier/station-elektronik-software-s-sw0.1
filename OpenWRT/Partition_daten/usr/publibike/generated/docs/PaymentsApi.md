# swagger_client.PaymentsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**internal_payments_confirmation_post**](PaymentsApi.md#internal_payments_confirmation_post) | **POST** /internal/payments/confirmation | 


# **internal_payments_confirmation_post**
> internal_payments_confirmation_post(body)



Service to be used by SwissPost after a payment has been processed during registration 

### Example 
```python
from __future__ import print_statement
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PaymentsApi()
body = 'B' # str | Base64 encoded XML document. Base64 encoded byte array can additionally be URL encoded if the request is coming from user's browser.

try: 
    api_instance.internal_payments_confirmation_post(body)
except ApiException as e:
    print("Exception when calling PaymentsApi->internal_payments_confirmation_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **str**| Base64 encoded XML document. Base64 encoded byte array can additionally be URL encoded if the request is coming from user&#39;s browser. | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

