# SystemConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**station_configuration** | [**Configuration**](Configuration.md) |  | 
**smartlock_configuration** | [**Configuration**](Configuration.md) |  | 
**firmware** | [**list[Firmware]**](Firmware.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


