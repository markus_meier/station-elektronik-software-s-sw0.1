# VehicleContainerVehicles

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | technical id of the vehicle | 
**state_id** | **int** | state id of this vehicle (1 active, 2 broken, 3 blocked, 4 deleted) | 
**type_id** | **int** | vehicle type (1 bike, 2 E-Bike) | 
**smartlock_serial_number** | **str** | SmartLock serial number | 
**operations_number** | **str** | Operations number (Betriebsnummer) of the vehicle | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


