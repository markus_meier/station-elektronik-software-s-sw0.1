# swagger_client.ConfigurationApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**internal_stations_id_configurations_get**](ConfigurationApi.md#internal_stations_id_configurations_get) | **GET** /internal/stations/{id}/configurations | 


# **internal_stations_id_configurations_get**
> SystemConfiguration internal_stations_id_configurations_get(id)



Get configurations for stations, smartlocks etc. 

### Example 
```python
from __future__ import print_statement
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ConfigurationApi()
id = 56 # int | The station id

try: 
    api_response = api_instance.internal_stations_id_configurations_get(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ConfigurationApi->internal_stations_id_configurations_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The station id | 

### Return type

[**SystemConfiguration**](SystemConfiguration.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

