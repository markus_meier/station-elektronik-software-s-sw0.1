# VehicleContainer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**update_token** | **str** | token of current data-set | 
**vehicles** | [**list[VehicleContainerVehicles]**](VehicleContainerVehicles.md) | array with vehicle updates | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


