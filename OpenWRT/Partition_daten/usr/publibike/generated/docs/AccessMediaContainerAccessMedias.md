# AccessMediaContainerAccessMedias

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | technical id of the access media | 
**access_media** | **str** | access media identification | 
**type** | **int** | access media type (1 Phone, 2 SwissPass, 3 RFID) | 
**state** | **int** | access media state (1 active, 2 inactive) | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


