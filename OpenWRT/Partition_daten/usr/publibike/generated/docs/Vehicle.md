# Vehicle

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Technical vehicle id | 
**state_id** | **int** | Technical vehicle state id | [optional] 
**smartlock_battery_level** | **float** | Percentage of fill state of SmartLock battery. | [optional] 
**ebike_battery_level** | **float** | Percentage of fill state of E-Bike battery. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


