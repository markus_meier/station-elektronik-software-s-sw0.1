# AccessMediaContainerData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**person_id** | **int** | technical id of the person | 
**state_id** | **int** | state id of this person (1 okay, 2 blocked, 3 payment method blocked, 4 deactivated) | 
**access_medias** | [**list[AccessMediaContainerAccessMedias]**](AccessMediaContainerAccessMedias.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


