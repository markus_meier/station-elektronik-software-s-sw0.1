# StationStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**battery_level** | **float** | Percentage of fill state of station battery | 
**vehicles** | [**list[Vehicle]**](Vehicle.md) | Vehicle state updates | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


