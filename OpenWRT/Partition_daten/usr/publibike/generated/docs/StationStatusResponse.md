# StationStatusResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**blocked_vehicles** | [**list[BlockedVehicle]**](BlockedVehicle.md) |  | [optional] 
**station_status** | **int** | Station status (1 Active, 2 Inactive, 3 Active without vehicles) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


