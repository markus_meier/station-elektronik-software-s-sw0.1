# swagger_client.InformationApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**internal_information_get**](InformationApi.md#internal_information_get) | **GET** /internal/information | 


# **internal_information_get**
> InternalInformation internal_information_get()



Get internal information about the application 

### Example 
```python
from __future__ import print_statement
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.InformationApi()

try: 
    api_response = api_instance.internal_information_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling InformationApi->internal_information_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InternalInformation**](InternalInformation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

