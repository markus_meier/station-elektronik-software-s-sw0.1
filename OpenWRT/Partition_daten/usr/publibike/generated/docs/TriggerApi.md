# swagger_client.TriggerApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**internal_triggers_import_post**](TriggerApi.md#internal_triggers_import_post) | **POST** /internal/triggers/import | 


# **internal_triggers_import_post**
> internal_triggers_import_post()



Triggers import from skyvia. This ignores the import limits like max number of vehicles added or deleted. This can only be called from localhost. 

### Example 
```python
from __future__ import print_statement
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.TriggerApi()

try: 
    api_instance.internal_triggers_import_post()
except ApiException as e:
    print("Exception when calling TriggerApi->internal_triggers_import_post: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

