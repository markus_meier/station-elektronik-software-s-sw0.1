# swagger_client.RentalsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**internal_stations_id_rentals_put**](RentalsApi.md#internal_stations_id_rentals_put) | **PUT** /internal/stations/{id}/rentals | 


# **internal_stations_id_rentals_put**
> internal_stations_id_rentals_put(id, body)



Update with successful start or stop rental items 

### Example 
```python
from __future__ import print_statement
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RentalsApi()
id = 56 # int | The station id
body = [swagger_client.Rental()] # list[Rental] | A list of rentals

try: 
    api_instance.internal_stations_id_rentals_put(id, body)
except ApiException as e:
    print("Exception when calling RentalsApi->internal_stations_id_rentals_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The station id | 
 **body** | [**list[Rental]**](Rental.md)| A list of rentals | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

