# coding: utf-8

"""
    PubliBike Internal REST API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)

    OpenAPI spec version: 1.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import os
import sys
import unittest

import swagger_client
from swagger_client.rest import ApiException
from swagger_client.apis.status_api import StatusApi


class TestStatusApi(unittest.TestCase):
    """ StatusApi unit test stubs """

    def setUp(self):
        self.api = swagger_client.apis.status_api.StatusApi()

    def tearDown(self):
        pass

    def test_internal_stations_id_status_put(self):
        """
        Test case for internal_stations_id_status_put

        
        """
        pass


if __name__ == '__main__':
    unittest.main()
