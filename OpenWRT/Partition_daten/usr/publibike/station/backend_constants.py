class BackendConstants(object):
    ACCESS_MEDIA_STATE_ACTIVE = 1
    ACCESS_MEDIA_STATE_INACTIVE = 2

    ACCESS_MEDIA_TYPE_PHONE = 1
    ACCESS_MEDIA_TYPE_SWISSPASS = 2
    ACCESS_MEDIA_TYPE_RFID = 3

    PERSON_STATE_PENDING = 1
    PERSON_STATE_ACTIVE = 2
    PERSON_STATE_BLOCKED = 3
    PERSON_STATE_PAYMENT_DECLINED = 4
    PERSON_STATE_DELETED = 5

    VEHICLE_STATE_ACTIVE = 1
    VEHICLE_STATE_BROKEN = 2
    VEHICLE_STATE_BLOCKED = 3
    VEHICLE_STATE_DELETED = 4

    VEHICLE_TYPE_BIKE = 1
    VEHICLE_TYPE_EBIKE = 2
