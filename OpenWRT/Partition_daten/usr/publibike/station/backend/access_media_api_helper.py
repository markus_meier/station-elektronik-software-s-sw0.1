import logging

from base_backend_api_helper import BaseAccessApiHelper
from generated.swagger_client import AccessMediaContainer
from generated.swagger_client.apis.access_media_api import AccessMediaApi
from generated.swagger_client.rest import ApiException


class AccessMediaApiHelper(BaseAccessApiHelper):
    def fetch(self, updatetoken):
        # type: (str) -> AccessMediaContainer
        try:
            api = AccessMediaApi(self._api_client)
            data = api.internal_access_medias_get(updatetoken)
            return data
        except ApiException as e:
            logging.warn("Accessing backend failed: {}".format(e))
            raise
