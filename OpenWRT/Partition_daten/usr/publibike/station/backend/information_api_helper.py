import logging

from base_backend_api_helper import BaseAccessApiHelper
from generated.swagger_client import InternalInformation
from generated.swagger_client.apis.information_api import InformationApi
from generated.swagger_client.rest import ApiException


class InformationApiHelper(BaseAccessApiHelper):
    def fetch(self):
        # type: () -> InternalInformation
        try:
            api = InformationApi(self._api_client)
            data = api.internal_information_get()
            return data
        except ApiException as e:
            logging.warn("Accessing backend failed: {}".format(e))
            raise
