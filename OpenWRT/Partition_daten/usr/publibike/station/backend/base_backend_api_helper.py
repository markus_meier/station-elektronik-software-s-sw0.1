from generated.swagger_client.api_client import ApiClient
from generated.swagger_client.configuration import Configuration
from station import StationConfiguration


class BaseAccessApiHelper(object):
    def __init__(self, configuration):
        # type: (StationConfiguration) -> None
        self._configuration = configuration

        Configuration().host = configuration.backend_url()
        Configuration().username = configuration.backend_username()
        Configuration().password = configuration.backend_password()

        self._api_client = ApiClient()
        self._api_client.set_default_header("Authorization", Configuration().get_basic_auth_token())
