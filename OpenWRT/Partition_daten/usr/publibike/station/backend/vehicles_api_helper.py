import logging

from base_backend_api_helper import BaseAccessApiHelper
from generated.swagger_client.apis.vehicles_api import VehiclesApi
from generated.swagger_client.rest import ApiException


class VehiclesApiHelper(BaseAccessApiHelper):
    def fetch(self, station_id, updatetoken):
        try:
            api = VehiclesApi(self._api_client)
            data = api.internal_stations_id_vehicles_get(station_id, updatetoken)
            return data
        except ApiException as e:
            logging.warn("Accessing backend failed: {}".format(e))
            raise
