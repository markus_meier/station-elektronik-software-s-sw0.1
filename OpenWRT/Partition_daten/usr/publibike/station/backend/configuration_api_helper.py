import logging

from base_backend_api_helper import BaseAccessApiHelper
from generated.swagger_client import SystemConfiguration
from generated.swagger_client.apis.configuration_api import ConfigurationApi
from generated.swagger_client.rest import ApiException


class ConfigurationApiHelper(BaseAccessApiHelper):
    def fetch(self, station_id):
        # type: (int) -> SystemConfiguration
        try:
            api = ConfigurationApi(self._api_client)
            data = api.internal_stations_id_configurations_get(station_id)
            return data
        except ApiException as e:
            logging.warn("Accessing backend failed: {}".format(e))
            raise
