import datetime
import logging
from types import ListType

from base_backend_api_helper import BaseAccessApiHelper
from generated.swagger_client import RentalsApi, Rental
from generated.swagger_client.rest import ApiException


class RentalsApiHelper(BaseAccessApiHelper):
    def put(self, station_id, body):
        # type: (int, ListType[Rental]) -> None
        try:
            api = RentalsApi(self._api_client)
            api.internal_stations_id_rentals_put(station_id, body)
        except ApiException as e:
            logging.warn("Accessing backend failed: {}".format(e))
            raise

    def put_start_rental(self, station_id, vehicle_id, access_media_id, timestamp):
        # type: (int, int, int, datetime.datetime) -> None
        start_rental = self.create_start_rental(vehicle_id, access_media_id, timestamp)
        self.put(station_id, [start_rental])

    @staticmethod
    def create_start_rental(vehicle_id, access_media_id, timestamp):
        # type: (int, int, datetime.datetime) -> Rental
        return Rental(vehicle_id, access_media_id, timestamp.isoformat())

    def put_stop_rental(self, station_id, vehicle_id, duration, timestamp, number_of_intermediate_stops,
                        number_of_emergency_stops, aborted, access_media_id):
        # type: (int, int, int, datetime.datetime, int, int, bool, int) -> None
        stop_rental = self.create_stop_rental(vehicle_id, duration, timestamp, number_of_intermediate_stops,
                                              number_of_emergency_stops, aborted, access_media_id)
        self.put(station_id, [stop_rental])

    @staticmethod
    def create_stop_rental(vehicle_id, duration, timestamp, number_of_intermediate_stops, number_of_emergency_stops,
                           aborted, access_media_id):
        # type: (int, int, datetime.datetime, int, int, bool, int) -> Rental
        return Rental(vehicle_id, access_media_id, None, timestamp.isoformat(), duration, number_of_intermediate_stops,
                      number_of_emergency_stops, aborted)
