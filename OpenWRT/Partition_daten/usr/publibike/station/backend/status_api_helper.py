import logging

from base_backend_api_helper import BaseAccessApiHelper
from generated.swagger_client.apis.status_api import StatusApi
from generated.swagger_client.rest import ApiException


class StatusApiHelper(BaseAccessApiHelper):
    def put(self, station_id, body):
        try:
            api = StatusApi(self._api_client)
            data = api.internal_stations_id_status_put(station_id, body)
            return data
        except ApiException as e:
            logging.warn("Accessing backend failed: {}".format(e))
            raise
