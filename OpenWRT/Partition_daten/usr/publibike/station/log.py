import logging

from station import StationConfiguration


def setup_log(config):
    # type: (StationConfiguration, int) -> None
    log_formatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s] %(module)s  %(message)s")
    root_logger = logging.getLogger()
    root_logger.setLevel(config.log_level())

    file_handler = logging.FileHandler(config.log_filename())
    file_handler.setFormatter(log_formatter)
    root_logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    root_logger.addHandler(console_handler)
