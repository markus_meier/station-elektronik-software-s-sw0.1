try:
    import configparser
except ImportError:
    import ConfigParser as configparser


class StationConfiguration(object):
    def __init__(self, file_name="./station_configuration.cfg"):
        self.__parse_options(file_name)

    def __parse_options(self, file_name):
        self.config = configparser.RawConfigParser()
        self.config.read(file_name)

    def backend_url(self):
        return self.config.get("backend", "url")

    def backend_username(self):
        return self.config.get("backend", "username")

    def backend_password(self):
        return self.config.get("backend", "password")

    def database_filename(self):
        return self.config.get("database", "filename")

    def station_id(self):
        return self.config.get("station", "id")

    def log_filename(self):
        return self.config.get("log", "filename")

    def log_level(self):
        return self.config.getint("log", "level")
