from types import ListType

from station.database.database import Database


class StatusTable(object):
    VEHICLE_STATUS_TABLE = "vehicle_status"
    VEHICLE_STATUS_ID = "vehicle_id"
    VEHICLE_STATUS_SMARTLOCK_LEVEL = "smartlock_battery_level"
    VEHICLE_STATUS_EBIKE_LEVEL = "ebike_battery_level"

    def __init__(self, database):
        # type: (Database) -> None
        self._database = database

    def create_table(self, commit=False):
        cursor = self._database.cursor()
        cursor.execute("CREATE TABLE IF NOT EXISTS " + self.VEHICLE_STATUS_TABLE + " (" + self.VEHICLE_STATUS_ID +
                       " INTEGER, " + self.VEHICLE_STATUS_SMARTLOCK_LEVEL + " FLOAT, " +
                       self.VEHICLE_STATUS_EBIKE_LEVEL + " FLOAT);")
        if commit:
            self.commit()

    def drop_table(self, commit=False):
        cursor = self._database.cursor()
        cursor.execute("DROP TABLE IF EXISTS " + self.VEHICLE_STATUS_TABLE + ";")
        if commit:
            self.commit()

    def add_status_update(self, vehicle_id, smartlock_battery_level, ebike_battery_level, commit=False):
        # type: (int, float, float, bool) -> None
        cursor = self._database.cursor()
        cursor.execute("INSERT INTO " + self.VEHICLE_STATUS_TABLE + " (" + self.VEHICLE_STATUS_ID + ", " +
                       self.VEHICLE_STATUS_SMARTLOCK_LEVEL + ", " + self.VEHICLE_STATUS_EBIKE_LEVEL +
                       ") VALUES (?, ?, ?)",
                       (vehicle_id, smartlock_battery_level, ebike_battery_level))
        if commit:
            self.commit()

    def get_status_updates(self):
        # type: () -> ListType[tuple]
        cursor = self._database.cursor()
        cursor.execute("SELECT " + self.VEHICLE_STATUS_ID + ", " + self.VEHICLE_STATUS_SMARTLOCK_LEVEL + ", " +
                       self.VEHICLE_STATUS_EBIKE_LEVEL + " FROM " + self.VEHICLE_STATUS_TABLE + ";")
        return cursor.fetchall()

    def delete_entries(self, commit=False):
        cursor = self._database.cursor()
        cursor.execute("DELETE FROM " + self.VEHICLE_STATUS_TABLE + ";")
        if commit:
            self.commit()

    def commit(self):
        self._database.connection().commit()
