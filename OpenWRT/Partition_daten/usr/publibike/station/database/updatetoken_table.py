import logging

from station.database.database import Database


class UpdatetokenTable(object):
    VEHICLE_ENTRY_NAME = "vehicle"
    ACCESS_MEDIA_ENTRY_NAME = "access-media"

    UPDATETOKEN_TABLE = "updatetoken"
    UPDATETOKEN_NAME = "name"
    UPDATETOKEN_VALUE = "value"

    def __init__(self, database):
        # type: (Database) -> None
        self._database = database

    def drop_table(self, commit=False):
        cursor = self._database.cursor()
        cursor.execute("DROP TABLE IF EXISTS " + self.UPDATETOKEN_TABLE + ";")
        if commit:
            self.commit()

    def create_table(self, commit=False):
        cursor = self._database.cursor()
        cursor.execute("CREATE TABLE IF NOT EXISTS " + self.UPDATETOKEN_TABLE + "(" + self.UPDATETOKEN_NAME +
                       " VARCHAR, " + self.UPDATETOKEN_VALUE + " VARCHAR);")
        cursor.execute("INSERT INTO " + self.UPDATETOKEN_TABLE + "(" + self.UPDATETOKEN_NAME + ", " +
                       self.UPDATETOKEN_VALUE + ") SELECT ?, ? WHERE NOT EXISTS " +
                       "(SELECT 1 FROM " + self.UPDATETOKEN_TABLE + " WHERE " + self.UPDATETOKEN_NAME + " = ?);",
                       (self.VEHICLE_ENTRY_NAME, "", self.VEHICLE_ENTRY_NAME))
        cursor.execute("INSERT INTO " + self.UPDATETOKEN_TABLE + "(" + self.UPDATETOKEN_NAME + ", " +
                       self.UPDATETOKEN_VALUE + ") SELECT ?, ? WHERE NOT EXISTS " +
                       "(SELECT 1 FROM " + self.UPDATETOKEN_TABLE + " WHERE " + self.UPDATETOKEN_NAME + " = ?);",
                       (self.ACCESS_MEDIA_ENTRY_NAME, "", self.ACCESS_MEDIA_ENTRY_NAME))
        if commit:
            self.commit()

    def get_vehicle_updatetoken(self):
        # type: () -> str
        return self._get_updatetoken(self.VEHICLE_ENTRY_NAME)

    def set_vehicle_updatetoken(self, updatetoken, commit=False):
        # type: (str, bool) -> None
        self._set_updatetoken(self.VEHICLE_ENTRY_NAME, updatetoken, commit=commit)

    def get_access_media_updatetoken(self):
        # type: () -> str
        return self._get_updatetoken(self.ACCESS_MEDIA_ENTRY_NAME)

    def set_access_media_updatetoken(self, updatetoken, commit=False):
        # type: (str, bool) -> None
        self._set_updatetoken(self.ACCESS_MEDIA_ENTRY_NAME, updatetoken, commit=commit)

    def commit(self):
        self._database.connection().commit()

    def rollback(self):
        self._database.connection().rollback()

    def _get_updatetoken(self, name):
        # type: (str) -> str
        cursor = self._database.cursor()
        cursor.execute("SELECT " + self.UPDATETOKEN_VALUE + " FROM " + self.UPDATETOKEN_TABLE + " WHERE " +
                       self.UPDATETOKEN_NAME + " = ?;", (name,))
        result = cursor.fetchone()
        logging.debug("Returning updatetoken {} from table {}".format(result[self.UPDATETOKEN_VALUE], name))
        return result[self.UPDATETOKEN_VALUE]

    def _set_updatetoken(self, name, updatetoken, commit=False):
        # type: (str, str, bool) -> None
        logging.debug("Setting updatetoken {} in table {}".format(updatetoken, name))
        cursor = self._database.cursor()
        cursor.execute("UPDATE " + self.UPDATETOKEN_TABLE + " SET " + self.UPDATETOKEN_VALUE + " = ? WHERE " +
                       self.UPDATETOKEN_NAME + " = ?;", (updatetoken, name))
        if commit:
            self.commit()
