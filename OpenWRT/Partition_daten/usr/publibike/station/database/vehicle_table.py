from database import Database
from generated.swagger_client.models.vehicle_container_vehicles import VehicleContainerVehicles


class VehicleTable(object):
    VEHICLE_TABLE = "vehicle"
    VEHICLE_ID = "id"
    VEHICLE_STATE_ID = "state_id"
    VEHICLE_TYPE_ID = "type_id"
    VEHICLE_SMARTLOCK_SERIAL_NUMBER = "smartlock_serial_number"
    VEHICLE_OPERATIONS_NUMBER = "operations_number"

    def __init__(self, database):
        # type: (Database) -> None
        self._database = database

    def drop_table(self, commit=False):
        cursor = self._database.cursor()
        cursor.execute("DROP TABLE IF EXISTS " + self.VEHICLE_TABLE + ";")
        if commit:
            self.commit()

    def create_table(self, commit=False):
        cursor = self._database.cursor()
        cursor.execute(
            "CREATE TABLE IF NOT EXISTS " + self.VEHICLE_TABLE + " (" + self.VEHICLE_ID + " SERIAL PRIMARY KEY, " +
            self.VEHICLE_STATE_ID + " INTEGER, " + self.VEHICLE_TYPE_ID + " INTEGER, " +
            self.VEHICLE_SMARTLOCK_SERIAL_NUMBER + " VARCHAR, " + self.VEHICLE_OPERATIONS_NUMBER + " VARCHAR);")
        if commit:
            self.commit()

    def add_vehicle(self, vehicle, commit=False):
        # type: (VehicleContainerVehicles, bool) -> None
        cursor = self._database.cursor()
        cursor.execute(
            "INSERT INTO " + self.VEHICLE_TABLE + "(" + self.VEHICLE_ID + ", " + self.VEHICLE_STATE_ID + ", " +
            self.VEHICLE_TYPE_ID + ", " + self.VEHICLE_SMARTLOCK_SERIAL_NUMBER + ", " + self.VEHICLE_OPERATIONS_NUMBER +
            ") VALUES (?, ?, ?, ?, ?);",
            (vehicle.id, vehicle.state_id, vehicle.type_id, vehicle.smartlock_serial_number, vehicle.operations_number))
        if commit:
            self._database.connection().commit()

    def remove_vehicle(self, vehicle_id, commit=False):
        # type: (int, bool) -> None
        cursor = self._database.cursor()
        cursor.execute("DELETE FROM " + self.VEHICLE_TABLE + " WHERE " + self.VEHICLE_ID + " = ?;", (vehicle_id,))
        if commit:
            self._database.connection().commit()

    def get_vehicle(self, vehicle_id):
        # type: (int) -> tuple
        cursor = self._database.cursor()
        cursor.execute(
            "SELECT " + self.VEHICLE_ID + ", " + self.VEHICLE_STATE_ID + ", " + self.VEHICLE_TYPE_ID + ", " +
            self.VEHICLE_SMARTLOCK_SERIAL_NUMBER + ", " + self.VEHICLE_OPERATIONS_NUMBER + " FROM " +
            self.VEHICLE_TABLE + " WHERE " + self.VEHICLE_ID + " = ?;",
            (vehicle_id,))
        return cursor.fetchone()

    def commit(self):
        self._database.connection().commit()

    def rollback(self):
        self._database.connection().rollback()

    def update_vehicle(self, vehicle, commit=False):
        # type: (VehicleContainerVehicles, bool) -> None
        cursor = self._database.cursor()
        cursor.execute("UPDATE " + self.VEHICLE_TABLE + " SET " + self.VEHICLE_STATE_ID + " = ?, " +
                       self.VEHICLE_TYPE_ID + " = ?, " + self.VEHICLE_SMARTLOCK_SERIAL_NUMBER + " = ?, " +
                       self.VEHICLE_OPERATIONS_NUMBER + " = ? WHERE " + self.VEHICLE_ID + " = ?;",
                       (vehicle.state_id, vehicle.type_id, vehicle.smartlock_serial_number, vehicle.operations_number,
                        vehicle.id))
        if commit:
            self._database.connection().commit()

    def fetch(self):
        # type: (str) -> list[tuple]
        cursor = self._database.cursor()
        cursor.execute("SELECT " + self.VEHICLE_ID + ", " + self.VEHICLE_STATE_ID + ", " + self.VEHICLE_TYPE_ID + ", " +
                       self.VEHICLE_SMARTLOCK_SERIAL_NUMBER + ", " + self.VEHICLE_OPERATIONS_NUMBER + " FROM " +
                       self.VEHICLE_TABLE + ";")
        return cursor.fetchall()

    def get_vehicle_from_smartlock_serial_number(self, smartlock_serial_number):
        # type: (str) -> tuple
        cursor = self._database.cursor()
        cursor.execute("SELECT " + self.VEHICLE_ID + ", " + self.VEHICLE_STATE_ID + ", " + self.VEHICLE_TYPE_ID + ", " +
                       self.VEHICLE_SMARTLOCK_SERIAL_NUMBER + ", " + self.VEHICLE_OPERATIONS_NUMBER + " FROM " +
                       self.VEHICLE_TABLE + " WHERE " + self.VEHICLE_SMARTLOCK_SERIAL_NUMBER + " = ?;",
                       (smartlock_serial_number,))
        return cursor.fetchone()
