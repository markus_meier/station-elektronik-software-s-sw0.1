from station.database.database import Database


class FirmwareTable(object):
    FIRMWARE_TABLE = "firmware"
    FIRMWARE_TYPE_ID = "type_id"
    FIRMWARE_TYPE_NAME = "type_name"
    FIRMWARE_HARDWARE_VERSION = "hardware_version"
    FIRMWARE_FIRMWARE_VERSION = "firmware_version"
    FIRMWARE_FIRMWARE_URL = "firmware_url"

    def __init__(self, database):
        # type: (Database) -> None
        self._database = database

    def drop_table(self, commit=False):
        cursor = self._database.cursor()
        cursor.execute("DROP TABLE IF EXISTS " + self.FIRMWARE_TABLE + ";")
        if commit:
            self.commit()

    def create_table(self, commit=False):
        cursor = self._database.cursor()
        cursor.execute("CREATE TABLE IF NOT EXISTS " + self.FIRMWARE_TABLE + " (" + self.FIRMWARE_TYPE_ID +
                       " INTEGER, " + self.FIRMWARE_TYPE_NAME + " VARCHAR, " + self.FIRMWARE_HARDWARE_VERSION +
                       " VARCHAR, " + self.FIRMWARE_FIRMWARE_VERSION + " VARCHAR, " + self.FIRMWARE_FIRMWARE_URL +
                       " VARCHAR);")
        if commit:
            self.commit()

    def update_firmware(self, firmwares=list, commit=False):
        # type: (list, bool) -> None
        cursor = self._database.cursor()
        cursor.execute("DELETE FROM " + self.FIRMWARE_TABLE + ";")
        for firmware in firmwares:
            cursor.execute("INSERT INTO " + self.FIRMWARE_TABLE + "(" + self.FIRMWARE_TYPE_ID + ", " +
                           self.FIRMWARE_TYPE_NAME + ", " + self.FIRMWARE_HARDWARE_VERSION + ", " +
                           self.FIRMWARE_FIRMWARE_VERSION + ", " + self.FIRMWARE_FIRMWARE_URL +
                           ") VALUES (?, ?, ?, ?, ?);", (
                               firmware.type_id, firmware.type_name, firmware.hardware_version,
                               firmware.firmware_version,
                               firmware.firmware_url))
        if commit:
            self.commit()

    def commit(self):
        self._database.connection().commit()

    def rollback(self):
        self._database.connection().rollback()

    def fetch(self):
        cursor = self._database.cursor()
        cursor.execute("SELECT " + self.FIRMWARE_TYPE_ID + ", " + self.FIRMWARE_TYPE_NAME + ", " +
                       self.FIRMWARE_HARDWARE_VERSION + ", " + self.FIRMWARE_FIRMWARE_VERSION + ", " +
                       self.FIRMWARE_FIRMWARE_URL + " FROM " + self.FIRMWARE_TABLE + ";")
        return cursor.fetchall()
