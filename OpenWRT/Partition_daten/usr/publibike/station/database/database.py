import logging
import sqlite3

from station import StationConfiguration


class Database(object):
    """ Database helper class """

    def __init__(self, configuration):
        # type: (StationConfiguration) -> None
        self._station_configuration = configuration
        self._connection = None
        self._cursor = None

    def connect(self):
        """ Connects to the DB and returns a sqlite3 cursor
        :returns a sqlite3 cursor
        """
        self._connection = None
        self._cursor = None
        try:
            self._connection = sqlite3.connect(self._station_configuration.database_filename(),
                                               detect_types=sqlite3.PARSE_DECLTYPES)
            self._connection.row_factory = self.dict_factory
            self._cursor = self._connection.cursor()
        except Exception as e:
            logging.warn("Something went wrong while connecting to the DB: {}".format(e))
        return self._cursor

    def disconnect(self):
        """ Disconnects the DB in case it's connected """
        if self._cursor is not None:
            self._cursor.close()
        if self._connection is not None:
            self._connection.close()
        self._cursor = None
        self._connection = None

    def connection(self):
        """ :returns database connection """
        return self._connection

    def cursor(self):
        """ :returns database cursor """
        return self._cursor

    @staticmethod
    def dict_factory(cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d
