from types import ListType

from generated.swagger_client import AccessMediaContainerData, AccessMediaContainerAccessMedias
from station.database.database import Database


class AccessMediaTable(object):
    PERSON_TABLE = "person"
    PERSON_ID = "id"
    PERSON_STATE_ID = "person_state_id"

    ACCESS_MEDIA_TABLE = "access_media"
    ACCESS_MEDIA_ID = "access_media_id"
    ACCESS_MEDIA_PERSON_ID = "person_id"
    ACCESS_MEDIA_TYPE_ID = "type_id"
    ACCESS_MEDIA_STATE_ID = "access_media_state_id"
    ACCESS_MEDIA_ACCESS_MEDIA = "access_media"

    def __init__(self, database):
        # type: (Database) -> None
        self._database = database

    def drop_table(self, commit=False):
        cursor = self._database.cursor()
        cursor.execute("DROP TABLE IF EXISTS " + self.ACCESS_MEDIA_TABLE + ";")
        cursor.execute("DROP TABLE IF EXISTS " + self.PERSON_TABLE + ";")
        if commit:
            self.commit()

    def create_table(self, commit=False):
        cursor = self._database.cursor()
        cursor.execute("CREATE TABLE IF NOT EXISTS " + self.PERSON_TABLE + " (" + self.PERSON_ID +
                       " SERIAL PRIMARY KEY, " + self.PERSON_STATE_ID + " INTEGER);")
        cursor.execute(
            "CREATE TABLE IF NOT EXISTS " + self.ACCESS_MEDIA_TABLE + " (" + self.ACCESS_MEDIA_ID +
            " INTEGER NOT NULL, " + self.ACCESS_MEDIA_PERSON_ID + " INTEGER NOT NULL, " + self.ACCESS_MEDIA_TYPE_ID +
            " INTEGER, " + self.ACCESS_MEDIA_STATE_ID + " INTEGER, " + self.ACCESS_MEDIA_ACCESS_MEDIA + " VARCHAR, " +
            "CONSTRAINT access_media_person_id_fk FOREIGN KEY (" + self.ACCESS_MEDIA_PERSON_ID + ") " +
            "REFERENCES " + self.PERSON_TABLE + " (" + self.PERSON_ID + "));")
        if commit:
            self.commit()

    def add_person_with_access_media(self, access_media_container, commit=False):
        # type: (AccessMediaContainerData, bool) -> None
        cursor = self._database.cursor()
        cursor.execute(
            "INSERT INTO " + self.PERSON_TABLE + " (" + self.PERSON_ID + ", " + self.PERSON_STATE_ID +
            ") VALUES (?, ?);", (access_media_container.person_id, access_media_container.state_id))
        for access_media in access_media_container.access_medias:
            self.add_access_media_for_person(access_media, access_media_container.person_id, commit=commit)
        if commit:
            self.commit()

    def remove_person_with_access_media(self, person_id, commit=False):
        # type: (int, bool) -> None
        """ Removes person and its access medias """
        cursor = self._database.cursor()
        cursor.execute("DELETE FROM " + self.ACCESS_MEDIA_TABLE + " WHERE " + self.ACCESS_MEDIA_PERSON_ID + " = ?;",
                       (person_id,))
        cursor.execute("DELETE FROM " + self.PERSON_TABLE + " WHERE " + self.PERSON_ID + " = ?;", (person_id,))
        if commit:
            self.commit()

    def get_person(self, person_id):
        # type: (int) -> tuple
        cursor = self._database.cursor()
        cursor.execute("SELECT " + self.PERSON_ID + ", " + self.PERSON_STATE_ID + " FROM " + self.PERSON_TABLE +
                       " WHERE " + self.PERSON_ID + " = ?;", (person_id,))
        return cursor.fetchone()

    def commit(self):
        self._database.connection().commit()

    def rollback(self):
        self._database.connection().rollback()

    def fetch_person_table(self):
        cursor = self._database.cursor()
        cursor.execute("SELECT " + self.PERSON_ID + ", " + self.PERSON_STATE_ID + " FROM " + self.PERSON_TABLE + ";")
        return cursor.fetchall()

    def fetch_access_media(self):
        cursor = self._database.cursor()
        cursor.execute("SELECT " + self.ACCESS_MEDIA_ID + ", " + self.ACCESS_MEDIA_PERSON_ID + ", " +
                       self.ACCESS_MEDIA_TYPE_ID + ", " + self.ACCESS_MEDIA_STATE_ID + ", " +
                       self.ACCESS_MEDIA_ACCESS_MEDIA + " FROM " + self.ACCESS_MEDIA_TABLE + ";")
        return cursor.fetchall()

    def get_person_from_access_media(self, access_media_type, access_media):
        # type: (int, str) -> tuple
        cursor = self._database.cursor()
        cursor.execute("SELECT " + self.PERSON_ID + ", " + self.PERSON_STATE_ID + ", " + self.ACCESS_MEDIA_ID +
                       " FROM " + self.PERSON_TABLE + " JOIN " + self.ACCESS_MEDIA_TABLE + " ON " +
                       self.PERSON_ID + " = " + self.ACCESS_MEDIA_PERSON_ID + " WHERE " + self.ACCESS_MEDIA_TYPE_ID +
                       " = ? AND " + self.ACCESS_MEDIA_ACCESS_MEDIA + " = ?;",
                       (access_media_type, access_media))
        res = cursor.fetchone()
        return res

    def get_access_medias_for_person(self, person_id):
        # type: (int) -> ListType[tuple]
        cursor = self._database.cursor()
        cursor.execute("SELECT " + self.ACCESS_MEDIA_ID + ", " + self.ACCESS_MEDIA_TYPE_ID + ", " +
                       self.ACCESS_MEDIA_ACCESS_MEDIA + " FROM " + self.ACCESS_MEDIA_TABLE + " WHERE " +
                       self.ACCESS_MEDIA_PERSON_ID + " = ?;", (person_id,))
        res = cursor.fetchall()
        return res

    def delete_access_media(self, access_media_id, commit=False):
        # type: (int, bool) -> None
        cursor = self._database.cursor()
        cursor.execute("DELETE FROM " + self.ACCESS_MEDIA_TABLE + " WHERE " + self.ACCESS_MEDIA_ID + " = ?;",
                       (access_media_id,))
        if commit:
            self.commit()

    def add_access_media_for_person(self, access_media, person_id, commit=False):
        # type: (AccessMediaContainerAccessMedias, int, bool) -> None
        cursor = self._database.cursor()
        cursor.execute("INSERT INTO " + self.ACCESS_MEDIA_TABLE + " (" + self.ACCESS_MEDIA_ID + ", " +
                       self.ACCESS_MEDIA_PERSON_ID + ", " + self.ACCESS_MEDIA_TYPE_ID + ", " +
                       self.ACCESS_MEDIA_STATE_ID + ", " + self.ACCESS_MEDIA_ACCESS_MEDIA + ") VALUES " +
                       "(?, ?, ?, ?, ?);",
                       (access_media.id, person_id, access_media.type, access_media.state, access_media.access_media))
        if commit:
            self.commit()

    def update_person(self, access_media_container_data, commit=False):
        # type: (AccessMediaContainerData, bool) -> None
        cursor = self._database.cursor()
        cursor.execute("UPDATE " + self.PERSON_TABLE + " SET " + self.PERSON_STATE_ID + " = ? WHERE " + self.PERSON_ID +
                       "= ? ", (access_media_container_data.state_id, access_media_container_data.person_id))
        if commit:
            self.commit()

    def get_access_media(self, access_media_id):
        # type: (int) -> tuple
        cursor = self._database.cursor()
        cursor.execute("SELECT " + self.ACCESS_MEDIA_ID + ", " + self.ACCESS_MEDIA_TYPE_ID + ", " +
                       self.ACCESS_MEDIA_ACCESS_MEDIA + " FROM " + self.ACCESS_MEDIA_TABLE + " WHERE " +
                       self.ACCESS_MEDIA_ID + " = ?;", (access_media_id,))
        res = cursor.fetchone()
        return res

    def update_access_media(self, access_media, commit=False):
        # type: (AccessMediaContainerAccessMedias, bool) -> None
        cursor = self._database.cursor()
        cursor.execute("UPDATE " + self.ACCESS_MEDIA_TABLE + " SET " + self.ACCESS_MEDIA_TYPE_ID + " = ?, " +
                       self.ACCESS_MEDIA_STATE_ID + " = ?, " + self.ACCESS_MEDIA_ACCESS_MEDIA + " = ? WHERE " +
                       self.ACCESS_MEDIA_ID + " = ? ",
                       (access_media.type, access_media.state, access_media.access_media, access_media.id))
        if commit:
            self.commit()
