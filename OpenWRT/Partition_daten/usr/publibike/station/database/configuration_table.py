from types import ListType

from station.database.database import Database


class ConfigurationTable(object):
    STATION_CONFIGURATION_TABLE = "station_configuration"
    SMARTLOCK_CONFIGURATION_TABLE = "smartlock_configuration"
    KEY_COLUMN = "key"
    VALUE_COLUMN = "value"

    def __init__(self, database):
        # type: (Database) -> None
        self._database = database

    def drop_table(self, commit=False):
        # type: (bool) -> None
        cursor = self._database.cursor()
        cursor.execute("DROP TABLE IF EXISTS " + self.STATION_CONFIGURATION_TABLE + ";")
        cursor.execute("DROP TABLE IF EXISTS " + self.SMARTLOCK_CONFIGURATION_TABLE + ";")
        if commit:
            self.commit()

    def create_table(self, commit=False):
        # type: (bool) -> None
        cursor = self._database.cursor()
        cursor.execute("CREATE TABLE IF NOT EXISTS " + self.STATION_CONFIGURATION_TABLE + "(" + self.KEY_COLUMN +
                       " VARCHAR, " + self.VALUE_COLUMN + " VARCHAR);")
        cursor.execute("CREATE TABLE IF NOT EXISTS " + self.SMARTLOCK_CONFIGURATION_TABLE + "(" + self.KEY_COLUMN +
                       " VARCHAR, " + self.VALUE_COLUMN + " VARCHAR);")
        if commit:
            self.commit()

    def commit(self):
        self._database.connection().commit()

    def rollback(self):
        self._database.connection().rollback()

    def update_smartlock_configuration(self, smartlock_configuration, commit=False):
        # type: (ListType[dict], bool) -> None
        self._update_configuration(self.SMARTLOCK_CONFIGURATION_TABLE, smartlock_configuration, commit=commit)

    def update_station_configuration(self, station_configuration, commit=False):
        # type: (ListType[dict], bool) -> None
        self._update_configuration(self.STATION_CONFIGURATION_TABLE, station_configuration, commit=commit)

    def _update_configuration(self, table, configuration, commit=False):
        # type: (str, ListType[dict], bool) -> None
        cursor = self._database.cursor()
        cursor.execute("DELETE FROM " + table + ";")
        for config in configuration:
            cursor.execute("INSERT INTO " + table + " (" + self.KEY_COLUMN + ", " + self.VALUE_COLUMN +
                           ") VALUES (?, ?);", (config["key"], config["value"]))
        if commit:
            self.commit()

    def fetch_station_configuration(self):
        # type: () -> ListType[tuple]
        return self._fetch_configuration(table_name=self.STATION_CONFIGURATION_TABLE)

    def fetch_smartlock_configuration(self):
        # type: () -> ListType[tuple]
        return self._fetch_configuration(table_name=self.SMARTLOCK_CONFIGURATION_TABLE)

    def _fetch_configuration(self, table_name):
        # type: (str) -> ListType[tuple]
        cursor = self._database.cursor()
        cursor.execute("SELECT " + self.KEY_COLUMN + ", " + self.VALUE_COLUMN + " FROM " + table_name + ";")
        return cursor.fetchall()
