import datetime
from types import ListType

from station.database.database import Database


class RentalTable(object):
    # common
    ID = "id"
    VEHICLE_ID = "vehicle_id"
    ACCESS_MEDIA_ID = "access_media_id"

    START_RENTAL_TABLE = "start_rental"
    START_RENTAL_START = "start"

    STOP_RENTAL_TABLE = "stop_rental"
    STOP_RENTAL_STOP = "stop"
    STOP_RENTAL_DURATION = "duration_s"
    STOP_RENTAL_INTERMEDIATE_STOPS = "number_of_intermediate_stops"
    STOP_RENTAL_EMERGENCY_STOPS = "number_of_emergency_stops"
    STOP_RENTAL_ABORTED = "aborted"

    def __init__(self, database):
        # type: (Database) -> None
        self._database = database

    def create_table(self, commit=False):
        cursor = self._database.cursor()
        cursor.execute("CREATE TABLE IF NOT EXISTS " + self.START_RENTAL_TABLE + "(" + self.ID +
                       " INTEGER PRIMARY KEY AUTOINCREMENT, " + self.VEHICLE_ID + " INTEGER, " + self.ACCESS_MEDIA_ID +
                       " INTEGER, " + self.START_RENTAL_START + " TIMESTAMP);")
        cursor.execute("CREATE TABLE IF NOT EXISTS " + self.STOP_RENTAL_TABLE + " (" + self.ID +
                       " INTEGER PRIMARY KEY AUTOINCREMENT, " + self.VEHICLE_ID + " INTEGER, " + self.STOP_RENTAL_STOP +
                       " TIMESTAMP, " + self.STOP_RENTAL_DURATION + " INTEGER, " + self.STOP_RENTAL_INTERMEDIATE_STOPS +
                       " INTEGER, " + self.STOP_RENTAL_EMERGENCY_STOPS + " INTEGER, " + self.STOP_RENTAL_ABORTED +
                       " BOOLEAN, " + self.ACCESS_MEDIA_ID + "  INTEGER);")

        if commit:
            self._database.connection().commit()

    def drop_table(self, commit=False):
        cursor = self._database.cursor()
        cursor.execute("DROP TABLE IF EXISTS " + self.STOP_RENTAL_TABLE + ";")
        cursor.execute("DROP TABLE IF EXISTS " + self.START_RENTAL_TABLE + ";")
        if commit:
            self._database.connection().commit()

    def add_start_rental(self, vehicle_id, access_media_id, start_date, commit=False):
        # type: (int, int, datetime.datetime, bool) -> None
        cursor = self._database.cursor()
        cursor.execute("INSERT INTO " + self.START_RENTAL_TABLE + " (" + self.VEHICLE_ID + ", " + self.ACCESS_MEDIA_ID +
                       ", " + self.START_RENTAL_START + ") VALUES (?, ?, ?);",
                       (vehicle_id, access_media_id, start_date))
        if commit:
            self.commit()

    def add_stop_rental(self, vehicle_id, stop_date, duration_s, number_of_intermediate_stops,
                        number_of_emergency_stops, aborted, access_media_id, commit=False):
        # type: (int, datetime.datetime, int, int, int, bool, int, bool) -> None
        cursor = self._database.cursor()
        cursor.execute("INSERT INTO " + self.STOP_RENTAL_TABLE + " (" + self.VEHICLE_ID + ", " + self.STOP_RENTAL_STOP +
                       ", " + self.STOP_RENTAL_DURATION + ", " + self.STOP_RENTAL_INTERMEDIATE_STOPS + ", " +
                       self.STOP_RENTAL_EMERGENCY_STOPS + ", " + self.STOP_RENTAL_ABORTED + ", " +
                       self.ACCESS_MEDIA_ID + ") VALUES (?, ?, ?, ?, ?, ?, ?);",
                       (vehicle_id, stop_date, duration_s, number_of_intermediate_stops, number_of_emergency_stops,
                        aborted, access_media_id))
        if commit:
            self.commit()

    def get_start_rentals(self):
        cursor = self._database.cursor()
        cursor.execute("SELECT " + self.ID + " , " + self.VEHICLE_ID + ", " + self.ACCESS_MEDIA_ID + ", " +
                       self.START_RENTAL_START + " FROM " + self.START_RENTAL_TABLE + ";")
        return cursor.fetchall()

    def get_stop_rentals(self):
        cursor = self._database.cursor()
        cursor.execute("SELECT " + self.ID + ", " + self.VEHICLE_ID + ", " + self.STOP_RENTAL_STOP + ", " +
                       self.STOP_RENTAL_DURATION + ", " + self.STOP_RENTAL_INTERMEDIATE_STOPS + ", " +
                       self.STOP_RENTAL_EMERGENCY_STOPS + ", " + self.STOP_RENTAL_ABORTED + ", " +
                       self.ACCESS_MEDIA_ID + " FROM " + self.STOP_RENTAL_TABLE + ";")
        return cursor.fetchall()

    def delete_start_rentals(self, start_rentals, commit=False):
        # type: (ListType[tuple], bool) -> None
        start_rental_ids = []
        for start_rental in start_rentals:
            start_rental_ids.append((start_rental[self.ID],))

        cursor = self._database.cursor()
        cursor.executemany("DELETE FROM " + self.START_RENTAL_TABLE + " WHERE " + self.ID + " = ?;", start_rental_ids)
        if commit:
            self.commit()

    def delete_stop_rentals(self, stop_rentals, commit=False):
        # type: (ListType[tuple], bool) -> None
        stop_rental_ids = []
        for stop_rental in stop_rentals:
            stop_rental_ids.append((stop_rental[self.ID],))

        cursor = self._database.cursor()
        cursor.executemany("DELETE FROM " + self.STOP_RENTAL_TABLE + " WHERE " + self.ID + " = ?;", stop_rental_ids)
        if commit:
            self.commit()

    def commit(self):
        self._database.connection().commit()
