import logging
import string
from types import ListType

from station.bluetooth.interchange_protocol_type import InterchangeProtocolType
from station.bluetooth.parse_exception import ParseException


class InterchangeProtocol(object):
    CMD_OUT_PART_CONTAINER = "CONTAINER"
    CMD_OUT_PART_COMMAND = "COMMAND"
    CMD_OUT_PART_TYPE = "TYPE="
    CMD_OUT_PART_LENGTH = "LENGTH="
    CMD_OUT_PART_DATA = "DATA=0x"

    DATA_TYPE = "type"
    DATA_TYPE_START_RENTAL = "start_rental"
    DATA_TYPE_STOP_RENTAL = "stop_rental"
    DATA_TYPE_SMARTLOCK_STATUS = "smartlock_status"
    DATA_TYPE_DISCONNECT = "disconnect"
    DATA_APP_PUBLIC_KEY = "app_public_key"
    DATA_BETRIEBSNUMMER = "betriebsnummer"
    DATA_SMARTLOCK_SERIAL_NUMBER = "smartlock_serial_number"
    DATA_RFID_UID = "rfid_uid"
    DATA_RIDE_DURATION = "ride_duration"
    DATA_INTERMEDIATE_STOPS = "intermediate_stops"
    DATA_EMERGENCY_STOPS = "emergency_stops"
    DATA_SMARTLOCK_BATTERY = "smartlock_battery"
    DATA_EBIKE_BATTERY = "ebike_battery"
    DATA_BIKE_TYPE = "bike_type"
    DATA_SMARTLOCK_SW_VERSION = "smartlock_sw_version"

    def __init__(self):
        pass

    def parse_cli_output(self, line):
        # type: (str) -> ListType[str]
        """ a line from the CLI too may consist of one command or a container.
        A container will be parsed by the tool into the sub commands, separated by ;
        """
        logging.debug("Received the following line: {}".format(line))
        result = []
        for item in string.split(line, ";"):
            item = string.strip(item)
            if item != "":
                res = self._parse_cli_item(item)
                result.append(res)
        return result

    def _parse_cli_item(self, item):
        logging.debug("Received the following item: {}".format(item))
        split = string.split(item)
        if len(split) <= 3:
            msg = "Expected at least 3 parts in item {}".format(item)
            logging.error(msg)
            raise ParseException(msg)

        # Do the parse magic
        cmd_type = None
        length = None
        data = None
        for item in split:
            if string.find(item, self.CMD_OUT_PART_COMMAND) == 0:
                logging.debug("Command detected")
            elif string.find(item, self.CMD_OUT_PART_CONTAINER) == 0:
                logging.debug("Container detected")
            elif string.find(item, self.CMD_OUT_PART_TYPE) == 0:
                cmd_type = int(string.replace(item, self.CMD_OUT_PART_TYPE, ""))
                logging.debug("Detected command type {0:04X}".format(cmd_type))
            elif string.find(item, self.CMD_OUT_PART_LENGTH) == 0:
                length = int(string.replace(item, self.CMD_OUT_PART_LENGTH, ""))
                logging.debug("Detected data length {0}".format(length))
            elif string.find(item, self.CMD_OUT_PART_DATA) == 0:
                data = string.replace(item, self.CMD_OUT_PART_DATA, "")
                logging.debug("Detected data '{0}'".format(data))
            else:
                logging.warn("Unexpected item '{0}' found".format(item))

        if cmd_type is None or length is None or data is None:
            msg = "command type, length or data not detected for item '{}'!".format(item)
            logging.error(msg)
            raise ParseException(msg)
        if len(data) != length * 2:
            msg = "Expected data '{}' to have length {} (have {})".format(data, length * 2, len(data))
            logging.error(msg)
            raise ParseException(msg)
        return cmd_type, length, data

    def parse_data(self, data):
        result = {self.DATA_TYPE: None}
        for item in data:
            item_type = item[0]
            data = item[2]
            logging.debug("parsing item_type {}, data {}".format(item_type, data))
            if item_type == InterchangeProtocolType.TYPE_RENT_STATION_1:
                result[self.DATA_TYPE] = self.DATA_TYPE_START_RENTAL
            elif item_type == InterchangeProtocolType.TYPE_RENTAL_RETURN_1:
                result[self.DATA_TYPE] = self.DATA_TYPE_STOP_RENTAL
            elif item_type == InterchangeProtocolType.TYPE_SMARTLOCK_STATUS_1:
                result[self.DATA_TYPE] = self.DATA_TYPE_SMARTLOCK_STATUS
            elif item_type == InterchangeProtocolType.TYPE_DISCONNECT_1:
                result[self.DATA_TYPE] = self.DATA_TYPE_DISCONNECT
            elif item_type == InterchangeProtocolType.TYPE_PUBLIC_KEY_1:
                result[self.DATA_APP_PUBLIC_KEY] = data.decode("hex")
            elif item_type == InterchangeProtocolType.TYPE_BETRIEBSNUMMER_1:
                result[self.DATA_BETRIEBSNUMMER] = data.decode("hex")
            elif item_type == InterchangeProtocolType.TYPE_LOCK_ID_1:
                result[self.DATA_SMARTLOCK_SERIAL_NUMBER] = data.decode("hex")
            elif item_type == InterchangeProtocolType.TYPE_RFID_1:
                result[self.DATA_RFID_UID] = data.decode("hex")
            elif item_type == InterchangeProtocolType.TYPE_RIDE_DURATION_1:
                result[self.DATA_RIDE_DURATION] = int(data, 16)
            elif item_type == InterchangeProtocolType.TYPE_NUMBER_OF_INTERMEDIATE_STOPS_1:
                result[self.DATA_INTERMEDIATE_STOPS] = int(data, 16)
            elif item_type == InterchangeProtocolType.TYPE_NUMBER_OF_EMERGENCY_STOPS_1:
                result[self.DATA_EMERGENCY_STOPS] = int(data, 16)
            elif item_type == InterchangeProtocolType.TYPE_SCHLOSS_BATTERY_1:
                result[self.DATA_SMARTLOCK_BATTERY] = int(data, 16)
            elif item_type == InterchangeProtocolType.TYPE_EBIKE_BATTERY_1:
                result[self.DATA_EBIKE_BATTERY] = int(data, 16)
            elif item_type == InterchangeProtocolType.TYPE_VEHICLE_TYPE_1:
                result[self.DATA_BIKE_TYPE] = int(data, 16)
            elif item_type == InterchangeProtocolType.TYPE_SW_VERSION_1:
                result[self.DATA_SMARTLOCK_SW_VERSION] = data.decode("hex")
            else:
                logging.debug("Unexpected / unneeded item type {} (item: {})".format(item_type, item))
        return result

    @staticmethod
    def create_command(command, append=False):
        # type: (tuple, bool) -> str
        logging.debug("create_command: {}".format(command))
        command_type = command[0]
        length = command[1]
        data = command[2]
        result = "-t {0} -l {1} -d 0x{2} ".format(command_type, length, data)
        if append:
            result += "-a "
        return result

    @staticmethod
    def create_response_command(error_code=int):
        result = InterchangeProtocol.create_command((InterchangeProtocolType.TYPE_RESPONSE_1, 4, "%0.08X" % error_code))
        result += "-s -x "
        return result

    @staticmethod
    def create_container(container_type, commands):
        # type: (int, list) -> str
        """ Creates a container with the given code and commands """
        logging.debug("create_container with type {0} and commands {1}".format(container_type, commands))
        res = ""
        # add all commands
        for command in commands:
            res += InterchangeProtocol.create_command(command, append=True)

        res += "-t {0} -c -s -x".format(container_type)

        logging.debug("returning: {0}".format(res))
        return res
