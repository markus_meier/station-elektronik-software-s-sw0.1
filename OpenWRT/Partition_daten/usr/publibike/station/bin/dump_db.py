#!/usr/bin/env python
from optparse import OptionParser

from station import StationConfiguration
from station.database.access_media_table import AccessMediaTable
from station.database.configuration_table import ConfigurationTable
from station.database.database import Database
from station.database.firmware_table import FirmwareTable
from station.database.rental_table import RentalTable
from station.database.status_table import StatusTable
from station.database.vehicle_table import VehicleTable


class DumpDb(object):
    def __init__(self, config):
        # type: (StationConfiguration) -> None
        self._config = config
        self._database = Database(configuration=config)
        self._database.connect()

    def run(self):
        parser = OptionParser()
        parser.add_option("-a", "--access-media", dest="access_media", help="dump access-media table", default=False,
                          action="store_true")
        parser.add_option("-p", "--person", dest="person", help="dump person table", default=False, action="store_true")
        parser.add_option("-c", "--station-configuration", dest="station_configuration",
                          help="dump station configuration table", default=False, action="store_true")
        parser.add_option("-s", "--smartlock-configuration", dest="smartlock_configuration",
                          help="dump smartlock configuration table", default=False, action="store_true")
        parser.add_option("-f", "--firmware", dest="firmware", help="dump firmware table", default=False,
                          action="store_true")
        parser.add_option("-v", "--vehicle", dest="vehicle", help="dump vehicle table", default=False,
                          action="store_true")
        parser.add_option("-r", "--start-rental", dest="start_rental", help="dump start rental table", default=False,
                          action="store_true")
        parser.add_option("-t", "--stop-rental", dest="stop_rental", help="dump stop rental table", default=False,
                          action="store_true")
        parser.add_option("-u", "--status", dest="status", help="dump status table", default=False, action="store_true")

        (options, args) = parser.parse_args()
        if options.access_media:
            self.dump_table(self._database, AccessMediaTable, AccessMediaTable.fetch_access_media)
        elif options.person:
            self.dump_table(self._database, AccessMediaTable, AccessMediaTable.fetch_person_table)
        elif options.station_configuration:
            self.dump_table(self._database, ConfigurationTable, ConfigurationTable.fetch_station_configuration)
        elif options.smartlock_configuration:
            self.dump_table(self._database, ConfigurationTable, ConfigurationTable.fetch_smartlock_configuration)
        elif options.firmware:
            self.dump_table(self._database, FirmwareTable, FirmwareTable.fetch)
        elif options.vehicle:
            self.dump_table(self._database, VehicleTable, VehicleTable.fetch)
        elif options.start_rental:
            self.dump_table(self._database, RentalTable, RentalTable.get_start_rentals)
        elif options.stop_rental:
            self.dump_table(self._database, RentalTable, RentalTable.get_stop_rentals)
        elif options.status:
            self.dump_table(self._database, StatusTable, StatusTable.get_status_updates)
        else:
            print(parser.print_help())

    @staticmethod
    def dump_table(database, class_type, class_function):
        # type (Database, class, function) -> None
        table = class_type(database)
        table.create_table()
        items = class_function(table)
        DumpDb.print_items(items)

    @staticmethod
    def print_items(items):
        for item in items:
            print(item)


if __name__ == "__main__":
    station_configuration = StationConfiguration()
    DumpDb(config=station_configuration).run()
