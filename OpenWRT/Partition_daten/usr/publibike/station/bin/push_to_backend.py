#!/usr/bin/env python
import logging
from sqlite3 import IntegrityError

from generated.swagger_client.rest import ApiException
from station import StationConfiguration
from station.backend.rentals_api_helper import RentalsApiHelper
from station.backend.status_api_helper import StatusApiHelper
from station.database.database import Database
from station.database.rental_table import RentalTable
from station.database.status_table import StatusTable
from station.log import setup_log
from station.utils.rental_utils import RentalUtils
from station.utils.status_utils import StatusUtils


class PushToBackend(object):
    def __init__(self, station_configuration=StationConfiguration):
        self._station_configuration = station_configuration
        self._database = Database(self._station_configuration)

    def push_to_backend(self):
        try:
            logging.info("push_to_backend started")

            self._database.connect()

            self.push_status_updates()
            self.push_rentals()

            self._database.disconnect()
            logging.info("push_to_backend finished successfully")
        except Exception:
            logging.exception("Unexpected error occurred in push_to_backend")

    def push_status_updates(self):
        try:
            logging.info("Starting push status updates")

            api_helper = StatusApiHelper(self._station_configuration)
            status_table = StatusTable(self._database)
            status_table.create_table()
            util = StatusUtils(api_helper=api_helper, status_table=status_table, config=self._station_configuration)

            # TODO get station battery level, see PUBL-1588
            util.push_to_backend(42.0)

        except ApiException:
            logging.exception("API exception while pushing")
        except IntegrityError:
            logging.exception("DB integrity error while pushing")
        except Exception:
            logging.exception("Unexpected exception while pushing")

    def push_rentals(self):
        try:
            logging.info("Starting to push rentals")

            api_helper = RentalsApiHelper(self._station_configuration)
            rental_table = RentalTable(self._database)
            rental_table.create_table()
            util = RentalUtils(api_helper=api_helper, rental_table=rental_table, config=self._station_configuration)

            util.push_to_backend()

        except ApiException:
            logging.exception("API exception while pushing")
        except IntegrityError:
            logging.exception("DB integrity error while pushing")
        except Exception:
            logging.exception("Unexpected exception while pushing")


def push_to_backend():
    try:
        station_configuration = StationConfiguration()
        setup_log(station_configuration)

        pusher = PushToBackend(station_configuration=station_configuration)
        pusher.push_to_backend()

    except Exception as e:
        logging.error("Unexpected error occurred in push_to_backend", e)
        logging.exception(e)


if __name__ == "__main__":
    push_to_backend()
