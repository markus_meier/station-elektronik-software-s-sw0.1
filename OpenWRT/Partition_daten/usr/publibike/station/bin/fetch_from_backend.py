#!/usr/bin/env python
import logging
from sqlite3 import IntegrityError

from generated.swagger_client.rest import ApiException
from station import StationConfiguration
from station.backend import AccessMediaApiHelper
from station.backend.configuration_api_helper import ConfigurationApiHelper
from station.backend.vehicles_api_helper import VehiclesApiHelper
from station.database.access_media_table import AccessMediaTable
from station.database.configuration_table import ConfigurationTable
from station.database.database import Database
from station.database.firmware_table import FirmwareTable
from station.database.updatetoken_table import UpdatetokenTable
from station.database.vehicle_table import VehicleTable
from station.log import setup_log
from station.utils.access_media_utils import AccessMediaUtils
from station.utils.configuration_utils import ConfigurationUtils
from station.utils.vehicle_utils import VehicleUtils


class FetchFromBackend(object):
    def __init__(self, station_configuration):
        # type: (StationConfiguration) -> None
        self._station_configuration = station_configuration
        self._database = Database(self._station_configuration)
        self._updatetoken_table = UpdatetokenTable(database=self._database)

    def fetch_from_backend(self):
        try:
            logging.info("fetch_from_backend started")

            self._database.connect()
            self._updatetoken_table.create_table()

            self.fetch_access_medias()
            self.fetch_vehicles()
            self.fetch_configuration()

            self._database.disconnect()
            logging.info("fetch_from_backend finished successfully")
        except Exception:
            logging.exception("Unexpected error occurred in fetch_from_backend")

    def fetch_access_medias(self):
        """ Fetches access-medias from backend. Note Database needs to be connected before calling this! """
        try:
            logging.info("Starting to update access-medias")

            api_helper = AccessMediaApiHelper(self._station_configuration)
            access_media_table = AccessMediaTable(self._database)
            access_media_table.create_table()
            util = AccessMediaUtils(api_helper=api_helper, access_media_table=access_media_table,
                                    updatetoken_table=self._updatetoken_table, config=self._station_configuration)

            self.do_fetch(util)
        except Exception:
            logging.exception("Unexpected exception while updating access-medias")

    def fetch_vehicles(self):
        try:
            logging.info("Starting to update vehicles")

            api_helper = VehiclesApiHelper(self._station_configuration)
            vehicle_table = VehicleTable(self._database)
            vehicle_table.create_table()
            util = VehicleUtils(api_helper=api_helper, vehicle_table=vehicle_table,
                                updatetoken_table=self._updatetoken_table, config=self._station_configuration)

            self.do_fetch(util)
        except Exception:
            logging.exception("Unexpected exception while updating vehicles")

    def fetch_configuration(self):
        try:
            logging.info("Starting to update configuration")

            api_helper = ConfigurationApiHelper(self._station_configuration)
            configuration_table = ConfigurationTable(self._database)
            configuration_table.create_table()
            firmware_table = FirmwareTable(self._database)
            firmware_table.create_table()
            util = ConfigurationUtils(api_helper=api_helper, configuration_table=configuration_table,
                                      firmware_table=firmware_table, config=self._station_configuration)

            self.do_fetch(util)
        except Exception:
            logging.exception("Unexpected exception while updating configuration")

    def do_fetch(self, util, delete_data=False):
        try:
            logging.debug("Starting to update with delete_data {}".format(delete_data))
            util.update_from_backend(delete_data=delete_data)
            logging.info("Update finished successfully")
        except ApiException:
            logging.exception("API exception while updating")
        except IntegrityError:
            logging.exception("DB integrity error")
            if not delete_data:
                logging.info("Deleting and re-fetching data")
                self.do_fetch(util, delete_data=True)
            if delete_data:
                logging.warn("Integrity error occurred again after deleting data...")
                raise
        except Exception:
            logging.exception("Unexpected exception while updating")


def fetch_from_backend():
    try:
        station_configuration = StationConfiguration()
        setup_log(station_configuration)

        fetcher = FetchFromBackend(station_configuration=station_configuration)
        fetcher.fetch_from_backend()

    except Exception:
        logging.exception("Unexpected error occurred in fetch_from_backend")


if __name__ == "__main__":
    fetch_from_backend()
