#!/usr/bin/env python
import logging
from optparse import OptionParser

from station import StationConfiguration
from station.backend import AccessMediaApiHelper
from station.backend.rentals_api_helper import RentalsApiHelper
from station.backend.status_api_helper import StatusApiHelper
from station.backend.vehicles_api_helper import VehiclesApiHelper
from station.database.access_media_table import AccessMediaTable
from station.database.database import Database
from station.database.rental_table import RentalTable
from station.database.status_table import StatusTable
from station.database.updatetoken_table import UpdatetokenTable
from station.database.vehicle_table import VehicleTable
from station.log import setup_log
from station.utils.access_media_utils import AccessMediaUtils
from station.utils.bluetooth_utils import BluetoothUtils
from station.utils.rental_utils import RentalUtils
from station.utils.status_utils import StatusUtils
from station.utils.vehicle_utils import VehicleUtils


class HandleBluetoothRequest(object):
    def __init__(self, station_configuration):
        # type: (StationConfiguration) -> None
        self._station_configuration = station_configuration

        self._access_media_api_helper = AccessMediaApiHelper(station_configuration)
        self._status_api_helper = StatusApiHelper(station_configuration)
        self._rental_api_helper = RentalsApiHelper(station_configuration)
        self._vehicle_api_helper = VehiclesApiHelper(station_configuration)

        self._database = Database(station_configuration)
        self._access_media_table = AccessMediaTable(self._database)
        self._updatetoken_table = UpdatetokenTable(self._database)
        self._status_table = StatusTable(self._database)
        self._rental_table = RentalTable(self._database)
        self._vehicle_table = VehicleTable(self._database)

        self._access_media_utils = AccessMediaUtils(self._access_media_api_helper, self._access_media_table,
                                                    station_configuration, self._updatetoken_table)
        self._status_utils = StatusUtils(self._status_api_helper, self._status_table, station_configuration)
        self._rental_utils = RentalUtils(self._rental_api_helper, self._rental_table, station_configuration)
        self._vehicle_utils = VehicleUtils(self._vehicle_api_helper, self._vehicle_table, station_configuration,
                                           self._updatetoken_table)

        self._bluetooth_utils = BluetoothUtils(self._access_media_utils, self._status_utils, self._rental_utils,
                                               self._vehicle_utils)

    def handle_bluetooth_request(self):
        # type: () -> None
        parser = OptionParser()
        parser.add_option("-d", "--device", dest="device", help="device to handle bluetooth request from",
                          default="/dev/aot_iped_COM0")
        (options, args) = parser.parse_args()

        try:
            self._database.connect()
            self.create_tables()

            self.read_from_device(options.device)

            self._database.disconnect()
        except Exception:
            logging.exception("Unexpected exception caught")

    def read_from_device(self, dev):
        # type: (str) -> None
        logging.debug("opening device: {}".format(dev))
        try:
            with open(dev, 'r+') as f:
                connected = True
                while connected:
                    line = f.readline()
                    logging.debug("Received line: {}".format(line))
                    if len(line) > 0:
                        response = self._bluetooth_utils.process_incoming_request(line)
                        if response is not None:
                            logging.debug("Sending response: {}".format(response))
                            f.write(response + "\n")
                        else:
                            logging.debug("Received disconnect --> stopping to read from device")
                            connected = False

        except IOError:
            logging.exception("Caught IOError during device access")

    def create_tables(self):
        self._updatetoken_table.create_table()
        self._status_table.create_table()
        self._rental_table.create_table()
        self._access_media_table.create_table()
        self._vehicle_table.create_table()


def handle_bluetooth_request():
    try:
        station_configuration = StationConfiguration()
        setup_log(station_configuration)

        handler = HandleBluetoothRequest(station_configuration)
        handler.handle_bluetooth_request()

    except Exception:
        logging.exception("Unexpected error occurred")


if __name__ == "__main__":
    handle_bluetooth_request()
