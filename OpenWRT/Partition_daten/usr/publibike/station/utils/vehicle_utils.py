import logging
from sqlite3 import IntegrityError

from station import StationConfiguration
from station.backend.vehicles_api_helper import VehiclesApiHelper
from station.backend_constants import BackendConstants
from station.database.updatetoken_table import UpdatetokenTable
from station.database.vehicle_table import VehicleTable


class VehicleUtils(object):
    def __init__(self, api_helper, vehicle_table, config, updatetoken_table):
        # type: (VehiclesApiHelper, VehicleTable, StationConfiguration, UpdatetokenTable) -> None
        self.api_helper = api_helper
        self.vehicle_table = vehicle_table
        self.config = config
        self.updatetoken_table = updatetoken_table

    def update_from_backend(self, delete_data=False):
        # type: (bool) -> None
        try:
            if delete_data:
                logging.info("delete_data enabled -> setting empty update-token and dropping tables")
                self.updatetoken_table.set_access_media_updatetoken("")
                self.vehicle_table.drop_table(commit=False)
                self.vehicle_table.create_table(commit=False)
            updatetoken = self.updatetoken_table.get_vehicle_updatetoken()
            vehicles = self.api_helper.fetch(self.config.station_id(), updatetoken)
            self._update_vehicles_in_db(vehicles.vehicles)
            self.updatetoken_table.set_vehicle_updatetoken(vehicles.update_token)
            self.updatetoken_table.commit()
            logging.debug("Committed changes to DB.")
        except IntegrityError as e:
            self.vehicle_table.rollback()
            self.updatetoken_table.rollback()
            logging.exception("DB integrity error, rolled back changes", e)
            raise

    def _update_vehicles_in_db(self, vehicles):
        """ Separate function that we can unit tests adding/removing vehicles without using the backend """
        logging.info("Number of vehicles from backend to process: {}".format(len(vehicles)))
        for vehicle in vehicles:
            if vehicle.state_id in [BackendConstants.VEHICLE_STATE_ACTIVE, BackendConstants.VEHICLE_STATE_BROKEN,
                                    BackendConstants.VEHICLE_STATE_BLOCKED]:
                if self.vehicle_table.get_vehicle(vehicle.id):
                    logging.debug("Updating vehicle {}".format(vehicle))
                    self.vehicle_table.update_vehicle(vehicle)
                else:
                    logging.debug("Adding vehicle {}".format(vehicle))
                    self.vehicle_table.add_vehicle(vehicle)
            elif vehicle.state_id == BackendConstants.VEHICLE_STATE_DELETED:
                logging.debug("Removing vehicle {}".format(vehicle))
                self.vehicle_table.remove_vehicle(vehicle.id)
            else:
                msg = "Unexpected vehicle state_id {} (vehicle {})".format(vehicle.state_id, vehicle)
                logging.warn(msg)
                raise Exception(msg)
        logging.debug("Committing changes to DB")
        self.vehicle_table.commit()

    def get_vehicle_from_smartlock_serial_number(self, smartlock_serial_number):
        # type: (str) -> tuple
        return self.vehicle_table.get_vehicle_from_smartlock_serial_number(smartlock_serial_number)
