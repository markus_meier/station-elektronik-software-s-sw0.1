import logging

from generated.swagger_client import StationStatus, Vehicle
from station import StationConfiguration
from station.backend.status_api_helper import StatusApiHelper
from station.database.status_table import StatusTable


class StatusUtils(object):
    def __init__(self, api_helper, status_table, config):
        # type: (StatusApiHelper, StatusTable, StationConfiguration) -> None
        self.api_helper = api_helper
        self.status_table = status_table
        self.config = config

    def add_status_entry(self, vehicle_id, smartlock_battery_level, ebike_battery_level):
        # type: (int, float, float) -> None
        """ Adds a status entry to the local database """
        logging.info("Adding status entry for vehicle {}: smartlock battery level {}, ebike battery level {}".format(
            vehicle_id, smartlock_battery_level, ebike_battery_level))
        self.status_table.add_status_update(vehicle_id=vehicle_id, smartlock_battery_level=smartlock_battery_level,
                                            ebike_battery_level=ebike_battery_level)
        self.status_table.commit()

    def push_to_backend(self, station_battery_level):
        # type: (float) -> None
        """ pushes the status entries from the database to the backend and removes them from the database """

        station_id = self.config.station_id()
        api = self.api_helper

        status = StationStatus(station_battery_level, [])
        status_entries_db = self.status_table.get_status_updates()
        for status_entry in status_entries_db:
            vehicle = Vehicle(id=status_entry[StatusTable.VEHICLE_STATUS_ID],
                              smartlock_battery_level=status_entry[StatusTable.VEHICLE_STATUS_SMARTLOCK_LEVEL],
                              ebike_battery_level=status_entry[StatusTable.VEHICLE_STATUS_EBIKE_LEVEL])
            status.vehicles.append(vehicle)

        # push it
        api.put(station_id, status)

        # and drop data from the local DB
        self.status_table.delete_entries()
        self.status_table.commit()
