import logging
from sqlite3 import IntegrityError
from types import ListType

from station import StationConfiguration
from station.backend import AccessMediaApiHelper
from station.backend_constants import BackendConstants
from station.database.access_media_table import AccessMediaTable
from station.database.updatetoken_table import UpdatetokenTable


class AccessMediaUtils(object):
    def __init__(self, api_helper, access_media_table, config, updatetoken_table):
        # type: (AccessMediaApiHelper, AccessMediaTable, StationConfiguration, UpdatetokenTable) -> None
        self.api_helper = api_helper
        self.access_media_table = access_media_table
        self.config = config
        self.updatetoken_table = updatetoken_table

    def update_from_backend(self, delete_data=False):
        if delete_data:
            logging.info("delete_data enabled -> setting empty update-token and dropping table")
            self.updatetoken_table.set_access_media_updatetoken("")
            self.access_media_table.drop_table(commit=False)
            self.access_media_table.create_table(commit=False)
        updatetoken = self.updatetoken_table.get_access_media_updatetoken()
        access_medias = self.api_helper.fetch(updatetoken)
        self._update_access_medias_in_db(access_medias.data)
        self.updatetoken_table.set_access_media_updatetoken(access_medias.update_token)
        self.updatetoken_table.commit()

    def _update_access_medias_in_db(self, access_medias):
        """ Separate function that we can unit test adding/removing access-medias without using the backend """
        logging.info("Number of persons and access-medias from backend to process: {}".format(len(access_medias)))
        try:
            for access_media in access_medias:
                if access_media.state_id in [BackendConstants.PERSON_STATE_PENDING,
                                             BackendConstants.PERSON_STATE_ACTIVE,
                                             BackendConstants.PERSON_STATE_BLOCKED,
                                             BackendConstants.PERSON_STATE_PAYMENT_DECLINED]:

                    # Check if person already exists
                    if self.access_media_table.get_person(access_media.person_id):
                        logging.debug("Updating person and access-media {}".format(access_media))
                        self.access_media_table.update_person(access_media)
                        for item in access_media.access_medias:
                            if item.state == BackendConstants.ACCESS_MEDIA_STATE_ACTIVE:
                                # check if access media already exists
                                if self.access_media_table.get_access_media(item.id) is None:
                                    logging.debug(
                                        "Adding access media {} for person {}".format(item, access_media.person_id))
                                    self.access_media_table.add_access_media_for_person(item, access_media.person_id)
                                else:
                                    logging.debug("Updating access media {}".format(item))
                                    self.access_media_table.update_access_media(item)
                            elif item.state == BackendConstants.ACCESS_MEDIA_STATE_INACTIVE:
                                logging.debug(
                                    "Removing access media {} from person {}".format(item, access_media.person_id))
                                self.access_media_table.delete_access_media(item.id)
                            else:
                                logging.error("Unexpected item state {} (item: {})".format(item.state, item))
                    else:
                        logging.debug("Adding person and access-media {}".format(access_media))
                        # Remove disabled access-medias
                        access_media.access_medias = self._strip_disabled_access_medias(access_media.access_medias)
                        self.access_media_table.add_person_with_access_media(access_media)
                elif access_media.state_id == BackendConstants.PERSON_STATE_DELETED:
                    logging.debug("Removing person {}".format(access_media))
                    self.access_media_table.remove_person_with_access_media(access_media.person_id)
                else:
                    msg = "Unexpected person / access-media state_id {} (vehicle {})".format(access_media.state_id,
                                                                                             access_media)
                    logging.warn(msg)
                    raise Exception(msg)
            logging.debug("Committing changes to DB")
            self.access_media_table.commit()
            self.updatetoken_table.commit()
        except IntegrityError:
            self.access_media_table.rollback()
            self.updatetoken_table.rollback()
            logging.exception("DB integrity error, rolled back changes")
            raise

    @staticmethod
    def _strip_disabled_access_medias(access_medias):
        access_media_without_disabled_medias = access_medias
        for item in access_media_without_disabled_medias:
            if item.state == BackendConstants.ACCESS_MEDIA_STATE_INACTIVE:
                access_media_without_disabled_medias.remove(item)
        return access_media_without_disabled_medias

    def get_person_from_public_key(self, public_key):
        # type: (str) -> tuple
        """ Returns the person id, state and access media id of the given public key
        @:returns person id, person state and access-media id if found, None if not found
        """
        return self.access_media_table.get_person_from_access_media(
            access_media_type=BackendConstants.ACCESS_MEDIA_TYPE_PHONE,
            access_media=public_key)

    def get_person_from_rfid(self, rfid_uid):
        # type: (str) -> tuple
        """ Returns the person of the given RFID UID
        @:returns person id, person state and access-media id if found, None if not found
        """
        return self.access_media_table.get_person_from_access_media(
            access_media_type=BackendConstants.ACCESS_MEDIA_TYPE_RFID,
            access_media=rfid_uid)

    def get_access_medias_for_person(self, person_id):
        # type: (int) -> ListType[tuple]
        """ Returns all access media of given person_id
        @:returns all access media of the given person
        """
        return self.access_media_table.get_access_medias_for_person(person_id=person_id)
