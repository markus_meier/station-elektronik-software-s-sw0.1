import logging
from sqlite3 import IntegrityError
from types import ListType

from generated.swagger_client import Configuration
from station import StationConfiguration
from station.backend.configuration_api_helper import ConfigurationApiHelper
from station.database.configuration_table import ConfigurationTable
from station.database.firmware_table import FirmwareTable


class ConfigurationUtils(object):
    def __init__(self, api_helper, configuration_table, firmware_table, config):
        # type: (ConfigurationApiHelper, ConfigurationTable, FirmwareTable, StationConfiguration) -> None
        self.api_helper = api_helper
        self.configuration_table = configuration_table
        self.firmware_table = firmware_table
        self.config = config

    def update_from_backend(self, delete_data=False):
        # type: (bool) -> None
        try:
            if delete_data:
                logging.info("delete_data enabled -> dropping table")
                self.configuration_table.drop_table(commit=False)
                self.configuration_table.create_table(commit=False)
                self.firmware_table.drop_table(commit=False)
                self.firmware_table.create_table(commit=False)
            station_configuration = self.api_helper.fetch(self.config.station_id())
            self._update_firmware_in_db(station_configuration.firmware)
            self._update_configuration_in_db(station_configuration.station_configuration,
                                             station_configuration.smartlock_configuration)
            self.configuration_table.commit()
            self.firmware_table.commit()
            logging.debug("Committed changes to DB.")
        except IntegrityError as e:
            self.configuration_table.rollback()
            self.firmware_table.rollback()
            logging.exception("DB integrity error, rolled back changes", e)
            raise

    def _update_firmware_in_db(self, firmwares):
        logging.info("Number of firmwares from backend to store: {}".format(len(firmwares)))
        self.firmware_table.update_firmware(firmwares)

    def _update_configuration_in_db(self, station_configuration=Configuration, smartlock_configuration=Configuration):
        # type: (ListType[dict], ListType[dict]) -> None
        logging.info("Number of configuration items for station {}, smartlock {}".format(
            len(station_configuration), len(smartlock_configuration)))
        self.configuration_table.update_station_configuration(station_configuration)
        self.configuration_table.update_smartlock_configuration(smartlock_configuration)
