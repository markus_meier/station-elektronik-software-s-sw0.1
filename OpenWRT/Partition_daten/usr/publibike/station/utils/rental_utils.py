import datetime
import logging

from station import StationConfiguration
from station.backend.rentals_api_helper import RentalsApiHelper
from station.database.rental_table import RentalTable


class RentalUtils(object):
    def __init__(self, api_helper, rental_table, config):
        # type: (RentalsApiHelper, RentalTable, StationConfiguration) -> None
        self.api_helper = api_helper
        self.rental_table = rental_table
        self.config = config

    def add_start_rental(self, vehicle_id, access_media_id):
        # type: (int, int) -> None
        logging.info("Adding start rental for vehicle {}, access-media {}".format(vehicle_id, access_media_id))
        now = datetime.datetime.now()
        self.rental_table.add_start_rental(vehicle_id=vehicle_id, access_media_id=access_media_id, start_date=now)
        self.rental_table.commit()

    def add_stop_rental(self, vehicle_id, duration_s, number_of_intermediate_stops, number_of_emergency_stops,
                        access_media_id, aborted):
        # type: (int, int, int, int, int, bool) -> None
        logging.info("Adding stop rental for vehicle {}, duration {}, intermediate stops{}, emergency stops {}, "
                     "access-media {}, aborted {}".format(vehicle_id, duration_s, number_of_intermediate_stops,
                                                          number_of_emergency_stops, access_media_id, aborted))
        now = datetime.datetime.now()
        self.rental_table.add_stop_rental(vehicle_id, now, duration_s, number_of_intermediate_stops,
                                          number_of_emergency_stops, aborted, access_media_id)
        self.rental_table.commit()

    def push_to_backend(self):
        # type: (None) -> None
        """ Pushes rentals to backend and removes them locally """
        station_id = self.config.station_id()
        api = self.api_helper

        rentals = []

        start_rentals = self.rental_table.get_start_rentals()
        for start_rental in start_rentals:
            r = api.create_start_rental(start_rental[RentalTable.VEHICLE_ID], start_rental[RentalTable.ACCESS_MEDIA_ID],
                                        timestamp=start_rental[RentalTable.START_RENTAL_START])
            rentals.append(r)

        stop_rentals = self.rental_table.get_stop_rentals()
        for stop_rental in stop_rentals:
            r = api.create_stop_rental(stop_rental[RentalTable.VEHICLE_ID],
                                       stop_rental[RentalTable.STOP_RENTAL_DURATION],
                                       stop_rental[RentalTable.STOP_RENTAL_STOP],
                                       stop_rental[RentalTable.STOP_RENTAL_INTERMEDIATE_STOPS],
                                       stop_rental[RentalTable.STOP_RENTAL_EMERGENCY_STOPS],
                                       stop_rental[RentalTable.STOP_RENTAL_ABORTED],
                                       stop_rental[RentalTable.ACCESS_MEDIA_ID])
            rentals.append(r)

        # push rentals if available
        if len(rentals) > 0:
            logging.debug("Sending rentals {} from station {}".format(rentals, station_id))
            api.put(station_id, rentals)

        # and drop data from the local DB
        self.rental_table.delete_start_rentals(start_rentals)
        self.rental_table.delete_stop_rentals(stop_rentals)
        self.rental_table.commit()
