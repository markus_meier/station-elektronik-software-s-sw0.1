import logging

from station.backend_constants import BackendConstants
from station.bluetooth.interchange_protocol import InterchangeProtocol
from station.bluetooth.interchange_protocol_error import InterchangeProtocolError
from station.bluetooth.interchange_protocol_type import InterchangeProtocolType
from station.database.access_media_table import AccessMediaTable
from station.database.vehicle_table import VehicleTable
from station.utils.access_media_utils import AccessMediaUtils
from station.utils.rental_utils import RentalUtils
from station.utils.status_utils import StatusUtils
from station.utils.vehicle_utils import VehicleUtils


class BluetoothUtils(object):
    def __init__(self, access_media_utils, status_utils, rental_utils, vehicle_utils):
        # type: (AccessMediaUtils, StatusUtils, RentalUtils, VehicleUtils) -> None
        self.access_media_utils = access_media_utils
        self.status_utils = status_utils
        self.rental_utils = rental_utils
        self.vehicle_utils = vehicle_utils

        self.interchange_protocol = InterchangeProtocol()

    def process_incoming_request(self, request):
        # type: (str) -> str
        # None -> Disconnect
        parsed_request = self.interchange_protocol.parse_cli_output(request)
        data = self.interchange_protocol.parse_data(parsed_request)

        data_type = data[InterchangeProtocol.DATA_TYPE]
        if data_type == InterchangeProtocol.DATA_TYPE_START_RENTAL:
            return self._process_start_rental(data)
        elif data_type == InterchangeProtocol.DATA_TYPE_STOP_RENTAL:
            return self._process_stop_rental(data)
        elif data_type == InterchangeProtocol.DATA_TYPE_SMARTLOCK_STATUS:
            return self._process_smartlock_status(data)
        elif data_type == InterchangeProtocol.DATA_TYPE_DISCONNECT:
            return self._process_disconnect(data)
        else:
            logging.error("Unknown request type {} (data: {})".format(data_type, data))
            # for now just return an empty string, so we're ignoring unknown messages
            return ""

    def _process_start_rental(self, data):
        logging.debug("Rental with data: {}".format(data))
        if InterchangeProtocol.DATA_APP_PUBLIC_KEY in data:
            return self._process_app_start_rental(data)
        elif InterchangeProtocol.DATA_RFID_UID in data:
            return self._process_rfid_start_rental(data)
        else:
            msg = "Unknown rental, not APP and RFID: {}".format(data)
            logging.error(msg)
            raise Exception(msg)

    def _process_app_start_rental(self, data):
        logging.debug("APP rental with data: {}".format(data))

        # verify person
        public_key = data[InterchangeProtocol.DATA_APP_PUBLIC_KEY]
        person = self.access_media_utils.get_person_from_public_key(public_key)
        if person is None:
            logging.info("Could not find person for public key {}".format(public_key))
            return self.interchange_protocol.create_response_command(InterchangeProtocolError.BUS_PERSON_UNKNOWN)
        res = self._verify_person(person)
        if res is not None:
            return res

        # verify vehicle
        smartlock_serial_number = data[InterchangeProtocol.DATA_SMARTLOCK_SERIAL_NUMBER]
        vehicle = self.vehicle_utils.get_vehicle_from_smartlock_serial_number(smartlock_serial_number)
        res = self._verify_vehicle(vehicle, smartlock_serial_number)
        if res is not None:
            return res

        # we allow to rent!
        # store in DB and fetch access medias of the given person and return them in the container
        self._create_start_rental_db_entry(person[AccessMediaTable.ACCESS_MEDIA_ID], vehicle[VehicleTable.VEHICLE_ID])

        # since we get smartlock (and ebike) battery levels, we can also add a status update entry
        self._add_status_entry(data, vehicle[VehicleTable.VEHICLE_ID])

        # create response
        return self._create_start_rental_success_container(person[AccessMediaTable.PERSON_ID])

    def _process_rfid_start_rental(self, data):
        logging.debug("RFID rental with data: {}".format(data))

        # verify person
        rfid_uid = data[InterchangeProtocol.DATA_RFID_UID]
        person = self.access_media_utils.get_person_from_rfid(rfid_uid)
        if person is None:
            logging.info("Could not find person for rfid uid {}".format(rfid_uid))
            return self.interchange_protocol.create_response_command(InterchangeProtocolError.BUS_PERSON_UNKNOWN)
        res = self._verify_person(person)
        if res is not None:
            return res

        # verify vehicle
        smartlock_serial_number = data[InterchangeProtocol.DATA_SMARTLOCK_SERIAL_NUMBER]
        vehicle = self.vehicle_utils.get_vehicle_from_smartlock_serial_number(smartlock_serial_number)
        res = self._verify_vehicle(vehicle, smartlock_serial_number)
        if res is not None:
            return res

        # we allow to rent!
        # store in DB and fetch access medias of the given person and return them in the container
        self._create_start_rental_db_entry(person[AccessMediaTable.ACCESS_MEDIA_ID], vehicle[VehicleTable.VEHICLE_ID])

        # since we get smartlock (and ebike) battery levels, we can also add a status update entry
        self._add_status_entry(data, vehicle[VehicleTable.VEHICLE_ID])

        # create response
        return self._create_start_rental_success_container(person[AccessMediaTable.PERSON_ID])

    def _verify_person(self, person):
        logging.debug("Verifying person {}".format(person))
        person_state = person[AccessMediaTable.PERSON_STATE_ID]
        if person_state == BackendConstants.PERSON_STATE_PAYMENT_DECLINED:
            logging.info("Person {} is in PAYMENT_DECLINED state".format(person[AccessMediaTable.PERSON_ID]))
            return self.interchange_protocol.create_response_command(
                InterchangeProtocolError.BUS_PERSON_PAYMENT_DECLINED)
        elif person_state == BackendConstants.PERSON_STATE_BLOCKED:
            logging.info("Person {} is in BLOCKED state".format(person[AccessMediaTable.PERSON_ID]))
            return self.interchange_protocol.create_response_command(InterchangeProtocolError.BUS_PERSON_BLOCKED)
        return None

    def _verify_vehicle(self, vehicle, smartlock_serial_number, check_state=True):
        logging.debug("Verifying vehicle {}, smartlock serial number {}".format(vehicle, smartlock_serial_number))
        if vehicle is None:
            logging.info("Could not find vehicle with smartlock serial number {}".format(smartlock_serial_number))
            return self.interchange_protocol.create_response_command(InterchangeProtocolError.BUS_VEHICLE_UNKNOWN)
        if not check_state:
            return None
        vehicle_state = vehicle[VehicleTable.VEHICLE_STATE_ID]
        if vehicle_state == BackendConstants.VEHICLE_STATE_BLOCKED:
            logging.info("Vehicle {} is in BLOCKED state".format(vehicle[VehicleTable.VEHICLE_ID]))
            return self.interchange_protocol.create_response_command(InterchangeProtocolError.BUS_VEHICLE_BLOCKED)
        return None

    def _create_start_rental_success_container(self, person_id):
        logging.debug("Creating rental success container for person {}".format(person_id))
        person_id_hex = "{}".format(person_id).encode("hex")
        container_content = [(InterchangeProtocolType.TYPE_USER_ID_1, len(person_id_hex), person_id_hex)]

        # TODO get configuration values, once PUBL-1640 is implemented
        container_content.append((InterchangeProtocolType.TYPE_MAX_RENTAL_TIME_1, 2, "FFFF"))
        container_content.append((InterchangeProtocolType.TYPE_EBIKE_SUPPORT_LEVEL_1, 1, "05"))

        access_medias = self.access_media_utils.get_access_medias_for_person(person_id)
        for access_media in access_medias:
            access_media_type = access_media[AccessMediaTable.ACCESS_MEDIA_TYPE_ID]
            access_media_key = access_media[AccessMediaTable.ACCESS_MEDIA_ACCESS_MEDIA]
            access_media_key_hex = access_media_key.encode("hex")

            if access_media_type == BackendConstants.ACCESS_MEDIA_TYPE_PHONE:
                container_content.append(
                    (InterchangeProtocolType.TYPE_PUBLIC_KEY_1, len(access_media_key_hex), access_media_key_hex))
            elif access_media_type == BackendConstants.ACCESS_MEDIA_TYPE_RFID:
                container_content.append(
                    (InterchangeProtocolType.TYPE_RFID_1, len(access_media_key_hex), access_media_key_hex))
            else:
                logging.warn(
                    "Unexpected access media type {} (access media {})".format(access_media_type, access_media))
        return self.interchange_protocol.create_container(InterchangeProtocolType.TYPE_CREDENTIALS_1, container_content)

    def _create_start_rental_db_entry(self, access_media_id, vehicle_id):
        logging.debug("Creating start rental for access_media {} and vehicle {}".format(access_media_id, vehicle_id))
        self.rental_utils.add_start_rental(vehicle_id=vehicle_id, access_media_id=access_media_id)

    def _create_stop_rental_db_entry(self, vehicle_id, duration_s, intermediate_stops, emergency_stops,
                                     access_media_id):
        # type: (int, int, int, int, int) -> None
        logging.debug("Creating stop rental for vehicle {}: duration {}, intermediate stops {}, emergency stops {}, "
                      "access-media {}".format(vehicle_id, duration_s, intermediate_stops, emergency_stops,
                                               access_media_id))
        self.rental_utils.add_stop_rental(vehicle_id=vehicle_id, duration_s=duration_s,
                                          number_of_intermediate_stops=intermediate_stops,
                                          number_of_emergency_stops=emergency_stops, aborted=False,
                                          access_media_id=access_media_id)

    def _process_stop_rental(self, data):
        # type: (dict) -> str
        logging.debug("Rental return with data: {}".format(data))
        if InterchangeProtocol.DATA_APP_PUBLIC_KEY in data:
            return self._process_app_stop_rental(data)
        elif InterchangeProtocol.DATA_RFID_UID in data:
            return self._process_rfid_stop_rental(data)
        else:
            msg = "Unknown rental, not APP and RFID: {}".format(data)
            logging.error(msg)
            raise Exception(msg)

    def _process_app_stop_rental(self, data):
        # type: (dict) -> str
        logging.debug("APP rental return with data: {}".format(data))

        # verify person
        public_key = data[InterchangeProtocol.DATA_APP_PUBLIC_KEY]
        person = self.access_media_utils.get_person_from_public_key(public_key)
        if person is None:
            logging.info("Could not find person for public key {}".format(public_key))
            return self.interchange_protocol.create_response_command(InterchangeProtocolError.BUS_PERSON_UNKNOWN)

        # verify vehicle
        smartlock_serial_number = data[InterchangeProtocol.DATA_SMARTLOCK_SERIAL_NUMBER]
        vehicle = self.vehicle_utils.get_vehicle_from_smartlock_serial_number(smartlock_serial_number)
        res = self._verify_vehicle(vehicle, smartlock_serial_number, check_state=False)
        if res is not None:
            return res

        # we allow rental return!
        # store in DB and return BUS_OK
        self._create_stop_rental_db_entry(vehicle[VehicleTable.VEHICLE_ID],
                                          duration_s=data[InterchangeProtocol.DATA_RIDE_DURATION],
                                          intermediate_stops=data[InterchangeProtocol.DATA_INTERMEDIATE_STOPS],
                                          emergency_stops=data[InterchangeProtocol.DATA_EMERGENCY_STOPS],
                                          access_media_id=person[AccessMediaTable.ACCESS_MEDIA_ID])
        return self.interchange_protocol.create_response_command(InterchangeProtocolError.BUS_OK)

    def _process_rfid_stop_rental(self, data):
        # type: (dict) -> str
        logging.debug("RFID rental return with data: {}".format(data))

        # verify person
        rfid_uid = data[InterchangeProtocol.DATA_RFID_UID]
        person = self.access_media_utils.get_person_from_rfid(rfid_uid)
        if person is None:
            logging.info("Could not find person for RFID UID {}".format(rfid_uid))
            return self.interchange_protocol.create_response_command(InterchangeProtocolError.BUS_PERSON_UNKNOWN)

        # verify vehicle
        smartlock_serial_number = data[InterchangeProtocol.DATA_SMARTLOCK_SERIAL_NUMBER]
        vehicle = self.vehicle_utils.get_vehicle_from_smartlock_serial_number(smartlock_serial_number)
        res = self._verify_vehicle(vehicle, smartlock_serial_number, check_state=False)
        if res is not None:
            return res

        # we allow rental return!
        # store in DB and return BUS_OK
        self._create_stop_rental_db_entry(vehicle[VehicleTable.VEHICLE_ID],
                                          duration_s=data[InterchangeProtocol.DATA_RIDE_DURATION],
                                          intermediate_stops=data[InterchangeProtocol.DATA_INTERMEDIATE_STOPS],
                                          emergency_stops=data[InterchangeProtocol.DATA_EMERGENCY_STOPS],
                                          access_media_id=person[AccessMediaTable.ACCESS_MEDIA_ID])
        return self.interchange_protocol.create_response_command(InterchangeProtocolError.BUS_OK)

    def _process_smartlock_status(self, data):
        # type: (dict) -> str
        logging.debug("SmartLock status data: {}".format(data))

        # verify vehicle
        smartlock_serial_number = data[InterchangeProtocol.DATA_SMARTLOCK_SERIAL_NUMBER]
        vehicle = self.vehicle_utils.get_vehicle_from_smartlock_serial_number(smartlock_serial_number)
        res = self._verify_vehicle(vehicle, smartlock_serial_number, check_state=False)
        if res is not None:
            return res

        # Add status entry to DB
        self._add_status_entry(data, vehicle[VehicleTable.VEHICLE_ID])

        # Create response
        operations_number_hex = "{}".format(vehicle[VehicleTable.VEHICLE_OPERATIONS_NUMBER]).encode("hex")
        vehicle_type_hex = "{0:08X}".format(vehicle[VehicleTable.VEHICLE_TYPE_ID])
        container_content = [
            (InterchangeProtocolType.TYPE_VEHICLE_TYPE_1, 4, vehicle_type_hex),
            (InterchangeProtocolType.TYPE_BETRIEBSNUMMER_1, len(operations_number_hex) / 2, operations_number_hex),
        ]
        return self.interchange_protocol.create_container(InterchangeProtocolType.TYPE_LOCK_CONFIG_1, container_content)

    def _add_status_entry(self, data, vehicle_id):
        # type: (dict, int) -> None
        ebike_battery_level = None
        if InterchangeProtocol.DATA_EBIKE_BATTERY in data:
            ebike_battery_level = data[InterchangeProtocol.DATA_EBIKE_BATTERY]
        self.status_utils.add_status_entry(vehicle_id=vehicle_id,
                                           smartlock_battery_level=data[InterchangeProtocol.DATA_SMARTLOCK_BATTERY],
                                           ebike_battery_level=ebike_battery_level)

    @staticmethod
    def _process_disconnect(data):
        # type: (dict) -> None
        logging.debug("Received disconnect: {}".format(data))
        return None
