import sys
import unittest

has_xmlrunner = False
try:
    import xmlrunner
    has_xmlrunner = True
except ImportError:
    pass

# add all test classes here
testmodules = [
    'test.station.test_station_configuration',
    'test.station.backend.test_information_api_helper',
    'test.station.backend.test_access_media_api_helper',
    'test.station.backend.test_vehicles_api_helper',
    'test.station.backend.test_configuration_api_helper',
    'test.station.backend.test_status_api_helper',
    'test.station.backend.test_rentals_api_helper',
    'test.station.database.test_database',
    'test.station.database.test_vehicle_table',
    'test.station.database.test_updatetoken_table',
    'test.station.database.test_access_media_table',
    'test.station.database.test_firmware_table',
    'test.station.database.test_configuration_table',
    'test.station.database.test_rental_table',
    'test.station.database.test_status_table',
    'test.station.utils.test_vehicle_utils',
    'test.station.utils.test_access_media_utils',
    'test.station.utils.test_configuration_utils',
    'test.station.utils.test_rental_utils',
    'test.station.utils.test_status_utils',
    'test.station.utils.test_bluetooth_utils',
    'test.station.bluetooth.test_interchange_protocol',
]

suite = unittest.TestSuite()

for t in testmodules:
    try:
        # If the module defines a suite() function, call it to get the suite.
        mod = __import__(t, globals(), locals(), ['suite'])
        suitefn = getattr(mod, 'suite')
        suite.addTest(suitefn())
    except (ImportError, AttributeError):
        # else, just load all the test cases from the module.
        try:
            suite.addTest(unittest.defaultTestLoader.loadTestsFromName(t))
        except Exception, e:
            print "could not load module %s: %s" % (t, e)

if has_xmlrunner:
    res = xmlrunner.XMLTestRunner(verbosity=2, output='test-reports').run(suite)
else:
    res = unittest.TextTestRunner(verbosity=2).run(suite)

sys.exit(len(res.failures) > 0)
