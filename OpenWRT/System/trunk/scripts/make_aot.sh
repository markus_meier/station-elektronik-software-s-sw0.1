#!/bin/bash

# ***********************************************
# help
# ***********************************************
if [ "$(echo $* | grep HELP)" ]; then

	# ***********************************************
	# help
	# ***********************************************
	echo ""
	echo "bash make_openwrt_aot.sh"
	echo "------------------------"
	echo ""
	echo "Parameters:"
	echo "HELP"
	echo "DELETESOURCE"
	echo "OPENWRT"
	echo "LINUX4SAM"
	echo "DOWNLOAD"
	echo "COMPILE"
	echo ""
	echo "Example:"
	echo "bash make_openwrt_aot.sh DELETESOURCE OPENWRT LINUX4SAM DOWNLOAD COMPILE"
	echo ""
	exit
fi

# ***********************************************
# user
# ***********************************************
echo "USER: $USER"
PFADDST=$(pwd)

if [ -e "/home/$USER" ]; then
	cd /home/$USER
	PFADSRC=$(pwd)
else
	echo "Home directory does not exist!!!"
	echo "exit"
	exit
fi

echo "SRC: $PFADSRC"
echo "DST: $PFADDST"

# ***********************************************
# Unterschiede GIT und SVN
# ***********************************************
if [ -e "aot-linux/OpenWRT/System/README.md" ]; then
	PFADSCR="$PFADSRC/aot-linux/OpenWRT/System"
	echo "PFAD: $PFADSRC"
else
	PFADSRC="$PFADSCR/aot-linux"
	echo "PFAD: $PFADSRC"
fi

# ***********************************************
# delete source
# ***********************************************
if [ "$(echo $* | grep DELETESOURCE)" ]; then

	# ***********************************************
	# delete
	# ***********************************************
	cd $PFADDST
	if [ -e "at91" ]; then
		rm -rf at91
	fi
fi

# ***********************************************
# OpenWRT
# ***********************************************
echo "***************************************************************************************"
echo "Very important!!!"

MIPS="$(cat /proc/cpuinfo | grep bogomips | awk '{ print $3 }' | sed 's/\./ /g' | awk '{ print $1 }')"
for i in $MIPS
do
	MIPS=$i
done
echo "MIPS: $MIPS"
if [ "$MIPS" -lt "6000" ]; then
        echo "Your Machine has only $MIPS bogomips. This will take a very long time to compile. Please look, your machine has more than 6000 bogomips."
else
	echo "Your Machine has $MIPS bogomips. This will take a time to compile. "
fi
echo "***************************************************************************************"

# ***********************************************
# Linux4SAM - Download
# ***********************************************
if [ "$(echo $* | grep LINUX4SAM | grep DOWNLOAD)" ]; then

	# ***********************************************
	# AT91 Boostrap
	# ***********************************************
	cd $PFADDST
	if [ ! -e "at91" ]; then
		mkdir at91
	fi
	cd at91
	git clone git://github.com/linux4sam/at91bootstrap.git
	cd at91bootstrap
	echo ""
	echo "branch"
	git branch -r
	echo ""
	echo "tag"
	git tag -l
	echo ""
	git checkout v3.8.7
	echo ""
	cd $PFADDST

	# ***********************************************
	# AT91 U-Boot
	# ***********************************************
	cd $PFADDST
	if [ ! -e "at91" ]; then
		mkdir at91
	fi
	cd at91
	git clone git://github.com/linux4sam/u-boot-at91.git
	#git clone -b linux4sam_5.5 git://github.com/linux4sam/u-boot-at91.git
	cd u-boot-at91
	echo ""
	echo "branch"
	git branch -r
	echo ""
	echo "tag"
	git tag -l
	echo ""
	git checkout linux4sam_5.5
	echo ""
	cd $PFADDST

	# ***********************************************
	# AT91 Kernel
	# ***********************************************
	cd $PFADDST
	if [ ! -e "at91" ]; then
		mkdir at91
	fi
	cd at91
	git clone git://github.com/linux4sam/linux-at91.git
	cd linux-at91
	echo ""
	echo "branch"
	git branch -r
	echo ""
	echo "tag"
	git tag -l
	echo ""
	git checkout linux4sam_5.5
	echo ""
	cd $PFADDST
fi

# ***********************************************
# OpenWRT - download 
# ***********************************************
if [ "$(echo $* | grep OPENWRT | grep DOWNLOAD)" ]; then

	# ***********************************************
	# clone
	# ***********************************************
	cd $PFADDST
	if [ ! -e "at91" ]; then
		mkdir at91
	fi
	cd at91
	git clone -b chaos_calmer git://github.com/openwrt/openwrt.git
	chown $USER:$USER openwrt -R

	# ***********************************************
	# Packete
	# ***********************************************
	cd openwrt
	./scripts/feeds update -a
	./scripts/feeds install -a
	cd $PFADDST
fi

# ***********************************************
# OpenWRT - download 
# ***********************************************
if [ "$(echo $* | grep OPENWRT | grep CONFIG)" ]; then

	# ***********************************************
	# config
	# ***********************************************
	cp $PFADSCR/trunk/openwrt-aot/config/openwrt-aot/config	$PFADDST/at91/openwrt/.config.old
	cp $PFADSCR/trunk/openwrt-aot/config/openwrt-aot/config	$PFADDST/at91/openwrt/.config

	touch $PFADDST/at91/openwrt/.config.old
	touch $PFADDST/at91/openwrt/.config
	cd $PFADDST
fi

# ***********************************************
# OpenWRT - Compile source
# ***********************************************
if [ "$(echo $* | grep OPENWRT | grep COMPILE)" ]; then

	# ***********************************************
	# OpenWRT
	# ***********************************************
	cd $PFADDST
	if [ -e "at91/openwrt" ]; then
		echo ""
		echo "openwrt - compile"
		cd at91
		# ***********************************************
		# kompilieren
		# ***********************************************
		cd openwrt
		make clean
		make
		cd $PFADDST

		if [ ! -e "at91/bin-openwrt" ]; then
			mkdir at91/bin-openwrt
		fi
		cp at91/openwrt/bin/at91/openwrt-at91-sama5d3-root.ext4 at91/bin-openwrt/openwrt-at91-sama5d3-root.ext4
	fi
fi

# ***********************************************
# AT91 Kernel - aot_iped_com Kernel integration 
# ***********************************************
if [ "$(echo $* | grep LINUX4SAM | grep CONFIG)" ]; then

	# ***********************************************
	# config
	# ***********************************************
	cd $PFADDST
	if [ -e "at91/linux-at91" ]; then
		cd at91
		cd linux-at91
		echo ""
		echo "aot_iped_com Kernel integration"
		if [ ! "$(cat $PFADDST/at91/linux-at91/drivers/Makefile | grep aotiped)" ]; then
			echo "ADD: aot_iped_com driver"
			cat $PFADSRC/aot-linux/Kerneltreiber/Makefile >> $PFADDST/at91/linux-at91/drivers/Makefile
		fi
		if [ ! -e "$PFADDST/at91/linux-at91/drivers/aotiped" ]; then
			echo "ADD: aot_iped_com source code"
			if [ -e "$PFADSRC/aot-linux/Kerneltreiber/aotiped" ]; then
				cp -r $PFADSRC/aot-linux/Kerneltreiber/aotiped $PFADDST/at91/linux-at91/drivers/
			fi
		fi
		echo "done"
	fi
fi

# ***********************************************
# Linux4SAM
# ***********************************************
if [ "$(echo $* | grep LINUX4SAM | grep COMPILE)" ]; then

	# ***********************************************
	# AT91 Boostrap
	# ***********************************************
	if [ "$(echo $* | grep LINUX4SAM | grep COMPILE | grep BOOTSTRAP)" ]; then
		cd $PFADDST
		if [ -e "at91/at91bootstrap" ]; then
			echo ""
			echo "bootstrap at91 - compile"
			cd at91
			cd at91bootstrap
			make mrproper
			make sama5d3_xplainedsd_uboot_defconfig
			make CROSS_COMPILE=arm-linux-gnueabi-
			echo ""
			cd $PFADDST
		fi
	fi

	# ***********************************************
	# AT91 U-Boot
	# ***********************************************
	if [ "$(echo $* | grep LINUX4SAM | grep COMPILE | grep UBOOT)" ]; then
		cd $PFADDST
		if [ -e "at91/u-boot-at91" ]; then
			echo ""
			echo "u-boot at91 - compile"
			cd at91
			cd u-boot-at91
			make distclean
			make sama5d3_xplained_mmc_defconfig
			make CC=arm-linux-gnueabi-gcc CROSS_COMPILE=arm-linux-gnueabi- all
			echo ""
			cd $PFADDST
	
			if [ ! -e "at91/bin-u-boot" ]; then
				mkdir at91/bin-u-boot
			fi
			cp at91/u-boot-at91/u-boot.bin at91/bin-u-boot/u-boot.bin
		fi
	fi

	# ***********************************************
	# AT91 Kernel
	# ***********************************************
	if [ "$(echo $* | grep LINUX4SAM | grep COMPILE | grep KERNEL)" ]; then
		cd $PFADDST
		if [ -e "at91/linux-at91" ]; then
			cd at91
			cd linux-at91
			echo ""
			echo "linux at91 - compile"
			if [ -e "$PFADSRC/aot-linux/OpenWRT/System/trunk/linux-aot/arch/arm/configs/sama5_aot_defconfig" ]; then
				if [ -e $PFADDST/at91/linux-at91/arch/arm/configs/sama5_aot_defconfig ]; then
					rm -f $PFADDST/at91/linux-at91/arch/arm/configs/sama5_aot_defconfig
				fi
				cp $PFADSRC/aot-linux/OpenWRT/System/trunk/linux-aot/arch/arm/configs/sama5_aot_defconfig $PFADDST/at91/linux-at91/arch/arm/configs/sama5_aot_defconfig
			fi
			make ARCH=arm sama5_aot_defconfig CROSS_COMPILE=arm-linux-gnueabi-
			make ARCH=arm CROSS_COMPILE=arm-linux-gnueabi-
			make ARCH=arm uImage LOADADDR=0x20008000 CROSS_COMPILE=arm-linux-gnueabi-
			make ARCH=arm dtbs CROSS_COMPILE=arm-linux-gnueabi-
			echo ""
			cd $PFADDST
	
			if [ ! -e "at91/bin-kernel-ko" ]; then
				mkdir at91/bin-kernel-ko
			fi
			find at91/linux-at91 -name "*.ko" -exec cp -R {} "at91/bin-kernel-ko" \;
	
			if [ ! -e "at91/bin-kernel" ]; then
				mkdir at91/bin-kernel
			fi
			cp at91/linux-at91/arch/arm/boot/zImage at91/bin-kernel/zImage
		fi
	fi
fi

# ***********************************************
# exit
# ***********************************************
exit

