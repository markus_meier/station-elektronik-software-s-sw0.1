# README #

## Infos ##
* Login in der VM als Benutzer: "user" mit dem Passwort: "1234"

### Kompilieren für OpenWRT###
Dazu müssen die Sourcen heruntergeladen und die Toolchain kompiliert werden. Wichtig zu wissen: Computer mit: 3.4GHz, 8 Core mit 24GB RAM, dauert das erstmalige kompilieren etwa 90 min.
Nach Ausführen der Zeile 4, kann eine grosszügige Mittagspause eingeplant werden.

* if [ -e "/home/user/aot-linux" ]; then rm -rf /home/user/aot-linux; fi
* if [ -e "/home/user/at91" ]; then rm -rf /home/user/at91; fi
* git clone https://bitbucket.org/RomanBir/station-elektronik-software-s-sw0.1.git /home/user/aot-linux
* cd /home/user
* time bash aot-linux/OpenWRT/System/trunk/scripts/make_aot.sh DELETESOURCE OPENWRT LINUX4SAM DOWNLOAD CONFIG COMPILE BOOTSTRAP UBOOT KERNEL
* cd /home/user
* cd aot-linux/Interchange_Protokoll_Encode_Decode_IPED
* bash compile.sh

oder als Debug version:

* bash compile.sh DEBUG

Nun steht das Programm: "interchange_protokoll_encode_decode" zur verfügung

### Kompilieren für x86 mit eclipse ###
Hierfür kann eclipse mit X Forwarding verwendet werden.
Ein geeigneter X-Server für Windows ist zu finden unter:

* https://sourceforge.net/projects/xming

Damit der X-Server mit der korrekten Tastaturlayout arbeitet, folgendes batch File (z.B. xming.bat) erstellen mit folgendem Inhalt, dann den X-Server starten:

"%COMMONPROGRAMFILES(x86)%\..\Xming\run.exe" "%COMMONPROGRAMFILES(x86)%\..\Xming\Xming.exe" -multiwindow -clipboard -xkblayout ch -clipboard

Nun mit dem putty eine Verbindung zur VM mit X-Forwarding eingestellt durchführen.

* cd /home/user
* eclipse 1>/dev/null 2>/dev/null &

Mit eclipse nun folgende Schritte machen:

* Ein neues Projekt erstellen: File -> New -> C Projekt
* Die Sourcen importieren
* Automatische Makefile generierung ausschalten, WICHTIG!!!
* Im Fenster "Make Target" ein paar Einträge erstellen zum bequemen kompilieren

### Kompilieren für x86 ohne eclipse ###

* cd /home/user
* cd aot-linux/Interchange_Protokoll_Encode_Decode_IPED
* make clean
* make CC=gcc ARCH=x86

oder ohne Debug Ausgaben:

* make CC=gcc ARCH=x86_NO_DBG

