# README #


### 7-zip ###
Zum entkomprimieren der Images.

Download:

- http://www.7-zip.de


### Win32 Disk Imager ###
Image auf die SD-Card schreiben.

- https://sourceforge.net/projects/win32diskimager


### SAMA5D3 ###
Wir haben das Board hier bestellt:

- https://www.digikey.ch/product-detail/de/microchip-technology/ATSAMA5D3-XPLD/ATSAMA5D3-XPLD-ND/4729307

### Entwicklungsumgebung ###
Die Entwicklungsumgebung ist auf diesem FTP Server:

- FTP Server: ftp.netzone.ch

Mail an: daniel.schwab@aotag.ch mit mobile Nummer zum empfangen von Benutzername und Passwort via SMS.

### Wichtige Infos ###
- Es k�nnen keine "kmod" Kernel Module aus dem Repository in den aktuell verwendeten Kernel geladen werden. Werden Kernel Module ben�tigt, bitte ein Mail an: daniel.schwab@aotag.ch
- Je nach Board, muss der "Button" "RESET" oder "WAKE UP" kurz gedr�ckt werden, damit das Board startet.
- Es darf lediglich ein Ger�t an das Internet angeschlossen sein (Ethernet0, Ethernet1 oder Modem).
- MAC Adresse fixiert:
	- eth0    1E:3A:A5:09:DC:19    1000MBit
	- eth1    86:B2:44:C0:E2:79     100MBit

### Firmwareupdate mit dem SAMA5D3 ###
Ab Version 0.3.1

Kommandozeile folgendes eingeben:
	- aot_firmware_update		///> Es werden nur Programme und Daten hinzugef�gt, keine gel�scht. Quelle GIT -> Bitbucket

### 3G APN umstellen ###
Je nach SIM Karte die verwendet werden, m�ssen andere konfigurationen eingestellt werden. Mit den folgenden Kommandos kwird dies ge�ndert.
	- aot_set_3g_publibike_swisscom
	- aot_set_3g_salt

### Images erstellen mit dem SAMA5D3 ###
Ab Version 0.1.9

Kommandozeile folgendes eingeben:
	- aot_clone_from_git
	- aot_Image_write_to_disk /dev/sdx /tmp/sd-card/aot-linux/OpenWRT/Image/SAMA5D3_V_0_2_0_2017_08_24_09_23.img.gz

### Images ###

### Version 0.3.1 ###

- File
	- SAMA5D3_V_0_3_1_2017_09_01_13_38.img.gz

- SD-Card
	- mind. 8GByte

- Feature
	- Wenn ein Ethernetkabel eingesteckt wird, schaltet die ppp Verbindung aus. Routing l�uft dann �ber Ethernet.
	- Wein ein Ethernetkabel ausgezogen wird, wird die ppp Verbindung wieder hochgefahren.
	- Wichtig ist, dass zwischen einstecken und ausziehen immer etwa 30 Sekunden gewartet werden soll. Gilt auch f�r das CMD Line Tool. 
	- Umschalten zwischen Salt und Swisscom SIM Karten. Es wird davon ausgegangen, dass ein Ethernetkabel eingesteckt ist und dar�ber via SSH komuniziert wird.
		- aot_set_3g_salt 0			# Einstellen auf Salt, Modemkommunikation OFF
		- aot_set_3g_publibike_swisscom 0	# Einstellen auf publibike Swisscom, Modemkommunikation OFF
		- Default eingestellt: aot_set_3g_publibike_swisscom 0
	- Skript f�r Firmwareupdate erweitert

- Known Bugs

### Version 0.3.0 ###

- File
	- SAMA5D3_V_0_3_0_2017_08_28_20_00.img.gz

- SD-Card
	- mind. 8GByte

- Feature
	- Python Skripte zur Kommunikatin mit dem Backend.

- Known Bugs
	- Die Funktion: interchange_protocol_decode gibt kein ERR_OK zur�ck. Daten werden korrekt decodiert. Bufferr�cksetzung kann wie im Beispiel mit ERR_NO_SYNC_FOUND gemacht werden.

### Version 0.2.2 ###

- File
	- SAMA5D3_V_0_2_2_2017_08_25_14_00.img.gz

- SD-Card
	- mind. 8GByte

- Feature
	- Skript: "aot_firmware_update"
	- Routing, wenn Modem und Ethernet verwendet wird. Das zuletzt angeschlossene Ger�t wird geroutet. Ausschalten des Modem ist noch nicht implementiert.

- Known Bugs
	- Die Funktion: interchange_protocol_decode gibt kein ERR_OK zur�ck. Daten werden korrekt decodiert. Bufferr�cksetzung kann wie im Beispiel mit ERR_NO_SYNC_FOUND gemacht werden.

### Version 0.2.1 ###

- File
	- SAMA5D3_V_0_2_1_2017_08_24_17_54.img.gz

- SD-Card
	- mind. 8GByte

- Feature
	- Skript: "aot_clone_from_git; aot_firmware_update"

- Known Bugs
	- Die Funktion: interchange_protocol_decode gibt kein ERR_OK zur�ck. Daten werden korrekt decodiert. Bufferr�cksetzung kann wie im Beispiel mit ERR_NO_SYNC_FOUND gemacht werden.

### Version 0.2.0 ###

- File
	- SAMA5D3_V_0_2_0_2017_08_24_09_23.img.gz

- SD-Card
	- mind. 8GByte

- Feature
	- Skript: "aot_firmware_update"

- Known Bugs
	- Die Funktion: interchange_protocol_decode gibt kein ERR_OK zur�ck. Daten werden korrekt decodiert. Bufferr�cksetzung kann wie im Beispiel mit ERR_NO_SYNC_FOUND gemacht werden.
	- Skript: "aot_firmware_update"

### Version 0.1.9 ###

- File
	- SAMA5D3_V_0_1_9_2017_08_23_10_41.img.gz

- SD-Card
	- mind. 8GByte

- Feature
	- Kernel Ger�t bereit zum benutzen: /dev/aot_iped_COM0
	- Bugfix Startup Skripte.

- Known Bugs
	- Die Funktion: interchange_protocol_decode gibt kein ERR_OK zur�ck. Daten werden korrekt decodiert. Bufferr�cksetzung kann wie im Beispiel mit ERR_NO_SYNC_FOUND gemacht werden.

### Version 0.1.7 ###

- File
	- SAMA5D3_V_0_1_7_2017_08_15_16_24.7z

- SD-Card
	- mind. 8GByte

- Feature
	- interchange_protocol_encode_decode V1.1.4 hinzugef�gt
	- Bugfix CMD Line Tool -> Container Data output: offset 2 Bytes 

- Known Bugs
	- Die Funktion: interchange_protocol_decode gibt kein ERR_OK zur�ck. Daten werden korrekt decodiert. Bufferr�cksetzung kann wie im Beispiel mit ERR_NO_SYNC_FOUND gemacht werden.

### Version 0.1.5 ###

- File
	- SAMA5D3_V_0_1_5_2017_07_19_18_58.7z

- SD-Card
	- mind. 8GByte

- Feature
	- interchange_protocol_encode_decode V1.1.1 hinzugef�gt
	- interchange_protocol_encode_decode_debug V1.1.1 hinzugef�gt
	- postgresql default disabled, zum starten: /etc/init.d/aot_postgresql start
	- Zum testen (zerst�ren der Datenbankdaten): aot_destroy_data_partition

### Version 0.1.6 ###

- File
	- SAMA5D3_V_0_1_5_2017_08_09_10_19.7z

- SD-Card
	- mind. 8GByte

- Feature
	- interchange_protocol_encode_decode V1.1.2 hinzugef�gt - Datenausgabe als single linie, Datenunterteilung mit: ";"

- Known Bugs
	- Die Funktion: interchange_protocol_decode gibt kein ERR_OK zur�ck. Datenwerden korrekt decodiert. Bufferr�cksetzung kann wie im Beispiel mit ERR_NO_SYNC_FOUND gemacht werden.

### Version 0.1.5 ###

- File
	- SAMA5D3_V_0_1_5_2017_07_19_18_58.7z

- SD-Card
	- mind. 8GByte

- Feature
	- interchange_protocol_encode_decode V1.1.1 hinzugef�gt
	- interchange_protocol_encode_decode_debug V1.1.1 hinzugef�gt
	- postgresql default disabled, zum starten: /etc/init.d/aot_postgresql start
	- Zum testen (zerst�ren der Datenbankdaten): aot_destroy_data_partition

### Version 0.1.4 ###

- File
	- SAMA5D3_V_0_1_4_2017_07_18_11_40.7z

- SD-Card
	- mind. 8GByte

- Feature
	- interchange_protocol_encode_decode V1.0.0 hinzugef�gt
	- VPN config auf UDP und 1194 ge�ndert
	- Das Programm: "interchange_protocol_encode_decode", wird in der Version 1.0.0 mitgeliefert.

### Version 0.1.3 ###

- File
	- SAMA5D3_V_0_1_3_2017_07_14_11_16.7z

- SD-Card
	- mind. 8GByte

- Feature
	- Python Modul hinzugef�gt: requests

### Version 0.1.2 ###

- File
	- SAMA5D3_V_0_1_2_2017_06_29_12_30.7z

- SD-Card
	- mind. 8GByte

- Feature
	- SWAP Partition etwas verkleinert, damit die Partition auch mit etwas Reserve in eine 8GByte Karte passt.

- Programme
	- OpenVPN

### Version 0.1.1 ###

- File
	- SAMA5D3_V_0_1_1_2017_06_19_16_18.7z

- SD-Card
	- mind. 16GByte

- Feature
	- PPP Verbindung ins Internet mit dem HUAWEI M609, sobald es eingesteckt wird.
	- Ethernet muss vorher ausgezogen werden, damit die Daten, �ber das Modem geroutet werden.

- OpenVPN
	- F�r Testzwecke ist ein Satz Schl�ssel erstellt worden. Abgelegt unter /etc/easy-rsa/keys/
		- Server config: /etc/openvpn/vpn-server.ovpn
		- Client config: /etc/openvpn/vpn-client.ovpn
	- Damit der Server oder Client nach dem Reboot l�uft, die Endung ".ovpn" umbenennen in ".conf", dann einen reboot durchf�hren.
		- Folgende Anpassungen sind n�tig:
		- vpn-client: remote art_of_technology.dnsalias.com 59123	// Port und URL anpassen
		- vpn-server: port 59123									// Port anpassen

### Version 0.1.0 ###

- File
	- SAMA5D3_V_0_1_0_2017_05_29_16_10.7z

- SD-Card
	- ind. 16GByte

Feature

- MAC Adresse fixiert
	- eth0    1E:3A:A5:09:DC:19
	- eth1    86:B2:44:C0:E2:79

- DHCP Client auf beiden Schnittstellen

- Implementierte Kommandos
	- zum benutzen: 		/sbin/aot_destroy_data_partition	# zerst�ren der Datenpartition, anschliessender Reboot
	- nicht benutzen: 		/sbin/aot_postgres_startup			# Wird im Startupprozess verwendet, do not use!!!

- System Startup (bereits automatisiert)
	- Starten der Systemdienste
	- Kontrolle der Datenpartition (SD-Card). Wenn defekt, Formatieren mit ext4, anlegen einer leeren Datenbank
	- Starten der Datenbank

- Login
	- Benutzername: root
	- Passwort: 1234

- Programme
	- OpenWRT (Chaos Calmer 15.05.1, https://openwrt.org)
	- Busybox 1.23.2
	- Python 2.7
	- postgresql 9.4.4
	- Kernel 4.4.26-linux4sam_5.5
	- LuCI 15.05-142-gb6330a1 http://x.x.x.x (grafische Konfigurationsoberfl�che)
