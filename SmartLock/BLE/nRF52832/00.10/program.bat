nrfjprog --recover                                  -f nrf52
nrfjprog --eraseall                                 -f nrf52
nrfjprog --program s132_nrf52_3.0.0_softdevice.hex  -f nrf52 --sectorerase
nrfjprog --program PubliBike_SmartLock_Firmware.hex -f nrf52 --sectorerase
nrfjprog --reset                                    -f nrf52
