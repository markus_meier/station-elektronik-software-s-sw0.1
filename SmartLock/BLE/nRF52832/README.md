# README #

### Images ###

### Version 00.03 ###
F�r das Modul: BMD-300

- File
	- PubliBike_SmartLock_Firmware.hex
	- s132_nrf52_3.0.0_softdevice.hex

- Feature
	- Angepasste Version f�r das BMD-300 Modul
	- UART Tunnel via BLE

- known issues
	- Maximal 60Bytes senden, dann mind. 10ms warten, dann die n�chsten 60Bytes senden, usw.

- Doku
	- https://publibike.atlassian.net/wiki/display/SYS/GATT+App+SmartLock

### Version 00.02 ###
Custom Service Publibike.

- File
	- PubliBike_SmartLock_Firmware.hex
	- s132_nrf52_3.0.0_softdevice.hex

- Feature
	- Kommuniziert mit dem Custom Service Publibike

- Doku
	- https://publibike.atlassian.net/wiki/display/SYS/GATT+App+SmartLock

### Version 00.01 ###
Nordic UART Example

- File
	- PubliBike_SmartLock_Firmware.hex
	- s132_nrf52_3.0.0_softdevice.hex

- Feature
	- Kommunikation zwischen dem nRF52832 und der Android App.

### Android App ###
Diese App haben wir verwendet zum Testen mit BLE 4.0 GATT.

- https://play.google.com/store/apps/details?id=no.nordicsemi.android.mcp&hl=de

#### App Source ####

- https://github.com/NordicSemiconductor/Android-nRF-UART
- https://github.com/NordicSemiconductor/IOS-nRF-Toolbox

### program.bat / program.sh ###
Doppel klick zum Programmieren des nRF52xxx.
Folgende Tool zum Programmieren werden benoetigt (Bitte Version 9.5.0 verwenden):

- Windows
	- https://www.nordicsemi.com/eng/nordic/Products/nRF52832/nRF5x-Command-Line-Tools-Win32/48768
- Mac
    - JLink von https://www.segger.com/downloads/jlink/JLink_MacOSX_V616e.pkg
    - nRF5x-Command-Line-Tools-OSX von http://www.nordicsemi.com/eng/Products/Bluetooth-low-energy/nRF52832

### Serial Terminal auf Mac ###
1. Anzeige der devices mit `ls -ltr /dev/tty.usbserial*` 
2. �ffnen des terminals mit `screen /dev/<device> 115200,cs8,-parenb,-cstopb,-hupcl`

Das Terminal kann mit `Ctrl-a` `Ctrl-d` geschlossen werden.

### Wichtige Infos ###
Die Beschreibung, wie das Board in Betrieb genommen wird, ist im PDF von uns beschrieben.

Development Board nRF52832

- Station: https://www.digikey.ch/products/de/rf-if-and-rfid/rf-evaluation-and-development-kits-boards/859?k=BMD-301
- Schloss: https://www.digikey.ch/products/de/rf-if-and-rfid/rf-evaluation-and-development-kits-boards/859?k=BMD-300

Pinbelegung

* Pin 22    P0.06     UART TX       -> STM32_RX
* Pin 24    P0.08     UART RX       -> STM32_TX
* Pin 31    P0.13     I2C_SCL       -> CRYPTO_SCL
* Pin 33    P0.15     I2C_SDA       -> CRYPTO_SDA

Die Speisung der Chips schalten wir (Antrimon) nicht ab.

Den nRF koennten wir (Antrimon) in Reset versetzen. Vermutlich resultiert das jedoch in einem hoeheren Stromverbrauch als der deep sleep.

An P0.19 haben wir noch eine Reserve-Verbindung zum STM32.
