#!/usr/bin/env bash

export PATH="$PATH:~/Downloads/nRF5x-Command-Line-Tools_9_5_0_OSX/nrfjprog"

nrfjprog --eraseall                                 -f nrf52                                 --log
nrfjprog --program ${BASH_SOURCE%/*}/s132_nrf52_3.0.0_softdevice.hex  -f nrf52 --sectorerase --log
nrfjprog --program ${BASH_SOURCE%/*}/PubliBike_Station_Firmware.hex   -f nrf52 --sectorerase --log
nrfjprog --reset                                    -f nrf52                                 --log
